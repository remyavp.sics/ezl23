//
//  BaseAlertController.swift
//  Follow
//
//  Created by Appzoc on 10/01/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation
import UIKit

public class BaseAlert {
    
    fileprivate init(){}
    
    // show normal alert with "OK" button.

    class func alert(withTitle title: String, message: String, tintColor: UIColor, andPresentOn viewContoller: UIViewController)
    {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.view.tintColor = tintColor
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            viewContoller.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    // show automatically dismissing alert.
    
    class func autoDismissAlert(withTitle: String, message: String, dismissTime: Double,  andPresentOn viewController: UIViewController){
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: withTitle, message: message, preferredStyle: UIAlertController.Style.alert)
            viewController.present(alert, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dismissTime){
                alert.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    
    
    // show alert with controll go back to the view controller which it is presented on
    
    class func completionAlert(withTitle title: String, message: String, tintColor: UIColor, andPresentOn viewContoller: UIViewController, escapingFuncName: @escaping () -> Void)
    {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            alert.view.tintColor = tintColor
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Try again", style: UIAlertAction.Style.default, handler: { (action) in
                escapingFuncName()
            }))
            
            
            viewContoller.present(alert, animated: true, completion: nil)
            
        }
    }
    
    class func tryAgainAlert(message:String,andPresentOn viewContoller: UIViewController,escapingFuncName: @escaping () -> Void){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Way to Nikah", message: message, preferredStyle: UIAlertController.Style.alert)
         
            alert.addAction(UIAlertAction(title: "Try again", style: UIAlertAction.Style.destructive, handler: { (action) in
                escapingFuncName()
            }))
            viewContoller.present(alert, animated: true, completion: nil)
        }
    }
    
    
    class func baseAlertCompletion(withTitle title: String, message: String, tintColor: UIColor,cancelAction:String,action:String,andPresentOn viewContoller: UIViewController,cancelActionStyle:UIAlertAction.Style,actionStyle:UIAlertAction.Style ,escapingFuncName: @escaping () -> Void)
    {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            alert.view.tintColor = tintColor
            
            alert.addAction(UIAlertAction(title: cancelAction, style: cancelActionStyle, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: action, style: actionStyle, handler: { (action) in
                escapingFuncName()
            }))
            
            
            viewContoller.present(alert, animated: true, completion: nil)
            
        }
    }
    
}



