//
//  BaseColor.swift
//  Popular
//
//  Created by admin on 26/06/18.
//  Copyright © 2018 Appzoc. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    class func RedColorFrom() -> UIColor {
        return UIColor(red: 251/255.0, green: 91/255.0, blue: 62/255.0, alpha: 1.0)
    }
    class func RedColorTo() -> UIColor {
        return UIColor(red: 220/255.0, green: 59/255.0, blue: 30/255.0, alpha: 1.0)
    }
    class func BlueColor() -> UIColor {
        return UIColor(red: 24/255.0, green: 176/255.0, blue: 181/255.0, alpha: 1.0)
    }
    
    class func DarkColor() -> UIColor {
        return UIColor(red: 78/255.0, green: 78/255.0, blue: 78/255.0, alpha: 1.0)
    }

    
        public convenience init?(hexString: String) {
            let r, g, b, a: CGFloat
            
            if hexString.hasPrefix("#") {
                let start = hexString.index(hexString.startIndex, offsetBy: 1)
                let hexColor = String(hexString[start...])
                
                if hexColor.count == 8 {
                    let scanner = Scanner(string: hexColor)
                    var hexNumber: UInt64 = 0
                    
                    if scanner.scanHexInt64(&hexNumber) {
                        r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                        g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                        b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                        a = CGFloat(hexNumber & 0x000000ff) / 255
                        
                        self.init(red: r, green: g, blue: b, alpha: a)
                        return
                    }
                }
            }
            
            return nil
        }

}
