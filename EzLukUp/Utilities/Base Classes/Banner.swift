
import Foundation
import NotificationBannerSwift

class Banner {
    
    static let shared = Banner()
    private init(){}
  //  var banner:GrowingNotificationBanner?
    var banner:FloatingNotificationBanner?
    var currentTitle : String?
//    func showBanner(title: String?, subtitle: String?,style:BannerStyle?){
//        banner?.bannerQueue.removeAll()
//        DispatchQueue.main.async {
//            self.currentTitle = title
//            self.banner?.dismiss()
//            let neWbanner = GrowingNotificationBanner(title: title, subtitle: subtitle, style: style ?? .success)
//
//           // neWbanner.backgroundColor = UIColor.dfBurgundy
//            neWbanner.show()
//            neWbanner.autoDismiss = true
//            neWbanner.dismissOnTap = true
//            neWbanner.dismissOnSwipeUp = true
//            neWbanner.duration = 1
//            neWbanner.backgroundColor = .darkGray
//           // neWbanner.dismissDuration = 0.2
//            self.banner = neWbanner
//            self.currentTitle  = title
//        }
//
//    }
    
    func showBanner(title: String?, subtitle: String?,style:BannerStyle?,titleFont:UIFont? = nil,titleColor:UIColor? = nil,titleTextAlign:NSTextAlignment? = nil,leftView:UIView? = nil,rightView:UIView? = nil,subtitleFont:UIFont? = nil,subtitleColor:UIColor? = nil,subtitleTextAlign:NSTextAlignment? = .center,bannerBackGroundColor:UIColor? = UIColor.black.withAlphaComponent(0.8),isCornerRounded:Bool = false){
        banner?.bannerQueue.removeAll()
        DispatchQueue.main.async {
            self.currentTitle = title
            self.banner?.dismiss()
            let neWbanner = FloatingNotificationBanner(title: title, subtitle: subtitle, titleFont: titleFont, titleColor: titleColor, titleTextAlign: titleTextAlign, subtitleFont: subtitleFont, subtitleColor: subtitleColor, subtitleTextAlign: subtitleTextAlign, leftView: leftView, rightView: rightView, style: style ?? .info, iconPosition: .center)
            neWbanner.backgroundColor = bannerBackGroundColor
            neWbanner.autoDismiss = true
            neWbanner.dismissOnTap = true
            neWbanner.dismissOnSwipeUp = true
            neWbanner.duration = 1
            var cornerRadi:CGFloat = 0
            if isCornerRounded {
                cornerRadi = (neWbanner.bannerHeight/2)-16
            } else {
                cornerRadi = 10
            }
            neWbanner.show(bannerPosition: .bottom, edgeInsets: UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15), cornerRadius: cornerRadi, shadowColor: .lightGray, shadowOpacity: 0.5, shadowBlurRadius: 16, shadowCornerRadius: cornerRadi, shadowEdgeInsets: UIEdgeInsets.zero)
           // neWbanner.dismissDuration = 0.2
            self.banner = neWbanner
            self.currentTitle  = title
        }
    }
    func showfailureBanner(title: String?, subtitle: String?,style:BannerStyle?,titleFont:UIFont? = nil,titleColor:UIColor? = nil,titleTextAlign:NSTextAlignment? = nil,leftView:UIView? = nil,rightView:UIView? = nil,subtitleFont:UIFont? = nil,subtitleColor:UIColor? = nil,subtitleTextAlign:NSTextAlignment? = .center,bannerBackGroundColor:UIColor? = UIColor.black.withAlphaComponent(0.8),isCornerRounded:Bool = false){
        banner?.bannerQueue.removeAll()
        DispatchQueue.main.async {
            self.currentTitle = title
            self.banner?.dismiss()
            let neWbanner = FloatingNotificationBanner(title: title, subtitle: subtitle, titleFont: titleFont, titleColor: titleColor, titleTextAlign: titleTextAlign, subtitleFont: subtitleFont, subtitleColor: subtitleColor, subtitleTextAlign: subtitleTextAlign, leftView: leftView, rightView: rightView, style: style ?? .info, iconPosition: .center)
            neWbanner.backgroundColor = bannerBackGroundColor
            neWbanner.autoDismiss = true
            neWbanner.dismissOnTap = true
            neWbanner.dismissOnSwipeUp = true
            neWbanner.duration = 1
            var cornerRadi:CGFloat = 0
            if isCornerRounded {
                cornerRadi = (neWbanner.bannerHeight/2)-16
            } else {
                cornerRadi = 10
            }
            neWbanner.show(bannerPosition: .top, edgeInsets: UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15), cornerRadius: cornerRadi, shadowColor: .lightGray, shadowOpacity: 0.5, shadowBlurRadius: 16, shadowCornerRadius: cornerRadi, shadowEdgeInsets: UIEdgeInsets.zero)
           // neWbanner.dismissDuration = 0.2
            self.banner = neWbanner
            self.currentTitle  = title
        }
    }
    func showParsingErrorBanner(){
        self.showBanner(title: nil, subtitle: "Failed to cast reponse to dictionary/found nil value", style: .warning)
    }
    
}

