//
//  Utils.swift
//  EzLukUp
//
//  Created by REMYA V P on 20/10/22.
//

import Foundation
import UIKit


func showDefaultAlert(viewController: UIViewController, title: String? = nil, msg: String) {
    let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
    }))
    viewController.present(alert, animated: true, completion: nil)
}
func limitValidation(string: String, minLength: Int, maxLength: Int) -> Bool {
    if maxLength > 0{
        return (string.count >= minLength && string.count <= maxLength)
    } else {
        return (string.count >= minLength)
    }
    
}

