//
//  UITextField.swift
//  DayToFresh
//
//  Created by farhan k on 10/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit


extension UITextField {
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    func setIcon(_ image: UIImage) {
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: 0, y: 0, width: 40, height: 40))
        let iconView = UIImageView(frame:
            CGRect(x: 0, y: 0, width: 18, height: 18))
        iconView.image = image
        iconView.contentMode = .scaleAspectFit
        iconView.center = iconContainerView.center
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
    }
}

