//
//  UINavigation.swift
//  DayToFresh
//
//  Created by farhan k on 12/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

public extension UINavigationController {
    
    /**
     Pop current view controller to previous view controller.
     
     - parameter type:     transition animation type.
     - parameter duration: transition animation duration.
     */
    func pop(transitionType type: CATransitionType = .push, transitionSubtype subType:CATransitionSubtype = .fromTop,duration: CFTimeInterval = 0.3) {
        self.addTransition(transitionType: type, transitionSubtype: subType, duration: duration)
        self.popViewController(animated: false)
    }
    
    /**
     Push a new view controller on the view controllers's stack.
     
     - parameter vc:       view controller to push.
     - parameter type:     transition animation type.
     - parameter duration: transition animation duration.
     */
    func push(viewController vc: UIViewController, transitionType type: CATransitionType = .fade,transitionSubtype subType:CATransitionSubtype = .fromTop ,duration: CFTimeInterval = 0.3) {
        self.addTransition(transitionType: type, transitionSubtype: subType, duration: duration)
        self.pushViewController(vc, animated: false)
    }
    
    private func addTransition(transitionType type: CATransitionType = .fade,transitionSubtype subType:CATransitionSubtype = .fromTop ,duration: CFTimeInterval = 0.3) {
        let transition = CATransition()
        transition.duration = duration
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = type
        transition.subtype = subType
        self.view.layer.add(transition, forKey: nil)
    }
    
    func popvc(viewController vc: UIViewController, transitionType type: CATransitionType = .fade,transitionSubtype subType:CATransitionSubtype = .fromTop ,duration: CFTimeInterval = 0.65) {
        self.addTransition(transitionType: type, transitionSubtype: subType, duration: duration)
        self.poptovc(viewController: vc)
    }
    
    private func poptovc(viewController vc: UIViewController){
        for controller:UIViewController in viewControllers {
            if controller.isKind(of:vc.classForCoder) {
                self.popToViewController(controller, animated: false)
                break
            }
            
        }
    }
    
}

extension UIViewController {
    func popToYour(VC:AnyClass,animated:Bool) {
        if let navController = self.navigationController {
            for controller in navController.viewControllers as Array {
                if controller.isKind(of: VC) {
                    _ = navController.popToViewController(controller, animated: animated)
                    break
                }
            }
        }
    }
    
     /// The visible view controller from a given view controller
    var visibleViewController: UIViewController? {
           if let navigationController = self as? UINavigationController {
               return navigationController.topViewController?.visibleViewController
           } else if let tabBarController = self as? UITabBarController {
               return tabBarController.selectedViewController?.visibleViewController
           } else if let presentedViewController = presentedViewController {
               return presentedViewController.visibleViewController
           } else {
               return self
           }
       }
}

extension UIApplication {
    /// The top most view controller
    static var topMostViewController: UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController?.visibleViewController
    }
}

