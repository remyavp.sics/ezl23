//
//  WebService.swift
//  DayToFresh
//
//  Created by farhan k on 22/04/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import Alamofire
import Foundation
import UIKit

let baseURL = ""

class Webservice : NSObject {
    
    typealias CompletionHandler = (_ completed:Bool,_ response:[String:Any]?,_ message:String?) -> Void
    var path:String?
    var encoding:ParameterEncoding = URLEncoding.default
    var params:Parameters? = nil
    var headerField:HTTPHeaders? = nil
    var requestMethod:HTTPMethod = .post
    var withLoading:Bool = true
    var controller:UIViewController!
    
    
    init(path:String?,requestMethod:HTTPMethod? = .post,params:[String:Any]?,header:[String:Any]? = nil,encoding:Encoding? = .url,withLoading:Bool,controller:UIViewController) {
        super.init()
        self.path = path
        self.requestMethod = requestMethod ?? .get
        if let path = self.path {
            var param:[String:Any] = [:]
            param = params ?? [:]
            param["action"] = path
            self.params = param
            
           
        }
        self.headerField = makeHTTPHeader(header)
        self.encoding = encoding?.rawValue ?? Encoding.url.rawValue
        self.withLoading = withLoading
        self.controller = controller
    }
    
    func excuteRequest(completion:@escaping(CompletionHandler)){
        
        if !NetworkReachabilityManager()!.isReachable {
            print("Request Failed Due to No internet connection")
            GeneralAlert.shared.generalAlertWithCallBack(controller: controller, message: "The internet connection appears to be offline.", title: "", actionTitle: "Try Again", alertStyle: .alert) {
                self.excuteRequest(completion: completion)
            }
        } else {
            
            if self.withLoading {
                           ActivityLoader.shared.startActivityLoader()
                       }
            
            let url = "\(baseURL)"
            
            
            print("========================= Debug Print:- Printing Request ======================== \n url :\(url) \n method :\(requestMethod) \n params :\(params ?? [:]) \n header:\(headerField ?? [:]) \n encoding :\(encoding) \n ========================= ======================== ========================")
            
            AF.request(url, method: requestMethod, parameters: params, encoding: encoding, headers: headerField).responseJSON {  (response) in
                print("================= Debug Print:- Response =================")
                debugPrint(response)
                print("========================= ======================== ========================")
                if self.withLoading {
                    ActivityLoader.shared.StopActivityLoader()
                }

                switch response.result {
                case .success(let success):
                    guard let result = success as? [String:Any] else {
                        completion(false,nil,"Failed to caste success response to dictionary")
                        return
                    }
                    let message = result["message"] as? String
                    if let error = result["errorcode"]{
                        let errocode = (error as AnyObject).description
                        if errocode == "0" {
                            let message = result["message"] as? String
                            completion(true,result,message)
                        } else {
                          //  Banner.shared.showBanner(title: nil, subtitle: message, style: .info)
                            completion(false,result,message)
                        }
                    }
                case .failure(let error):
                    print("========================= Debug Print:- Printing Error ======================== \n StatusCode :\(response.response?.statusCode ?? -111) \n Error discription :\(error.localizedDescription) \n ========================= ======================== ========================")
                    GeneralAlert.shared.generalAlert(controller: self.controller, message: error.localizedDescription, actionTitle: "Retry", alertStyle: .alert) {
                        self.excuteRequest(completion: completion)
                    }
                }
            }
        }
    }
    
    
    func makePostString(_ parameters:Parameters?) -> String {
        var str = "?"
        guard let params = parameters else {
            return ""
        }
        for (key,value) in params {
            str.append("\(key)=\(value)&")
        }
        str.removeLast()
        return str
    }
    
    deinit {
        print("deinitilized")
    }
    
    func makeHTTPHeader(_ headerFields:[String:Any]?) -> HTTPHeaders? {
        guard let headers = headerFields else {
            return nil
        }
        var headerField:HTTPHeaders = []
        for (key,value) in headers {
            let header = HTTPHeader(name: key, value: value as? String ?? "")
            headerField.add(header)
            
        }
        print("header make :\(headerField)")
        return headerField
    }
    
    fileprivate func makeHeaderField(_ header: [String : Any]?) -> [String:Any]? {
//        if SessionHandler.sessionToken == nil {
//            return nil
//        } else {
//            var _tempHeader:[String:Any] = [:]
//
//            if let header_fields = header {
//                for (key,value) in header_fields  {
//                    _tempHeader[key] = value
//                }
//            }
//            return _tempHeader
//        }
        return nil
    }
    
}

enum Encoding : RawRepresentable {
    
    typealias RawValue = ParameterEncoding
    
    case url
    case json
    
    init?(rawValue:ParameterEncoding) {
        switch rawValue {
        default: return nil
        }
    }
    
    var rawValue: RawValue {
        switch self {
        case .url:
            return URLEncoding.default
        case .json:
            return JSONEncoding(options: [])
        }
    }
    
}

