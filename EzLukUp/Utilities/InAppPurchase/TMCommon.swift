//
//  TMSettingsManager.swift
//  TaskMario
//
//  Created by Mohamed Shafi on 13/02/19.
//  Copyright © 2019 Appzoc. All rights reserved.
//

import UIKit

typealias AlertButtonActionHandler = (UIAlertAction) -> Void

class TMCommon: NSObject {
    
    static let sharedInstance = TMCommon()
    
    func showAlertwithCompletion(message:String, controller:UIViewController, okButtonAction: AlertButtonActionHandler?)
    {
        let alert = UIAlertController(title: "ChavaraMatrimony", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            okButtonAction
            
        }()))
        
        controller.present(alert, animated: true, completion:nil)

    }
    
    func showConfirmationAlert(_ message: String, onViewController viewController: UIViewController?, withCancelButtonTitle cancelButtonTitle: String, withOkButtonTitle okButtonTitle: String,withCancelButtonAction cancelButtonAction: AlertButtonActionHandler?,withOkButtonAction okButtonAction: AlertButtonActionHandler?) {
        
        if UIApplication.shared.keyWindow == nil {
            return }
        let alert = UIAlertController(title: "ChavaraMatrimony", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: cancelButtonTitle, style: UIAlertAction.Style.destructive, handler: {
            cancelButtonAction
        }()))
        alert.addAction(UIAlertAction(title: okButtonTitle, style: UIAlertAction.Style.default, handler: {
            okButtonAction
            
        }()))
        viewController?.present(alert, animated: true, completion:nil)
    }
    
    /*
     Get all pending in-app transaction
     */
    class func addPendingTrasaction(recieptData:NSData){
        
        let userDefault   = UserDefaults.standard
        let receiptString = recieptData.base64EncodedString()
        
        if var pendingList = userDefault.object(forKey: "RecieptPendingList") as? [String] {
            
            pendingList.append(receiptString)
            userDefault.set(pendingList, forKey: "RecieptPendingList")
            userDefault.synchronize()
            print("\ntest default array \(pendingList)")
            
        } else {
            
            var pendingList:[String] = []
            pendingList.append(receiptString)
            userDefault.set(pendingList, forKey: "RecieptPendingList")
            userDefault.synchronize()
        }
    }
    
    class func addPendingTrasactionDict(recieptDict:Dictionary<String,Any>){
        
        let userDefault    = UserDefaults.standard
//        let receiptString  = recieptData.base64EncodedString()
        
        if var pendingList = userDefault.object(forKey: "RecieptPendingList") as? [Dictionary<String,Any>] {
            
            pendingList.append(recieptDict)
            userDefault.set(pendingList, forKey: "RecieptPendingList")
            userDefault.synchronize()
            print("\ntest default array \(pendingList)")
        } else {
            var pendingList:[Dictionary<String,Any>] = []
            pendingList.append(recieptDict)
            userDefault.set(pendingList, forKey: "RecieptPendingList")
            userDefault.synchronize()
        }
    }

    
    /*
     Remove all pending transaction from user default
     */
    class func removePendingTrasaction(receiptKey:Dictionary<String, Any>){
        
        let userDefault    = UserDefaults.standard
        
        let paymentId = receiptKey["paymentId"]
        
        if let pendingList:[Dictionary<String, Any>] = userDefault.object(forKey: "RecieptPendingList") as? [Dictionary<String, Any>] {
            
            let notfoundItems = pendingList.filter {($0["paymentId"] as! String) != ( paymentId as! String) }
            
            userDefault.set(notfoundItems, forKey:"RecieptPendingList")
            userDefault.synchronize()
        }
    }
    
    /*
     Get all pending in-app Transaction
     */
    class func getPendingTrasactionDict() -> [Dictionary<String,Any>]?
    {
        if let reciepDta = UserDefaults.standard.object(forKey: "RecieptPendingList")
        {
            return reciepDta as? [Dictionary]
        }
        else
        {
            return nil
        }
    }
    
    /*
     Get all pending in-app Transaction
     */
    class func getPendingTrasaction() -> [String]?
    {
        if let reciepDta = UserDefaults.standard.object(forKey: "RecieptPendingList")
        {
            return reciepDta as? [String]
        }
        else
        {
            return nil
        }
    }
    
}

