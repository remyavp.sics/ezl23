
import StoreKit

public typealias ProductIdentifier = String


public typealias ProductsRequestCompletionHandler   = (_ success: Bool, _ products: [SKProduct]?) -> ()
public typealias BuyProductCompleteResponseBlock    = (_ transcation:SKPaymentTransaction?)->()


public class IAPHelper : NSObject  {
    
    var productIdentifiers: Set<ProductIdentifier> = Set(){
        didSet{
            for productIdentifier in productIdentifiers {
                let purchased = UserDefaults.standard.bool(forKey: productIdentifier)
                if purchased {
                    purchasedProductIdentifiers.insert(productIdentifier)
                    print("Previously purchased: \(productIdentifier)")
                } else {
                    print("Not purchased: \(productIdentifier)")
                }
            }
        }
    }
    
    var purchasedProductIdentifiers = Set<ProductIdentifier>()
    
    var productsRequest: SKProductsRequest?
    var productsRequestCompletionHandler: ProductsRequestCompletionHandler?
    var buyProductCompleteBlock: BuyProductCompleteResponseBlock?
    
    
    static let IAPHelperPurchaseNotification = "IAPHelperPurchaseNotification"
    
    static let shared:IAPHelper = IAPHelper()
    
    private override init() {
        super.init()
    }
    
//    public init(productIds: Set<ProductIdentifier>) {
//        self.productIdentifiers = productIds
//
//        for productIdentifier in productIds {
//            let purchased = UserDefaults.standard.bool(forKey: productIdentifier)
//            if purchased {
//                purchasedProductIdentifiers.insert(productIdentifier)
//                print("Previously purchased: \(productIdentifier)")
//            } else {
//                print("Not purchased: \(productIdentifier)")
//            }
//        }
//
//        super.init()
//
//    }
    
}

// MARK: - StoreKit API

extension IAPHelper {
    
    public func requestProducts(completionHandler: @escaping ProductsRequestCompletionHandler) {
        productsRequest?.cancel()
        productsRequestCompletionHandler = completionHandler
        
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest?.delegate = self
        productsRequest?.start()
    }
    
    public func buyProduct(product: SKProduct,completionHandler:@escaping BuyProductCompleteResponseBlock) {
        
        
        
       // SKPaymentQueue.default().add(self)
        
        buyProductCompleteBlock = completionHandler
        
        
        print("Buying \(product.productIdentifier)...")
        let payment = SKPayment(product: product)
        
        
        
        SKPaymentQueue.default().add(payment)
    }
    
    public func isProductPurchased(productIdentifier: ProductIdentifier) -> Bool {
        return purchasedProductIdentifiers.contains(productIdentifier)
    }
    
    public class func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    
    public func restorePurchases(completionHandler:@escaping BuyProductCompleteResponseBlock) {
        
        self.buyProductCompleteBlock = completionHandler;
        //SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}

// MARK: - SKProductsRequestDelegate

extension IAPHelper: SKProductsRequestDelegate {
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("Loaded list of products...")
        
        let responseProducts = response.products.sorted {
            $0.price.compare($1.price) == ComparisonResult.orderedDescending
        }
        
        
        let products = responseProducts
        productsRequestCompletionHandler?(true, products)
        clearRequestAndHandler()
        
        for p in products {
            print("Found product: \(p.productIdentifier) \(p.localizedTitle) \(p.price.floatValue)")
        }
    }
    
    public func request(_ request: SKRequest, didFailWithError error: Error) {
      
        DispatchQueue.main.async {
            let delegate = UIApplication.shared.delegate as! AppDelegate
          //  delegate.loadingIndicator.stop()
        }
        print("Failed to load list of products.")
        print("Error: \(error.localizedDescription)")
        productsRequestCompletionHandler?(false, nil)
        clearRequestAndHandler()
    }
    
    private func clearRequestAndHandler() {
        productsRequest = nil
        productsRequestCompletionHandler = nil
    }
}

// MARK: - SKPaymentTransactionObserver

extension IAPHelper: SKPaymentTransactionObserver {
    
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch (transaction.transactionState) {
            case .purchased:
                completeTransaction(transaction: transaction)
                break
            case .failed:
                failedTransaction(transaction: transaction)
                break
            case .restored:
                restoreTransaction(transaction: transaction)
                break
            case .deferred:
                print("deferred!")
                break
            case .purchasing:
                print("Purchasing!")
                
                break
            }
        }
    }
    
    private func completeTransaction(transaction: SKPaymentTransaction) {
        print("completeTransaction...")
        
        print("recordTransaction                           *************** completeTransaction  \(transaction.transactionIdentifier)")
        print("recordTransaction                           *************** completeTransaction \(transaction.transactionState)")
        
        
        recordTransaction(transaction: transaction)
        
        deliverPurchaseNotificatioForIdentifier(identifier: transaction.payment.productIdentifier)
        SKPaymentQueue.default().finishTransaction(transaction)
        
        if((self.buyProductCompleteBlock) != nil)
        {
            buyProductCompleteBlock!(transaction);
        }
        
       // SKPaymentQueue.default().remove(self)
        
    }
    
    private func restoreTransaction(transaction: SKPaymentTransaction) {
        guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }
        
        print("restoreTransaction... \(productIdentifier)")
        deliverPurchaseNotificatioForIdentifier(identifier: productIdentifier)
        SKPaymentQueue.default().finishTransaction(transaction)
        
        if((self.buyProductCompleteBlock) != nil)
        {
            print(" buyProductCompleteBlock restoreTransaction... \(productIdentifier)")
            buyProductCompleteBlock!(transaction);
        }
        
    }
    
    private func failedTransaction(transaction: SKPaymentTransaction) {
        print("failedTransaction...")
        
             print("Transaction Error: \(transaction.error?.localizedDescription)")
        
        SKPaymentQueue.default().finishTransaction(transaction)
        
        if((self.buyProductCompleteBlock) != nil)
        {
            buyProductCompleteBlock!(nil);
        }
        
       // SKPaymentQueue.default().remove(self)
        
    }
    
    public func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        
        print("paymentQueueRestoreCompletedTransactionsFinished   ------------  **************************************");
        
        if((self.buyProductCompleteBlock) != nil)
        {
            buyProductCompleteBlock!(nil);
        }
        
        if(queue.transactions.count>0)
        {
            let transaction:SKPaymentTransaction = queue.transactions[0];
            recordTransaction(transaction: transaction)
        }
        
       // SKPaymentQueue.default().remove(self)
    }
    
    public func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error){
        
        print("Transaction Error: \(error.localizedDescription)")
        
    
        IndicatorOverlay.hideActivityIndicator()
        
        
       // SKPaymentQueue.default().remove(self)
    }
    
    
    private func deliverPurchaseNotificatioForIdentifier(identifier: String?) {
        guard let identifier = identifier else { return }
        
        purchasedProductIdentifiers.insert(identifier)
        UserDefaults.standard.set(true, forKey: identifier)
        UserDefaults.standard.synchronize()
        // NSNotificationCenter.defaultCenter().postNotificationName(IAPHelper.IAPHelperPurchaseNotification, object: identifier)
    }
}

func recordTransaction(transaction:SKPaymentTransaction)
{
    if Bundle.main.appStoreReceiptURL == nil {
        print("No app store receipt URL exists!")
        return
    }
    let receiptData = NSData(contentsOf: Bundle.main.appStoreReceiptURL!)
    //let receiptDict
    if receiptData == nil {
        print("There was an error encoding the receipt data!")
        return
    }
    
    let receiptString = receiptData!.base64EncodedString()
    
    /// Save into UserDefault
    
    TMCommon.addPendingTrasaction(recieptData: receiptData!);
    
    //validateInappPurchaseReceipt(recpData: receiptString)
    
    // validateReciept(receiptString)
}

func validateReciept(receiptBase64:NSString)
{
    print("Validate Reciept                 ************** \(receiptBase64)")
    
    validateReceipt(appStoreReceiptURL: Bundle.main.appStoreReceiptURL as NSURL?) { (success: Bool) -> Void in
        print(success)
        
    }
    
}

private func receiptData(appStoreReceiptURL : NSURL?) -> NSData? {
    
    guard let receiptURL = appStoreReceiptURL,
        let receipt = NSData(contentsOf: receiptURL as URL) else {
            return nil
    }
    
    do {
        let receiptData = receipt.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let requestContents = ["receipt-data" : receiptData]
        let requestData = try JSONSerialization.data(withJSONObject: requestContents, options: [])
        return requestData as NSData?
    }
    catch let error as NSError {
        print(error)
    }
    
    return nil
}


public func validateReceipt(appStoreReceiptURL : NSURL?, onCompletion: (Bool) -> Void) {
    
    //    validateReceiptInternal(appStoreReceiptURL, isProd: false) { (statusCode: Int?) -> Void in
    //        guard let status = statusCode else {
    //            onCompletion(false)
    //            return
    //        }
    //    }
}

func validateInappPurchaseReceipt(recpData:String)
{
    print("validateInappPurchaseReceipt      -------------------------*************-----------------")
    
   /* let webService = VVWebServiceManager()
    webService.ValidateInappPurchaseReceipt(receipt:recpData, completionHandler: { (status:Bool, responseObject:AnyObject?, message:String?, error:NSError?) in
        
        let thread = ThreadHandler()
        
        thread.executeInMainThread {
            if status
            {
                let coinBalance: Int            = LBUserManager.sharedInstance.coinamount
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CoinUpdateNotification"), object: coinBalance)
                print("Success")
                LBCommon.removePendingTrasaction(receiptKey: recpData)
            }
            else{
                print("Fail")
            }
        }
    })  */
    
}
