//
//  ActivityLoader.swift
//  WayToNikkah
//
//  Created by Appzoc on 13/02/19.
//  Copyright © 2019 Mohamed Shafi. All rights reserved.
//

import UIKit

class ActivityLoader {
    
    static let shared = ActivityLoader()
    
    private init(){}
    
    private let blackView = UIView()
    private let activityIndicator = UIActivityIndicatorView()

    private let ballRiseView:NVActivityIndicatorView = {
        let frame = CGRect(x: 0, y: 0, width: 80, height: 60)
        return NVActivityIndicatorView(frame: frame, type: .ballPulse, color: UIColor.red, padding: nil)
    }()
    
    private func addBlackView() {
        DispatchQueue.main.async {
            if let window = UIApplication.shared.keyWindow {
                self.blackView.frame = window.frame
                self.blackView.backgroundColor = UIColor(white: 0, alpha: 0)
                self.ballRiseView.center = window.center
                self.blackView.addSubview(self.ballRiseView)
                window.addSubview(self.blackView)
                self.startAnimation()
            }
            UIApplication.shared.beginIgnoringInteractionEvents()
        }
    }
    
    private func startAnimation() {
        self.ballRiseView.startAnimating()
    }
    
    private func stopAnimation() {
        self.ballRiseView.stopAnimating()
    }
    
    func startActivityLoader() {
        DispatchQueue.main.async {
            self.addBlackView()
        }
    }
    
    func StopActivityLoader() {
        DispatchQueue.main.async {
            self.stopAnimation()
            self.blackView.removeFromSuperview()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
 
     func autoDismissLoader(dismissTime: Double,  andPresentOn viewController: UIViewController) {
        
        DispatchQueue.main.async {
            self.startActivityLoader()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + dismissTime){
                self.StopActivityLoader()
            }
        }
        
    }
    
    deinit {
        print("remove loader")
    }
}
