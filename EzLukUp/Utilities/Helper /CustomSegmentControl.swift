//
//  CustomSegmentControl.swift
//  tabbar demo
//
//  Created by Farhan on 12/06/19.
//  Copyright © 2019 Farhan. All rights reserved.
//

import UIKit

class CustomSegmentControl: UIControl  {
    
    private var buttons = [UIButton]()
    private var selector:UIView!
    var selectedSegmentIndex = 0
    private var width:CGFloat = 0
    private var selectedBTN : UIButton!
    
    @IBInspectable
    var titlefontSize : CGFloat = 12
    
    @IBInspectable
    var selectorHeight : CGFloat = 3
    
    @IBInspectable
    var isfullSizeSelector : Bool = false {
        didSet {
            self.selectorHeight = self.isfullSizeSelector ? self.frame.height : selectorHeight
        }
    }
    
    @IBInspectable
    var borderWidth : CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var borderColor : UIColor = .lightGray {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    var roundCorner : Bool = false {
        didSet {
            if roundCorner {
                layer.cornerRadius = frame.height / 2
                selector.layer.cornerRadius = frame.height / 2
            } else {
                layer.cornerRadius = 0
                selector.layer.cornerRadius = 0
            }
            updateSegmentView()
        }
    }
    
    @IBInspectable
    var segmentTitlesSeperatedByCommas : String = "" {
        didSet {
            updateSegmentView()
        }
    }
    
    @IBInspectable
    var textColor : UIColor = .lightGray {
        didSet {
            updateSegmentView()
        }
    }
    
    @IBInspectable
    var selectorColor : UIColor = .darkGray {
        didSet {
            updateSegmentView()
        }
    }
    
    @IBInspectable
    var selectorTextColor : UIColor = .white {
        didSet {
            updateSegmentView()
        }
    }
    
    // fill = 0
    // fillEqually = 1
    // fillpropotionally = 2
    private var distributionType:UIStackView.Distribution = .fill {
        didSet {
            updateSegmentView()
        }
    }
    
    @IBInspectable
    var distribution : Int = 0 {
        didSet {
            switch distribution {
            case 1:
                distributionType = .fillEqually
            case 2:
                distributionType = .fillProportionally
            default:
                distributionType = .fill
            }
        }
    }
    
    @IBInspectable var shadowOffset:CGSize{
        get{
            let currentOffset = self.layer.shadowOffset
            // let offset:(width:Int,height:Int) = (Int(currentOffset.width),Int(currentOffset.height))
            return currentOffset
        }
        set{
            self.layer.shadowOffset = newValue
            
        }
    }
    
    @IBInspectable var shadowColor:UIColor{
        get{
            return UIColor(cgColor: self.layer.shadowColor ?? UIColor.clear.cgColor)
        }
        
        set{
            self.layer.shadowColor = newValue.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity:Float{
        get{
            return self.layer.shadowOpacity
        }
        set{
            self.layer.shadowOpacity = newValue
        }
        
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return self.layer.shadowRadius
        }
        set {
            self.layer.shadowRadius = newValue
        }
    }
    
    
    private func updateSegmentView() {
        buttons.removeAll()
        subviews.forEach({$0.removeFromSuperview()})
        
        let segments = segmentTitlesSeperatedByCommas.components(separatedBy: ",")
        for segment in segments {
            let segmentBTN = UIButton(type: .system)
            segmentBTN.setTitle(segment, for: .normal)
//            segmentBTN.titleLabel?.font = UIFont.regularFontWithSize(titlefontSize)
            segmentBTN.setTitleColor(textColor, for: .normal)
            segmentBTN.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
            buttons.append(segmentBTN)
        }
        
        buttons[0].setTitleColor(selectorTextColor, for: .normal)
        self.selectedBTN = buttons.first
        
        // embedd in stack view to gave equal width
        let embbedButtonStack = UIStackView(arrangedSubviews: buttons)
        embbedButtonStack.axis = .horizontal
        embbedButtonStack.alignment = .fill
        embbedButtonStack.distribution = distributionType
        addSubview(embbedButtonStack)
        embbedButtonStack.translatesAutoresizingMaskIntoConstraints = false
        embbedButtonStack.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        embbedButtonStack.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        embbedButtonStack.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        embbedButtonStack.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        // let eachSegmentWidth = frame.width / CGFloat(buttons.count)
       // let eachSegmentWidth = buttons[0].frame.width
        if isfullSizeSelector {
            selector = UIView(frame: CGRect(x: 0, y: 0, width: width, height: selectorHeight))
        } else {
            selector = UIView(frame: CGRect(x: 0, y: frame.height - 3, width: width, height: selectorHeight))
        }
        
        selector.backgroundColor = selectorColor
     //   addSubview(selector)
        self.insertSubview(selector, at: 0)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateSegmentView()
    }
    
    @objc private func buttonTapped(_ sender : UIButton){
        for (index,btn) in buttons.enumerated() {
            btn.setTitleColor(textColor, for: .normal)
            
            if btn == sender {
                selectedSegmentIndex = index
                btn.setTitleColor(selectorTextColor, for: .normal)
                selectedBTN = btn
                width = selectedBTN.frame.width
                let selectorPosition = btn.frame.origin.x
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
                    self.selector.frame.size.width = self.width
                    self.selector.frame.origin.x = selectorPosition
                }, completion: nil)
            }
        }
        sendActions(for: .valueChanged)
    }
    
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
        if self.isfullSizeSelector {
            selector.layer.cornerRadius = 0
        } else {
            selector.layer.cornerRadius = selector.frame.height / 2
        }
       
        width = selectedBTN.frame.width
        self.selector.frame.size.width = width
     }
}
