//
//  DFToggle.swift
//  DayToFresh
//
//  Created by farhan k on 12/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import Foundation

protocol Togglable {
    mutating func toggle()
}

enum OnOffSwitch: Togglable {
    case off, on
    mutating func toggle() {
        switch self {
        case .off:
            self = .on
        case .on:
            self = .off
        }
    }
}
