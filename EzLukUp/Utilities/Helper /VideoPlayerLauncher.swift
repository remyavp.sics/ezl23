//
//  VideoPlayerLauncher.swift
//  DayToFresh
//
//  Created by farhan k on 06/05/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import AVKit

class VideoPlayerLauncher : NSObject , AVPlayerViewControllerDelegate {
    
    private var controller:UIViewController!
    private var url:String?
    private var player:AVPlayer?
    private let playerViewController = AVPlayerViewController()
    
    init(contoller:UIViewController,url:String?) {
        super.init()
        self.controller = contoller
        self.playerViewController.delegate = self
        self.url = url
    }
    
    func openPlayer() {
        if let videoURL = URL(string: url ?? "") {
            self.player = AVPlayer(url: videoURL)
            playerViewController.player = player
            controller.present(playerViewController, animated: true) {
                self.playerViewController.player!.play()
            }
        } 
    }
    

    
    deinit {
        print("Deinitilizing VideoPlayer Launcher")
    }
    
}
