//
//  WTNImagePickerManager.swift
//  WayToNikkah
//
//  Created by Farhan on 13/08/19.
//  Copyright © 2019 Mohamed Shafi. All rights reserved.
//

import UIKit
import Photos


protocol WTNImagePickerDelegate {
    func onFinishingPickingImage(With image:UIImage)
}

class WTNImagePicker : NSObject,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    private var viewController:UIViewController = UIViewController()
    private var imagePicker:UIImagePickerController = UIImagePickerController()
    private var wTNImagePickerDelegate:WTNImagePickerDelegate?
    
    init(_ VC:UIViewController) {
        //self.imagePicker.delegate = self
        self.viewController = VC
        self.wTNImagePickerDelegate = self.viewController as? WTNImagePickerDelegate
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.allowsEditing = false
        
    }
    
    func PresentingImagePickerController(){
        self.imagePicker.delegate = self
        self.checkPermission()
    }
    
    func presentingPicker(){
       // print("image delegate \(String(describing: imagePicker.delegate))")
        print("WTNImagePickerDelegate :\(String(describing: wTNImagePickerDelegate))")
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Upload Photo", message: "Select source for uploading photo", preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.darkGreen
            alert.addAction(UIAlertAction(title: "Camera", style: .default
                , handler: { (camera) in
                    if UIImagePickerController.isSourceTypeAvailable(.camera) {
                        self.imagePicker.sourceType = .camera
                        self.viewController.present(self.imagePicker, animated: true, completion: nil)
                    } else {
                        let _alert  = UIAlertController(title: "Warning", message: "You don't have camera , Please Select another option", preferredStyle: .alert)
                        _alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: { _ in
                           // self.viewController.dismiss(animated: true, completion: nil)
                           // self.viewController.navigationController?.popViewController(animated: true)
                        }))
                        self.viewController.present(_alert, animated: true, completion: nil)
                    }
            }))
            alert.addAction(UIAlertAction(title: "Photos", style: .default, handler: { (photoLib) in
                self.imagePicker.sourceType = .photoLibrary
                self.viewController.present(self.imagePicker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.viewController.present(alert, animated: true, completion: nil)
        }
    }
   private func checkPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            print("Access is granted by user")
            self.presentingPicker()
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    /* do stuff here */
                    print("success")
                    self.presentingPicker()
                }
            })
            print("It is not determined until now")
        case .restricted:
            // same same
            print("User do not have access to photo album.")
        case .denied:
            // same same
         
            print("User has denied the permission.")
            let alert = UIAlertController(title: "Allow access to your photos",
                                              message: "This lets you share from your camera roll and enables other features for photos and videos. Go to your settings and tap \"Photos\".",
                                              preferredStyle: .alert)
                
                let notNowAction = UIAlertAction(title: "Not Now",
                                                 style: .cancel,
                                                 handler: nil)
                alert.addAction(notNowAction)
                
                let openSettingsAction = UIAlertAction(title: "Open Settings",
                                                       style: .default) { [unowned self] (_) in
                    // Open app privacy settings
                    gotoAppPrivacySettings()
                }
                alert.addAction(openSettingsAction)
                
            self.viewController.present(alert, animated: true, completion: nil)
        case .limited:
            
            print("")
            
        }
    }
    // Mark:- Image PIcker Delegate
    
    func gotoAppPrivacySettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(url) else {
                assertionFailure("Not able to open App privacy settings")
                return
        }

        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
      if let pickedEditedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
        print("pickedEditedImage")
         self.wTNImagePickerDelegate?.onFinishingPickingImage(With: pickedEditedImage)
      } else if let pickedOrginalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
        print("pickedOrginalImage")
         self.wTNImagePickerDelegate?.onFinishingPickingImage(With: pickedOrginalImage)
        }
        picker.dismiss(animated: false, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
