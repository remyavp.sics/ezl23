//
//  OTCommon.swift
//  OTM
//
//  Created by Shafi on 05/05/17.
//  Copyright © 2017 Shafi. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

let dateFormatFromAPI  = "yyyy-MM-dd'T'HH:mm:ss.SSS"


class WNCommon: NSObject {
    
    typealias AlertButtonActionHandler = (UIAlertAction) -> Void

    static let sharedInstance : WNCommon = {
        let instance = WNCommon()
        return instance
    }()
    
    static func is6PlusNormal() -> Bool{
        
        var  status:Bool = false
        
        if(UIScreen.main.bounds.size.height == 736.0)
        {
            status = true
        }
        return status
    }
    
    static func is6Normal() -> Bool{
        
        var  status:Bool = false
        
        if(UIScreen.main.bounds.size.height == 667.0)
        {
            status = true
        }
        else if(UIScreen.main.bounds.size.height == 667.0 && UIScreen.main.nativeScale < UIScreen.main.scale)
        {
            status = true
        }
        return status
    }
    
    static func is5Normal() -> Bool{
        
        var  status:Bool = false
        
        if(UIScreen.main.bounds.size.height == 568.0)
        {
            status = true
        }
        else if(UIScreen.main.bounds.size.height == 568.0 && UIScreen.main.nativeScale < UIScreen.main.scale)
        {
            status = true
        }
        return status
    }
    
    class func isiPad() -> Bool {
        // device type.
        if UIDevice.current.userInterfaceIdiom == .pad {
           return true
        }else {
            return false
        }

    }
    
    func showAlertwithCompletionMethod(message:String, controller:UIViewController, completion: @escaping (UIAlertAction)-> Void)
    {
        let alert = UIAlertController(title: "OTM", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: completion))
        controller.present(alert, animated: true, completion: nil)
    }
    
    func getInstantiatedViewControllerWithIdentifier(_ identifier:String, fromStoryBoard storyboard:String) -> UIViewController
    {
        let storyboardRef = UIStoryboard(name:storyboard, bundle: nil)
        let viewcontroller = storyboardRef.instantiateViewController(withIdentifier: identifier)
        return viewcontroller
    }
    
    func showAlert(message:String, controller:UIViewController)
    {
        let alert = UIAlertController(title: "OTM", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
    func showAlertMessage(message:String)
    {
        let window = UIApplication.shared.keyWindow
        let controller = window?.rootViewController
        
        if(controller != nil)
        {
            let alert = UIAlertController(title: "OTM", message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            controller?.present(alert, animated: true, completion: nil)
        }
    }
    
    func showInBannerNetworkOfflineErrorMessage() {
        
    }
    
    func color(red r:CGFloat,green g:CGFloat,blue b:CGFloat,alpha a:CGFloat) -> UIColor {
        
        return UIColor(red:r/255.0, green: g/255.0, blue: b/255.0, alpha:a)
    }
    
    func documentDirectory()->String
    {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        return documentsPath
    }
    
    static func libraryDirectory()->String
    {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0]
        return documentsPath
    }
    
    static func libraryDirectory(dirName:String)->URL
    {
        let documentsDirectory = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).first!
        _ = documentsDirectory.appendingPathComponent(dirName, isDirectory: true)
        return documentsDirectory
    }

    
    func showAlertwithCompletion(message:String, controller:UIViewController, completion:@escaping ()-> Void)
    {
        let alert = UIAlertController(title: "OTM", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        controller.present(alert, animated: true, completion: {
            completion()
        })
    }
    
    func showConfirmationAlert(_ message: String, onViewController viewController: UIViewController?, withCancelButtonTitle cancelButtonTitle: String, withOkButtonTitle okButtonTitle: String,withCancelButtonAction cancelButtonAction: AlertButtonActionHandler?,withOkButtonAction okButtonAction: AlertButtonActionHandler?) {
        
        if UIApplication.shared.keyWindow == nil {
            return }
        let alert = UIAlertController(title: "OTM", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: cancelButtonTitle, style: UIAlertAction.Style.default, handler: {
            cancelButtonAction
        }()))
        alert.addAction(UIAlertAction(title: okButtonTitle, style: UIAlertAction.Style.default, handler: {
            okButtonAction
            
        }()))
        
        viewController?.present(alert, animated: true, completion:nil)
    }
    
    
    func getWindowFrameOfView(_ view:UIView, viewController controller:UIViewController) -> CGRect
    {
        var overflowCount:Int = 35
        var finalFrame:CGRect = view.frame
        var superView:UIView = view
        
        while overflowCount > 0
        {
            if let sv = superView.superview
            {
                superView = sv
                finalFrame.origin.x += superView.frame.origin.x
                finalFrame.origin.y += superView.frame.origin.y
                
                if superView.isKind(of: UICollectionView.self)
                {
                    finalFrame.origin.x = finalFrame.origin.x - (superView as! UICollectionView).contentOffset.x
                    finalFrame.origin.y = finalFrame.origin.y - (superView as! UICollectionView).contentOffset.y
                }
            }
            else
            {
                break
            }
            
            overflowCount = overflowCount - 1
        }
        
        if let _ = controller.navigationController
        {
            finalFrame.origin.y = finalFrame.origin.y + 65.0
        }
        
        return finalFrame
    }
    
    func getWindowFrameOfView(_ view:UIView) -> CGRect
    {
        var overflowCount:Int = 35
        var finalFrame:CGRect = view.frame
        var superView:UIView = view
        
        while overflowCount > 0
        {
            if let sv = superView.superview
            {
                superView = sv
                finalFrame.origin.x += superView.frame.origin.x
                finalFrame.origin.y += superView.frame.origin.y
                
                if superView.isKind(of: UICollectionView.self)
                {
                    finalFrame.origin.x = finalFrame.origin.x - (superView as! UICollectionView).contentOffset.x
                    finalFrame.origin.y = finalFrame.origin.y - (superView as! UICollectionView).contentOffset.y
                }
            }
            else
            {
                break
            }
            
            overflowCount = overflowCount - 1
        }
        
        return finalFrame
    }
    
    
    static func updateUserCredentialsIntoUserDefualt(info:NSDictionary?) {
        
        if let dict:NSDictionary = info{
            UserDefaults.standard.set(dict, forKey: "Credentials")
        }
        else{
            UserDefaults.standard.set(nil, forKey: "Credentials")
        }
        UserDefaults.standard.synchronize()
    }
    
    static func getCredetilasFromUserDefualt() -> NSDictionary? {
        
        
        if let infoDict:NSDictionary = UserDefaults.standard.value(forKey: "Credentials") as? NSDictionary
        {
            return infoDict
        }
        else{
            return nil
        }
        
    }
    
    func createDirectory(folderNam:String)->String{
        
        
        let libpath           =  NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0] as NSString
        let filePath          = libpath.appendingPathComponent(folderNam)
        
        print("filePath = \(filePath)")
        
        var isDir : ObjCBool = false
        
        let documentsPath = NSURL(fileURLWithPath:filePath)
        
        let fileManager = FileManager.default
        
        if(fileManager.fileExists(atPath: filePath, isDirectory:&isDir)==false)
        {
            do {
                try FileManager.default.createDirectory(at: documentsPath as URL, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                NSLog("Unable to create directory \(error.debugDescription)")
            }
        }
        else
        {
            if (isDir.boolValue==true){
                return filePath
            }
            else{
                
            }
        }
        return filePath
    }
    
    func getDiretoryPath(folderNam:String)->String
    {
        let libpath           =  NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0] as NSString
        let filePath          = libpath.appendingPathComponent(folderNam)
        
        print("filePath = \(filePath)")
        return filePath
    }
    
    
    
    
    func libDir() -> NSString{
        let libpath           =  NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0] as NSString
        
        return libpath
    }
    
    
    
    
    func getStringFrom(date:Date?) -> String? {
       
        if let dateValue = date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS" //date format style getting from API
            return dateFormatter.string(from: dateValue)
        }
        return nil
   }
    
    //MARK:- UserInfo
    func saveUserInfoOnStatus(withRootObject Object:Any){

        let encodedData = NSKeyedArchiver.archivedData(withRootObject: Object)
        UserDefaults.standard.set(encodedData, forKey: "LoginData")

    }
//    func saveUserInfo(isLoggedIn:Bool){
//        var Object = Dictionary<String,Any>()
//        if isLoggedIn{
//            Object = WTNUserInfo.sharedInstance.toData()
//        } else {
//            Object = ["userId":""]
//        }
//        print("========================\nSaving User Info :\(isLoggedIn)\n========================")
//        let encodedData = NSKeyedArchiver.archivedData(withRootObject: Object)
//        UserDefaults.standard.set(encodedData, forKey: "LoginData")
//    }
    
    func isAccountForgotted() -> Bool {
        let isAccountForgotted = UserDefaults.standard.bool(forKey: "isForgot")
        return isAccountForgotted
    }
    
    func forgotAccount(isForgot:Bool) {
        print("========================\nisForgoting Acoount : \(isForgot)\n========================")
        UserDefaults.standard.set(isForgot, forKey: "isForgot")
    }
    
    
//    func checkUserisLoggedIn() -> Bool {
//        
//        guard let LoginDataFromUserdefaults = UserDefaults.standard.value(forKey: "LoginData") as? Data else {
//            return false
//        }
//        let decodedDataLoginData = NSKeyedUnarchiver.unarchiveObject(with: LoginDataFromUserdefaults ) as? [String:Any]
//        userData = WTNUserInfo.sharedInstance.getUserData(fromData: decodedDataLoginData ?? [:])
//        if userData.userId != "" {
//            print("user is logged in the id is \(userData.toData())")
//            return true
//        } else {
//            print("user Is not logged")
//            return false
//        }
//    }
    
    func isNetworkAvailable(presentOn VC:UIViewController,CompletionHandler Handler: @escaping() -> ()) -> Bool {
        if !NetworkReachabilityManager()!.isReachable {
            print("Failure :- No Internet")
            BaseAlert.completionAlert(withTitle: "No InterNet", message: "Please connect to internet", tintColor: .black, andPresentOn: VC) {
                Handler()
            }
            return false
        }
        return true
    }
    
    class var hasTopNotch: Bool {
        if #available(iOS 11.0 , *) {
            // with notch: 44.0 on iPhone X, XS, XS Max, XR.
            // without notch: 24.0 on iPad Pro 12.9" 3rd generation, 20.0 on iPhone 8 on iOS 12+.
            return UIApplication.shared.delegate?.window??.safeAreaInsets.bottom ?? 0 > 24
        }
        return false
    }
    
     func heightOfBottom(viewController:UIViewController) -> CGFloat {
        var bottomSafeArea: CGFloat
        var safeHeight : CGFloat = 0.0
        if #available(iOS 11.0, *) {
            bottomSafeArea = viewController.view.safeAreaInsets.bottom
            safeHeight =  bottomSafeArea > 0 ? bottomSafeArea : 0
            return safeHeight
        } else {
            bottomSafeArea = viewController.bottomLayoutGuide.length
            safeHeight =  bottomSafeArea > 0 ? bottomSafeArea : 0
            return safeHeight
        }
    }
}

/*
 var topSafeArea: CGFloat
 var bottomSafeArea: CGFloat
 
 if #available(iOS 11.0, *) {
 topSafeArea = view.safeAreaInsets.top
 bottomSafeArea = view.safeAreaInsets.bottom
 } else {
 topSafeArea = topLayoutGuide.length
 bottomSafeArea = bottomLayoutGuide.length
 
 */



extension String {
    
    func validateEmail() -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isNullValue() -> Bool {
        return (self.count)==0 ?true:false
    }
}

//MARK:- CLASS: ThreadHandler -> To Handle mutithreaded code execution

typealias BlockHandler = () -> Void

enum ThreadType
{
    case serial
    case concurrent
}

class ThreadHandler:NSObject
{
    //MARK:- Delay methods - especially to invoke a method in main thread with delay.
    
    func addDelayInSeconds(_ seconds:Double, delayhandler:@escaping BlockHandler)
    {
        DispatchQueue.main.asyncAfter(deadline: gcdTime(timeInSeconds: seconds)) {
            
            delayhandler()
        }
    }
    
    //MARK:- Multi-Threading - Task to Thread dispatcher
    
    /* dispatch_sync only return after the block is finished whereas dispatch_async return after it is added to the queue and may not finished. */
    
    func executeInMainThread(_ handler:@escaping BlockHandler)
    {
        if Thread.isMainThread
        {
            handler()
        }
        else
        {
            DispatchQueue.main.async(execute: { () -> Void in
                
                handler()
            })
        }
    }
    
    func executeInAsynchronousThread(_ handler:@escaping BlockHandler) -> Void {
        
        let queue = DispatchQueue(label: "com.vivo", qos: DispatchQoS.userInitiated, attributes: .concurrent)
        queue.async {
            
            handler()
        }
    }
    
    
    func executeInAsynchronousConcurrentThreadWithIdentifier(threadIdentifier threadId:String, handler:@escaping BlockHandler) -> Void {
        
        let asyncQueue = DispatchQueue(label: threadId, attributes: DispatchQueue.Attributes.concurrent)
        
        asyncQueue.async {
            
            handler()
        }
    }
    
    func executeInAsynchronousSerialThreadWithIdentifier(threadIdentifier threadId:String, handler:@escaping BlockHandler) -> Void {
        
        let asyncQueue = DispatchQueue(label: threadId, attributes: [])
        
        asyncQueue.async {
            
            handler()
        }
    }
    
    func executeInSynchronousSerialThreadWithIdentifier(threadIdentifier threadId:String, handler:BlockHandler) -> Void {
        
        let asyncQueue = DispatchQueue(label: threadId, attributes: [])
        
        asyncQueue.sync {
            
            handler()
        }
    }
    
    //MARK:- GCD timing methods
    
    func gcdTime(timeInSeconds time:Double) -> DispatchTime {
        
        return DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
    }
    
    

    
}

extension Date{

    func utcDate() -> Date
    {
        let format = DateFormatter()
        format.dateFormat = "yyyy/MM/dd HH:mm:ss"
        format.timeZone = TimeZone(abbreviation:"UTC")
        let utcDateStr = format.string(from: self)
        //format.timeZone = TimeZone.current
        print("format.string(from: self) == \(format.string(from: self))")
        let utcDate = format.date(from: utcDateStr)
        return utcDate!
    }

     func getStringFromDate() -> String? {
        
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormatFromAPI //date format style getting from API
            return dateFormatter.string(from: self)
    }

}


extension String {
    
    func URLEncodedString() -> String? {
        let escapedString = self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        return escapedString
  }
    
    func getDateFromString() -> Date? {
        
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormatFromAPI //date format style getting from API
            return dateFormatter.date(from:self)
    }
    
    func localized(withComment:String) -> String {
            return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: withComment)
    }

    

}

