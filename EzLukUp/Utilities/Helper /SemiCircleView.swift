//
//  SemiCircleView.swift
//  WayToNikkah
//
//  Created by Farhan on 08/08/19.
//  Copyright © 2019 Mohamed Shafi. All rights reserved.
//

import UIKit

class SemiCircleView: UIView {
    
    var path = UIBezierPath()
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        makeSemiCircle()
    }
 
    
    func makeSemiCircle(){
        path = UIBezierPath(arcCenter: CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2),
                            radius: self.frame.size.height/2,
                            startAngle: CGFloat(180.0).toRadians(),
                            endAngle: CGFloat(0.0).toRadians(),
                            clockwise: true)
        
       
        let l = CAShapeLayer()
        l.path = path.cgPath
        l.fillColor = UIColor.white.cgColor
        l.shadowColor = UIColor.lightGray.cgColor
//        l.lineWidth = 1
//        l.strokeColor = UIColor.lightGray.cgColor
        l.shadowOpacity = 0.5
        l.shadowOffset = CGSize(width: 1, height: 0)
        l.shadowRadius = 3
        layer.addSublayer(l)
        
    }

}



// MARK: - To convert Degree to Radians
extension CGFloat {
    func toRadians() -> CGFloat {
        return self * .pi / 180.0
    }
}
