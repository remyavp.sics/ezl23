//
//  GetGeneralProfileModel.swift
//  EzLukUp
//
//  Created by REMYA V P on 17/01/23.


import Foundation

class getGeneralProfileResponse{
    var status : Bool?
    var Data : getGeneralDataModel?
    var RecommendedProviders : [getrecommendedProvidersModel] = []
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let dataarray = data["data"] as? [String:Any]{
           self.Data = getGeneralDataModel(fromData: dataarray)
        }
        if let recommendedlist = data["recommendedProviders"] as? [[String:Any]]{
            for recommended in recommendedlist{
                self.RecommendedProviders.append(getrecommendedProvidersModel(fromData: recommended))
            }
        }
    }
}

class getGeneralDataModel{
    var id : String?
    var name : String?
    var phoneNumber : String?
    var contactImage : String?
    var isUser : Bool?
    var city : String?
    var stateShortCode : String?
    var countryCode : String?
    var areaOfService : [GareaOfService] = []
    var contactUserId : contactUserIdList?
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.name = data["name"] as? String
        self.phoneNumber = data["phoneNumber"] as? String
        self.contactImage = data["contactImage"] as? String
        self.city = data["city"] as? String
        self.stateShortCode = data["stateShortCode"] as? String
        self.countryCode = data["countryCode"] as? String
        self.isUser = data["isUser"] as? Bool ?? false
        if let dataarray = data["contactUserId"] as? [String:Any]{
            self.contactUserId = contactUserIdList(fromData: dataarray)
        }
        if let catlist = data["areaOfService"] as? [[String:Any]]{
            for cat in catlist{
                self.areaOfService.append(GareaOfService(fromData: cat))
            }
        }
    }
}

class GareaOfService{
    var city : String?
    var stateShortCode : String?
    init(fromData data: [String:Any]) {
    self.city = data["city"] as? String
    self.stateShortCode = data["stateShortCode"] as? String
    }
}

class contactUserIdList{
    var fullName : String?
    var profilePic : String?
    var city : String?
    var countryCode : String?
    init(fromData data: [String:Any]) {
    self.fullName = data["fullName"] as? String
    self.profilePic = data["profilePic"] as? String
    self.city = data["city"] as? String
    self.countryCode = data["countryCode"] as? String
    }
}

class getrecommendedProvidersModel{
    var sectionOpened : Bool = true
    var id : String?
    var name : String?
    var categoryImage : String?
    var contacts : [contactsList] = []
    init(fromData data: [String:Any]) {
    self.id = data["_id"] as? String
    self.name = data["name"] as? String
    self.categoryImage = data["categoryImage"] as? String
    if let contactlist = data["contacts"] as? [[String:Any]]{
        for contact in contactlist{
            self.contacts.append(contactsList(fromData: contact))
        }
      }
   }
}

class contactsList{
    var sectionOpened : Bool = true
    var id : String?
    var userId : String?
    var name : String?
    var phoneNumber : String?
    var isUser : Bool?
    var contactUserId :[recomcontactUserIdlist] = []
    var countryCode : String?
    var twilioCarrierName : String?
    var twilioCarrierType : String?
    var city : String?
    var stateShortCode : String?
    var areaOfService :[proareaOfServicelist] = []
    var recomended :[recomendedList] = []
    var contactImage : String?
    init(fromData data: [String:Any]) {
    self.id = data["_id"] as? String
    self.userId = data["userId"] as? String
    self.name = data["name"] as? String
    self.phoneNumber = data["phoneNumber"] as? String
    self.isUser = data["isUser"] as? Bool ?? false
    self.twilioCarrierName = data["twilioCarrierName"] as? String
    self.twilioCarrierType = data["twilioCarrierType"] as? String
    self.city = data["city"] as? String
    self.stateShortCode = data["stateShortCode"] as? String
        self.countryCode = data["countryCode"] as? String
      if let contactuseridlist = data["contactUserId"] as? [[String:Any]]{
              for contact in contactuseridlist{
                  self.contactUserId.append(recomcontactUserIdlist(fromData: contact))
              }
          }
      if let recomendedcontactlist = data["recomended"] as? [[String:Any]]{
            for contact in recomendedcontactlist{
                self.recomended.append(recomendedList(fromData: contact))
            }
        }
        if let proareaOfServicelistt = data["areaOfService"] as? [[String:Any]]{
              for contact in proareaOfServicelistt{
                  self.areaOfService.append(proareaOfServicelist(fromData: contact))
              }
          }
        
        self.contactImage = data["contactImage"] as? String
   }
}

class recomcontactUserIdlist{
    var id : String?
    var fullName : String?
    var refPhoneNumber : String?
    var city : String?
    var countryCode : String?
    var phoneNumber : String?
    var profilePic : String?
    var state : String?
    init(fromData data: [String:Any]) {
    self.id = data["_id"] as? String
        self.fullName = data["fullName"] as? String
        self.refPhoneNumber = data["refPhoneNumber"] as? String
        self.city = data["city"] as? String
        self.countryCode = data["countryCode"] as? String
        self.phoneNumber = data["phoneNumber"] as? String
        self.profilePic = data["profilePic"] as? String
        self.state = data["state"] as? String
    }
    
}

class proareaOfServicelist{
    var city : String?
    var stateShortCode : String?
    init(fromData data: [String:Any]) {
        self.city = data["city"] as? String
        self.stateShortCode = data["stateShortCode"] as? String
    }
}

class recomendedList{
    var id :String?
    var userId :[userIdList] = []
    var name :String?
    var contactImage :String?
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.name = data["name"] as? String
        self.contactImage = data["contactImage"] as? String
        if let userlist = data["user"] as? [[String:Any]]{
              for contact in userlist{
                  self.userId.append(userIdList(fromData: contact))
              }
          }
    }
}

class userIdList{
    var fullName : String?
    var profilePic : String?
    init(fromData data: [String:Any]) {
        self.fullName = data["fullName"] as? String
        self.profilePic = data["profilePic"] as? String
    }
}
