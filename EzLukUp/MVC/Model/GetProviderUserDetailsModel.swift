//
//  GetProviderUserDetailsModel.swift
//  EzLukUp
//
//  Created by Srishti Innovative on 07/04/23.
//

import Foundation

class getProviderUserDetailsResponse{
    var status : Bool?
    var Data : getProviderUserDataModel?
    var Feedback : [getFeedbackModel] = []
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let dataarray = data["data"] as? [String:Any]{
            self.Data = getProviderUserDataModel(fromData: dataarray)
        }
        if let feedbacklist = data["feedback"] as? [[String:Any]]{
            for feedbacks in feedbacklist{
                self.Feedback.append(getFeedbackModel(fromData: feedbacks))
            }
        }
    }
}

class getProviderUserDataModel{
    var id : String?
    var fullName : String?
    var refPhoneNumber : String?
    var countryCode : String?
    var phoneNumber : String?
    var type : String?
    var city : String?
    var state : String?
    var formattedPhoneNumber : String?
    var aboutBusiness : String?
    var isSignup : Bool?
    var areaOfService : [areaOfServicelist] = []
    var categoryIds : [categorieslist] = []
    var serviceDetails : [String]?
    var profilePic : String?
    var operationalHours : String?
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.fullName = data["fullName"] as? String
        self.refPhoneNumber = data["refPhoneNumber"] as? String
        self.countryCode = data["countryCode"] as? String
        self.phoneNumber = data["phoneNumber"] as? String
        self.type = data["type"] as? String
        self.city = data["city"] as? String
        self.state = data["state"] as? String
        self.formattedPhoneNumber = data["formattedPhoneNumber"] as? String
        self.aboutBusiness = data["aboutBusiness"] as? String
        self.operationalHours = data["operationalHours"] as? String ?? ""
        self.isSignup = data["isSignup"] as? Bool ?? false
        if let arealist = data["areaOfService"] as? [[String:Any]]{
            for area in arealist{
                self.areaOfService.append(areaOfServicelist(fromData: area))
            }
        }
        if let catlist = data["categoryIds"] as? [[String:Any]]{
            for cat in catlist{
                self.categoryIds.append(categorieslist(fromData: cat))
            }
        }
        self.serviceDetails = data["serviceDetails"] as? [String]
        self.profilePic = data["profilePic"] as? String
    }
}
