//
//  FAQModel.swift
//  EzLukUp
//
//  Created by REMYA V P on 09/03/23.
//

import Foundation

class FAQ_Base{
    var status : Bool?
    var Data : [FAQ_Data] = []
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let datas = data["data"] as? [[String:Any]]{
            for dataarray in datas{
                self.Data.append(FAQ_Data(from: dataarray))
            }
        }
    }
}

class FAQ_Data{
    var id : String?
    var question : String?
    var answer : String?
    init(from data : [String:Any]){
        self.id = data["_id"] as? String
        self.question = data["title"] as? String
        self.answer = data["description"] as? String
   }
}




