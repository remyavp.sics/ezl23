//
//  EditProviderContactDetails.swift
//  EzLukUp
//
//  Created by REMYA V P on 30/01/23.
//

import Foundation

class EditProviderContactDetailsResponse{
    var status : Bool?
    var Data : getDataDetailsModel?
    var Feedback : getEditFeedbackModel?
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let dataarray = data["data"] as? [String:Any]{
            self.Data = getDataDetailsModel(fromData: dataarray)
        }
        if let feedbackarray = data["feedback"] as? [String:Any]{
            self.Feedback = getEditFeedbackModel(fromData: feedbackarray)
        }
    }
}

class getDataDetailsModel{
    var id : String?
    var name : String?
    var countryCode : String?
    var phoneNumber : String?
    var twilioCallerName : String?
    var categoryIds : [Categorylists] = []
    init(fromData data: [String:Any]) {
    self.id = data["_id"] as? String
    self.name = data["name"] as? String
    self.countryCode = data["countryCode"] as? String
    self.phoneNumber = data["phoneNumber"] as? String
    self.twilioCallerName = data["twilioCallerName"] as? String
        if let catlist = data["categoryIds"] as? [[String:Any]]{
            for cat in catlist{
                self.categoryIds.append(Categorylists(fromData: cat))
            }
        }
    }
}

class Categorylists{
    var id : String?
    var name : String?
    var categoryImage : String?
    init(fromData data: [String:Any]) {
    self.id = data["_id"] as? String
    self.name = data["name"] as? String
    self.categoryImage = data["categoryImage"] as? String
}
}

class getEditFeedbackModel{
    var id : String?
    var feedback : String?
    init(fromData data: [String:Any]) {
    self.id = data["_id"] as? String
    self.feedback = data["feedback"] as? String
    }
}


class getRecommendedCountModel{
    var recomendCount : Int?
    var status : Bool?
    init(fromData data: [String:Any]) {
    self.recomendCount = data["recomendCount"] as? Int
    self.status = data["feedback"] as? Bool ?? false
    }
}
