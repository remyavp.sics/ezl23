//
//  GetProviderProfileModel.swift
//  EzLukUp
//
//  Created by REMYA V P on 17/01/23.
//

import Foundation

class getProviderProfileResponse{
    var status : Bool?
    var Data : getProviderDataModel?
    var Feedback : [getFeedbackProvidersModel] = []
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let dataarray = data["data"] as? [String:Any]{
            self.Data = getProviderDataModel(fromData: dataarray)
        }
        if let feedbacklist = data["feedback"] as? [[String:Any]]{
            for feedback in feedbacklist{
                self.Feedback.append(getFeedbackProvidersModel(fromData: feedback))
            }
        }
    }
}

class getProviderDataModel{
    var id : String?
    var name : String?
    var contactImage : String?
    var phoneNumber : String?
    
    var isUser : Bool?
    var contactUserId : ContactUserIdList?
    var categoryIds : [Categorieslist] = []
    var areaOfService : [PareaOfService] = []
    var city : String?
    var stateShortCode : String?
    var twilioCallerName : String?
    var countryCode : String?
  //  var profilePic : String?
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.name = data["name"] as? String
        self.contactImage = data["contactImage"] as? String
        self.phoneNumber = data["phoneNumber"] as? String
        
        self.isUser = data["isUser"] as? Bool ?? false
        self.city = data["city"] as? String
        self.stateShortCode = data["stateShortCode"] as? String
        self.twilioCallerName = data["twilioCallerName"] as? String
        self.countryCode = data["countryCode"] as? String
      
        if let dataarray = data["contactUserId"] as? [String:Any]{
            self.contactUserId = ContactUserIdList(fromData: dataarray)
        }
        if let catlist = data["categoryIds"] as? [[String:Any]]{
            for cat in catlist{
                self.categoryIds.append(Categorieslist(fromData: cat))
            }
        }
        if let catlist = data["areaOfService"] as? [[String:Any]]{
            for cat in catlist{
                self.areaOfService.append(PareaOfService(fromData: cat))
            }
        }
    }
}

class PareaOfService{
    var city : String?
    var stateShortCode : String?
    init(fromData data: [String:Any]) {
    self.city = data["city"] as? String
    self.stateShortCode = data["stateShortCode"] as? String
    }
}

class ContactUserIdList{
    var id : String?
    var fullName : String?
    var phoneNumber : String?
    var countryCode : String?
    var city : String?
    var state : String?
    var aboutBusiness : String?
    var companyName : String?
    var categoryIds : [Categorieslist] = []
    var serviceDetails : [ServiceList] = []
    var profilePic : String?
    var operationalHours : String?
    var firstLevelConnections : [String]?
    var secondLevelConnections : [String]?
    var thirdLevelConnections : [String]?
    init(fromData data: [String:Any]) {
    self.id = data["_id"] as? String
    self.fullName = data["fullName"] as? String
    self.phoneNumber = data["phoneNumber"] as? String
    self.countryCode = data["countryCode"] as? String
    self.city = data["city"] as? String
    self.state = data["state"] as? String
    self.city = data["city"] as? String
    self.aboutBusiness = data["aboutBusiness"] as? String
        self.operationalHours = data["operationalHours"] as? String ?? ""
        self.companyName = data["companyName"] as? String ?? ""
        self.firstLevelConnections = data["firstLevelConnections"] as? [String]
        self.secondLevelConnections = data["secondLevelConnections"] as? [String]
        self.thirdLevelConnections = data["thirdLevelConnections"] as? [String]
        if let catlist = data["categoryIds"] as? [[String:Any]]{
            for cat in catlist{
                self.categoryIds.append(Categorieslist(fromData: cat))
            }
        }
        if let servicelist = data["serviceDetails"] as? [[String:Any]]{
            for service in servicelist{
                self.serviceDetails.append(ServiceList(fromData: service))
            }
        }
    }
}

class Categorieslist{
    var id : String?
    var name : String?
    var categoryImage : String?
    init(fromData data: [String:Any]) {
    self.id = data["_id"] as? String
    self.name = data["name"] as? String
    self.categoryImage = data["categoryImage"] as? String
}
}

class ServiceList{
    var url : String?
    var title : String?
    var description : String?
    init(fromData data: [String:Any]) {
        self.url = data["url"] as? String
        self.title = data["title"] as? String
        self.description = data["description"] as? String
    }
    
}

class getFeedbackProvidersModel{
    var id : String?
    var providerId : String?
    var userId : userIdlist?
    var feedback : String?
    var createdAt : String?
    var type : String?
    var categoryIds : String?
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.providerId = data["providerId"] as? String
        if let userarray = data["userId"] as? [String:Any]{
            self.userId = userIdlist(fromData: userarray)
        }
        self.feedback = data["feedback"] as? String
        self.createdAt = data["createdAt"] as? String
        self.type = data["type"] as? String
        self.categoryIds = data["categoryIds"] as? String
    }
}

class userIdlist{
    var id : String?
    var fullName : String?
    var profilePic : String?
    var type : String?
    var firstLevelConnections : [String]?
    var secondLevelConnections : [String]?
    var thirdLevelConnections : [String]?
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.fullName = data["fullName"] as? String
        self.profilePic = data["profilePic"] as? String
        self.type = data["type"] as? String
        self.firstLevelConnections = data["firstLevelConnections"] as? [String]
        self.secondLevelConnections = data["secondLevelConnections"] as? [String]
        self.thirdLevelConnections = data["thirdLevelConnections"] as? [String]
    }
}
