//
//  ProviderContactProfileVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 01/12/22.
//

import UIKit
import SwiftUI
import Kingfisher
import Alamofire
import IPImage
import Contacts

//var checkfeedback = ""

class ProviderContactProfileVC: UIViewController, UITextFieldDelegate {
    
    //MARK: - Outlets
    @IBOutlet weak var collectionBTN: BaseButton!
    @IBOutlet weak var collectionIMG: UIImageView!
    @IBOutlet weak var profileIMG: UIImageView!
    @IBOutlet weak var connectionLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var catview1: BaseView!
    @IBOutlet weak var catview2: BaseView!
    @IBOutlet weak var catview3: BaseView!
    
    @IBOutlet weak var stackviewCat: UIStackView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    
    @IBOutlet weak var stackview: UIStackView!
    @IBOutlet weak var serviceBTNView: BaseView!
    @IBOutlet weak var infoBTNView: BaseView!
    @IBOutlet weak var feedbackBTNView: BaseView!
    @IBOutlet weak var servicesLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var feedbackLbl: UILabel!
    
    @IBOutlet weak var containerView1: UIView!
    @IBOutlet weak var containerView2: UIView!
    @IBOutlet weak var containerView3: UIView!
    @IBOutlet weak var collectionPopupView: UIView!
    @IBOutlet weak var collectionviewBlur: UIView!
    
    @IBOutlet weak var stackviewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var searchpopupview: BaseView!
    @IBOutlet weak var searchblurview: UIView!
    
    var getuserdetails : getProviderProfileResponse?
    var selectedBtn  = 0 // if 0 selected is servicesbtn , 1- infobtn selected , 2- feedbackbtn selected
    var pickedimage :UIImage?
    var categorycount = 0
    var selecteduserid = "" // selected contactid from contactmainVC
    var getname = ""
    var getmobile = ""
    
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
    
    let contactuserId : String = UserDefaults.standard.value(forKey: "KcontactuserId") as? String ?? ""
    var mobnumofprovider = ""
    var checkedbtnstatus : Bool = false
    
    var selectedcontactid = ""
    var checkfeedback = ""
    var checkconsufeedback = ""
    var checkrecomprofile = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        userdetailsapi()
        checkfeedreportdata()
        checkrecomprofiledata()
        NotificationCenter.default.addObserver(self, selector: #selector(self.goToHomeTab), name: Notification.Name("hometab"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userdetailsapi()
        if checkcollectionstatus == "fromsearch"{
            self.searchpopupview.isHidden = true
            self.searchblurview.isHidden = true
            self.collectionIMG.image = UIImage(named: "collection_icon")
        }else{
            collectionPopupView.isHidden = true
            collectionviewBlur.isHidden = true
            self.searchblurview.isHidden = true
            self.collectionIMG.image = UIImage(named: "collection_icon")
        }
    }
    
    //MARK: - SETUP_FUNCTIONS
    func setupUI(){
        selectedBtn = 0
        stackview.layer.cornerRadius = stackview.frame.height / 2
        infoBTNView.backgroundColor = UIColor.clear
        feedbackBTNView.backgroundColor = UIColor.clear
        servicesLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        feedbackLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        self.containerView1.isHidden = false
        self.containerView2.isHidden = true
        self.containerView3.isHidden = true
        self.collectionPopupView.isHidden = true
        self.collectionviewBlur.isHidden = true
        self.searchpopupview.isHidden = true
        searchblurview.isHidden = true
        
    }
    
    @objc func goToHomeTab(){
            navigationController?.popToRootViewController(animated: true)
        }
    
    func checkrecomprofiledata(){
        if checkconsufeedback == "fromprofile"{
            setupUI()
        }
    }
    
    func checkfeedreportdata(){
        print("chakka",checkfeedback)
        if checkconsufeedback == "fromdata"{
            selectedBtn = 2
            serviceBTNView.backgroundColor = UIColor.clear
            infoBTNView.backgroundColor = UIColor.clear
            feedbackBTNView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
            servicesLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
            infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
            feedbackLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
            self.containerView1.isHidden = true
            self.containerView2.isHidden = true
            self.containerView3.isHidden = false
            self.collectionPopupView.isHidden = true
            self.collectionviewBlur.isHidden = true
            self.searchpopupview.isHidden = true
        }
    }
    
    
    
}


//MARK: - @IBActions & functions
extension ProviderContactProfileVC {
    
    @available(iOS 14.0, *)
    @IBAction func backBTNAction(_ sender: UIButton) {
        self.nameLbl.text?.removeAll()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func collectionBlurBTN(_ sender: UIButton) {
        if collectionPopupView.isHidden{
            collectionPopupView.isHidden = false
            collectionviewBlur.isHidden = false
            self.collectionIMG.image = UIImage(named: "selectcollection")
            
        }
        else{
            collectionPopupView.isHidden = true
            collectionviewBlur.isHidden = true
            self.collectionIMG.image = UIImage(named: "collection_icon")
        }
        
//        if checkcollectionstatus == "fromsearch"{
//           // if searchpopupview.isHidden{
//                searchpopupview.isHidden = false
//                searchblurview.isHidden = false
//                self.collectionIMG.image = UIImage(named: "selectcollection")
////            }
////            else{
////                searchpopupview.isHidden = true
////                searchblurview.isHidden = true
////                self.collectionIMG.image = UIImage(named: "collection_icon")
////            }
//        }
//        else{
//          //if collectionPopupView.isHidden{
//                collectionPopupView.isHidden = false
//            collectionviewBlur.isHidden = false
//                searchblurview.isHidden = false
//                self.collectionIMG.image = UIImage(named: "selectcollection")
////            }
////            else{
////               collectionPopupView.isHidden = true
////                searchblurview.isHidden = true
////                self.collectionIMG.image = UIImage(named: "collection_icon")
////            }
//        }
    }
    
    
    @IBAction func collectionBTNTapped(_ sender: UIButton) {
        if checkcollectionstatus == "fromsearch"{

            searchpopupview.isHidden = false
        }else{
            collectionPopupView.isHidden = false
            collectionviewBlur.isHidden = false
        }
        
        if checkcollectionstatus == "fromsearch"{
           // if searchpopupview.isHidden{
                searchpopupview.isHidden = false
                searchblurview.isHidden = false
            self.collectionIMG.image = UIImage(named: "Collections")
//            }
//            else{
//                searchpopupview.isHidden = true
//                searchblurview.isHidden = true
//                self.collectionIMG.image = UIImage(named: "collection_icon")
//            }
        }
        else{
          //if collectionPopupView.isHidden{
                collectionPopupView.isHidden = false
            collectionviewBlur.isHidden = false
                searchblurview.isHidden = false
                self.collectionIMG.image = UIImage(named: "selectcollection")
//            }
//            else{
//               collectionPopupView.isHidden = true
//                searchblurview.isHidden = true
//                self.collectionIMG.image = UIImage(named: "collection_icon")
//            }
        }
    }
    
    @IBAction func serviceBTNTapped(_ sender: UIButton) {
        selectedBtn = 0
        infoBTNView.backgroundColor = UIColor.clear
        feedbackBTNView.backgroundColor = UIColor.clear
        serviceBTNView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        servicesLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        feedbackLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        self.containerView1.isHidden = false
        self.containerView2.isHidden = true
        self.containerView3.isHidden = true
        self.collectionPopupView.isHidden = true
        self.collectionviewBlur.isHidden = true
        self.searchpopupview.isHidden = true
    }
    
    @IBAction func infoBTNTapped(_ sender: UIButton) {
        selectedBtn = 1
        serviceBTNView.backgroundColor = UIColor.clear
        feedbackBTNView.backgroundColor = UIColor.clear
        infoBTNView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        servicesLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
        feedbackLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
        infoLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        self.containerView1.isHidden = true
        self.containerView2.isHidden = false
        self.containerView3.isHidden = true
        self.collectionPopupView.isHidden = true
        self.collectionviewBlur.isHidden = true
        self.searchpopupview.isHidden = true
    }
    
    
    @IBAction func feedbackBTNTapped(_ sender: UIButton) {
        selectedBtn = 2
        serviceBTNView.backgroundColor = UIColor.clear
        infoBTNView.backgroundColor = UIColor.clear
        feedbackBTNView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        servicesLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
        infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
        feedbackLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        self.containerView1.isHidden = true
        self.containerView2.isHidden = true
        self.containerView3.isHidden = false
        self.collectionPopupView.isHidden = true
        self.collectionviewBlur.isHidden = true
        self.searchpopupview.isHidden = true
    }
    
    
    //MARK:- PopupView Actions
    @IBAction func shareBTNTapped(_ sender: UIButton) {
        let contact = createContact(fname: self.nameLbl.text ?? "", lname: "", mob:  self.mobnumofprovider)
        
        do {
            try self.shareContacts(contacts: [contact])
        }
        catch {
            print("failed to share contact with some unknown reasons")
        }
    }
    //MARK: - share contact card from  menu
    func createContact(fname:String,lname:String,mob:String) -> CNContact {
        // Creating a mutable object to add to the contact
        let contact = CNMutableContact()
        contact.imageData = NSData() as Data // The profile picture as a NSData object
        contact.givenName = fname
        contact.familyName = lname
        contact.phoneNumbers = [CNLabeledValue(
            label:CNLabelPhoneNumberiPhone,
            value:CNPhoneNumber(stringValue:mob))]
        
        return contact
    }
   
    func shareContacts(contacts: [CNContact]) throws {
        
        guard let directoryURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else {
            return
        }
        
        var filename = NSUUID().uuidString
        
        // Create a human friendly file name if sharing a single contact.
        if let contact = contacts.first, contacts.count == 1 {
            
            if let fullname = CNContactFormatter().string(from: contact) {
                filename = fullname.components(separatedBy: " ").joined(separator: "")
            }
        }
        
        let fileURL = directoryURL
            .appendingPathComponent(filename)
            .appendingPathExtension("vcf")
        
        let data = try CNContactVCardSerialization.data(with: contacts)
        
        print("filename: \(filename)")
        print("contact: \(String(describing: String(data: data, encoding: String.Encoding.utf8)))")
        
        try data.write(to: fileURL, options: [.atomicWrite])
        
        let activityViewController = UIActivityViewController(
            activityItems: [fileURL],
            applicationActivities: nil
        )
        
        present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func feedbackPopupBTNTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "FeedBackVC") as! FeedBackVC
        vc.getcontactid = self.contactuserId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func reportBTNTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ReportVC") as! ReportVC
        vc.getcontactid = self.contactid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func editBTNTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "EditProviderScrollVC") as! EditProviderScrollVC
        vc.getcontactid = self.selecteduserid
        self.navigationController?.pushViewController(vc, animated: true)
    }
  

 //MARK:- Search Popup Actions
    @IBAction func searchblurBTN(_ sender: UIButton) {
        if checkcollectionstatus == "fromsearch"{
            self.searchblurview.isHidden = true
            searchpopupview.isHidden = true
            self.collectionIMG.image = UIImage(named: "collection_icon")

        }else{
            self.searchblurview.isHidden = true
            collectionPopupView.isHidden = true
            collectionviewBlur.isHidden = true
            self.collectionIMG.image = UIImage(named: "collection_icon")
        }
        
    }
    
    @IBAction func searchShareBTN(_ sender: UIButton) {
        let contact = createContact(fname: self.nameLbl.text ?? "", lname: "", mob: getmobile)
        
        do {
            try self.shareContacts(contacts: [contact])
        }
        catch {
            print("failed to share contact with some unknown reasons")
        }
    }
    
    
    @IBAction func searchSaveBTN(_ sender: UIButton) {
        savesearchcontactapi()
    }
    
    @IBAction func searchFeedbackBTN(_ sender: UIButton) {
      //  vc.getcontactid = self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].contactUserId ?? ""
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "FeedBackVC") as! FeedBackVC
        
        vc.getcontactid = self.contactuserId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func searchReportBTN(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ReportVC") as! ReportVC
        vc.getcontactid = self.contactid
        self.navigationController?.pushViewController(vc, animated: true)
    }
 
}


extension ProviderContactProfileVC{
    //MARK: - API Call
    func userdetailsapi(){
        let params = ["contactid": contactid] as [String : Any]
        print("dynamic contactid = \(contactid)")
        print("dynamic tocken = \(token)")
        print(params)
     
        let url = kBaseUrl+"getProviderContactProfileDetails"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<500) .responseJSON { response in
            
            switch (response.result) {
            case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata",responsedata)
                    self.getuserdetails = getProviderProfileResponse(from:responsedata)
                    
                    DispatchQueue.main.async {
                        // self.nameLbl.text = self.getuserdetails?.Data?.name ?? ""
        //Setup Name
                        var sndname = self.getuserdetails?.Data?.contactUserId?.fullName ?? ""
                        var twilioname = self.getuserdetails?.Data?.twilioCallerName ?? ""
                        var gname = self.getuserdetails?.Data?.name ?? ""
                        self.mobnumofprovider = self.getuserdetails?.Data?.phoneNumber ?? ""
                        var sname = ""
                        var tname = ""
                        if sndname != ""{
                            print("(\(sndname))")
                            sname = sndname
                        }else{
                            print("(\(twilioname))")
                            tname = twilioname
                        }
                        
                        if self.getuserdetails?.Data?.isUser == true{
                            self.nameLbl.text = "\(gname) (\((sname )))"
                        }else{
                            self.nameLbl.text = "\(gname) (\(tname ))"
                        }
                        
                        if gname == sname{
                            self.nameLbl.text = "\(gname)"
                        }else if gname == tname{
                            self.nameLbl.text = "\(gname)"
                        }
    
                        print(gname)
                        
       //Setup Image
                       // let ipimage = IPImage(text: self.nameLbl.text ?? "", radius: 30, font: UIFont(name: "Jost-Regular", size: 25), textColor: nil, randomBackgroundColor: false)
                        
                        if self.getuserdetails?.Data?.contactUserId?.profilePic == nil{
                            print("profile is empty")
                            self.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.getuserdetails?.Data?.contactImage ?? "")),placeholder: UIImage(named: "profileicon"))
                        }else{
                            print("profileimage")
                            self.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.getuserdetails?.Data?.contactUserId?.profilePic ?? "")))
                        }
                        
     //Setup Companyname
                        print(self.getuserdetails?.Data?.contactUserId?.companyName)
                        if self.getuserdetails?.Data?.contactUserId?.companyName == ""{
                            self.companyName.text = ""
                        }else{
                            self.companyName.text = "@ \(self.getuserdetails?.Data?.contactUserId?.companyName ?? "")"
                        }
                                                
     //Setup Category
                        if self.getuserdetails?.Data?.isUser == true{
                            //self.categorycount = self.getuserdetails?.Data?.contactUserId?.categoryIds.count ?? 0
                            self.label1.text = self.getuserdetails?.Data?.contactUserId?.categoryIds.first?.name ?? ""
                            print("categorycount",self.categorycount)
                           // self.categorySetup()
                        }else{
                            //self.categorycount = self.getuserdetails?.Data?.categoryIds.count ?? 0
                            self.label1.text = self.getuserdetails?.Data?.categoryIds.first?.name ?? ""
                            print("categorycount",self.categorycount)
                          //  self.categorySetup()
                        }
                        
     // MARK:-Setup Level of connections
                        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
                        
                        if self.getuserdetails!.Data!.contactUserId != nil && self.getuserdetails!.Data!.contactUserId!.firstLevelConnections!.contains(userid){
                            self.connectionLbl.text = "1st"
                               // cell.connectNameLbl.text = "1st"
                        }else if self.getuserdetails!.Data!.contactUserId != nil && self.getuserdetails!.Data!.contactUserId!.secondLevelConnections!.contains(userid){
                                    self.connectionLbl.text = "2nd"
                                       // cell.connectNameLbl.text = "1st"
                                        }
                        else if self.getuserdetails!.Data!.contactUserId != nil && self.getuserdetails!.Data!.contactUserId!.thirdLevelConnections!.contains(userid){
                                    self.connectionLbl.text = "3rd"
                                       // cell.connectNameLbl.text = "1st"
                                        }
                            else{
                                self.connectionLbl.text = ""
                            }
                        
                        
                        
                        
                        
                        
//                        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
//
//    // MARK:-Setup Level of connections
//                        if self.getuserdetails?.Data?.contactUserId != nil && ((self.getuserdetails?.Data?.contactUserId?.firstLevelConnections!.contains(userid)) != nil){
//                            self.connectionLbl.text = "1st"
//                        }
//                        else if self.getuserdetails?.Data?.contactUserId != nil && ((self.getuserdetails?.Data?.contactUserId?.secondLevelConnections?.contains(userid)) != nil){
//                            self.connectionLbl.text = "2nd"
//                        }
//                        else if self.getuserdetails?.Data?.contactUserId != nil && ((self.getuserdetails?.Data?.contactUserId?.thirdLevelConnections?.contains(userid)) != nil){
//                                    self.connectionLbl.text = "3rd"
//                                }
//                            else{
//                                self.connectionLbl.text = ""
//                            }
                        
                        
                    }
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
            
            
        }
    }
    
    //MARK: -SAVE Search Contact
       func savesearchcontactapi(){

//           let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjMyZGFiMDVhZGVkOWU3Mjc4MWFkMjY2IiwiZW1haWwiOiJndWVzdC5zd2VldHRyZWF0QGdtYWlsLmNvbSIsImlhdCI6MTY2NTQwNDk1Nn0.XZ_y_QbQ9PgovjlZcgtHqWN8oicgUnVeRqXtiLq26_8"
           
           
       let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
       let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
       
       let params: [String : Any] = [
                     "userid": userid,
                    //"userId":"63d7bae986e8dfe1c06e732b",
                     "contactid": contactid]
       
       print(params)
           let url = kBaseUrl+"saveSearchContact"
       // let url = "http://13.234.177.61/api7/saveSearchContact"
       AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            switch (response.result) {
               case .success( let JSON):
                   if let responsedata =  JSON as? [String:Any]  {
                     print("responsedata :",responsedata)
                       let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                           if responsedata["message"] as? String ?? "" == "Contact added successfully"{
//                               self.searchpopupview.isHidden = true
//                               self.searchblurview.isHidden = true
//                               self.collectionIMG.image = UIImage(named: "collection_icon")
                               
                           }else{
                               let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                           }
                           }
                                                     ))
                               self.present(alert, animated: true)
                   }
                   case .failure(let error):
                       print("Request error: \(error.localizedDescription)")
               }
           }
       }
    
}


//    func categorySetup(){
//        if self.getuserdetails?.Data?.isUser == false {
////            if categorycount >= 3  {
////                stackviewHeight.constant = 27 * 3
///
///
///
////                self.catview1.isHidden = false
////                self.catview2.isHidden = false
////                self.catview3.isHidden = false
////                self.label1.text = getuserdetails?.Data?.categoryIds[0].name
////                self.label2.text = getuserdetails?.Data?.categoryIds[1].name
////                self.label3.text = getuserdetails?.Data?.categoryIds[2].name
////            }else if categorycount == 2 {
//////                stackviewHeight.constant = 27 * 2
////                self.catview1.isHidden = false
////                self.catview2.isHidden = false
////                self.catview3.isHidden = true
////                self.label1.text = getuserdetails?.Data?.categoryIds[0].name
////                self.label2.text = getuserdetails?.Data?.categoryIds[1].name
////
////            }else if categorycount == 1 {
////                stackviewHeight.constant = 27 * 1
//                self.catview1.isHidden = false
////                self.catview2.isHidden = true
////                self.catview3.isHidden = true
//                self.label1.text = getuserdetails?.Data?.categoryIds[0].name
////            }else {
////               // stackviewHeight.constant = 0
////            }
//        }else{
////            if categorycount >= 3  {
//////                stackviewHeight.constant = 27 * 3
////                self.catview1.isHidden = false
////                self.catview2.isHidden = false
////                self.catview3.isHidden = false
////                self.label1.text = getuserdetails?.Data?.contactUserId?.categoryIds[0].name
////                self.label2.text = getuserdetails?.Data?.contactUserId?.categoryIds[1].name
////                self.label3.text = getuserdetails?.Data?.contactUserId?.categoryIds[2].name
////            }else if categorycount == 2 {
//////                stackviewHeight.constant = 27 * 2
////                self.catview1.isHidden = false
////                self.catview2.isHidden = false
////                self.catview3.isHidden = true
////                self.label1.text = getuserdetails?.Data?.contactUserId?.categoryIds[0].name
////                self.label2.text = getuserdetails?.Data?.contactUserId?.categoryIds[1].name
////
////            }else if categorycount == 1 {
////                stackviewHeight.constant = 27 * 1
//                self.catview1.isHidden = false
////                self.catview2.isHidden = true
////                self.catview3.isHidden = true
//                self.label1.text = getuserdetails?.Data?.contactUserId?.categoryIds[0].name
////            }else {
////               // stackviewHeight.constant = 0
////            }
//        }
//
//    }
