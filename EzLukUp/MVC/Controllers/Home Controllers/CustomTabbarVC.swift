//
//  CustomTabbarVC.swift
//  EzLukUp
//
//  Created by Srishti on 14/11/22.
//

import UIKit
import Contacts
import ContactsUI
import Alamofire


@available(iOS 14.0, *)
class CustomTabbarVC: UIViewController {
    var viewsub : ContactsmainVC!
    @IBOutlet weak var homeimg: UIImageView!
    @IBOutlet weak var searchimg: UIImageView!
    @IBOutlet weak var menuimg: UIImageView!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var menuViewblur: UIView!
    
    @IBOutlet weak var tabbarView: UIView!
    @IBOutlet weak var searchcontainer: UIView! //search page container
    @IBOutlet weak var homecontainer: UIView!// Dashboard page container
    @IBOutlet weak var ContactlistvcContainer: UIView!// Contactlist page container
    
    @IBOutlet weak var settingsContainer: UIView!// Settings page container
    @IBOutlet weak var faqContainer: UIView!
    @IBOutlet weak var feedbackToEzContainer: UIView!
    @IBOutlet weak var requestservicecontainer: UIView!
  
    
 //Outlets
    let store = CNContactStore()
    var syncContact : Bool = false
    var contacDictionary = [[String:Any]]()
    var tempArr : [[String:Any]] = []
    let uploadCount = 1500
    var contactParam = [[String:Any]]()
    var uploadIsFaild = 3
    var activityView: UIActivityIndicatorView?
    var contactsCount = 0
    var timer = Timer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuView.isHidden = true
        menuViewblur.isHidden = true
        homecontainer.isHidden = false
        searchcontainer.isHidden = true
        ContactlistvcContainer.isHidden = true
        settingsContainer.isHidden = true
        feedbackToEzContainer.isHidden = true
        faqContainer.isHidden = true
        requestservicecontainer.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "containerSegue" {
               let navController = segue.destination as! UINavigationController
               let secondVC = navController.viewControllers.first as! SearchVC
               navController.setViewControllers([secondVC], animated: false)
           }
    }
    
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//         if segue.identifier == "containerSegue" {
//                let navController = segue.destination as! UINavigationController
//                let secondVC = navController.viewControllers.first as! SearchVC
//                navController.setViewControllers([secondVC], animated: false)
//            }
//        }
    
    @IBAction func homeBTN(_ sender: UIButton) {
        homecontainer.isHidden = false
        searchcontainer.isHidden = true
        ContactlistvcContainer.isHidden = true
        settingsContainer.isHidden = true
        feedbackToEzContainer.isHidden = true
        faqContainer.isHidden = true
        menuView.isHidden = true
        menuViewblur.isHidden = true
        requestservicecontainer.isHidden = true
       
        NotificationCenter.default.post(name: Notification.Name("hometab"), object: nil)
        NotificationCenter.default.post(name: Notification.Name("dashapi"), object: nil)
        NotificationCenter.default.post(name: Notification.Name("fromsearch"), object: nil)
       
    }
    
    @IBAction func searchBTN(_ sender: UIButton) {
        homecontainer.isHidden = true
        searchcontainer.isHidden = false
        ContactlistvcContainer.isHidden = true
        settingsContainer.isHidden = true
        feedbackToEzContainer.isHidden = true
        faqContainer.isHidden = true
        requestservicecontainer.isHidden = true
        menuView.isHidden = true
        menuViewblur.isHidden = true
        if let navController = presentingViewController as? UINavigationController {
            navController.popToRootViewController(animated: true)
        }
        NotificationCenter.default.post(name: Notification.Name("searchtab"), object: nil)
    }
    
    @IBAction func menuBTN(_ sender: UIButton) {
        
        
        if menuView.isHidden == true{
            menuView.isHidden = false
            menuViewblur.isHidden = false
            tabbarView.isHidden = false
        }
        else{
            menuView.isHidden = true
            menuViewblur.isHidden = true
            tabbarView.isHidden = false
        }
//        Menucontainer.isHidden = true
    }
    
    @IBAction func menublrBTN(_ sender: UIButton) {
        if menuView.isHidden == true{
            menuView.isHidden = false
            menuViewblur.isHidden = false
            tabbarView.isHidden = false
        }
        else{
            menuView.isHidden = true
            menuViewblur.isHidden = true
            tabbarView.isHidden = false
        }
    }
    @IBAction func menucloseBTN(_ sender: UIButton) {
        menuView.isHidden = true
        menuViewblur.isHidden = true
        tabbarView.isHidden = false
    }
    
    
    
    @available(iOS 14.0, *)
    @IBAction func mBTN(_ sender: UIButton) {
        if sender.tag == 0{
            print(sender.tag)
            faqContainer.isHidden = false
            menuView.isHidden = true
            menuViewblur.isHidden = true
            settingsContainer.isHidden = true
            ContactlistvcContainer.isHidden = true
            feedbackToEzContainer.isHidden = true
            NotificationCenter.default.post(name: Notification.Name("fromsearch"), object: nil)
        }else if sender.tag == 1{
            print(sender.tag)
            feedbackToEzContainer.isHidden = false
            menuView.isHidden = true
            menuViewblur.isHidden = true
            faqContainer.isHidden = true
            ContactlistvcContainer.isHidden = true
            settingsContainer.isHidden = true
            NotificationCenter.default.post(name: Notification.Name("fromsearch"), object: nil)
        }else if sender.tag == 2{
            print(sender.tag)
            settingsContainer.isHidden = false
            menuView.isHidden = true
            menuViewblur.isHidden = true
            faqContainer.isHidden = true
            ContactlistvcContainer.isHidden = true
            feedbackToEzContainer.isHidden = true
            NotificationCenter.default.post(name: Notification.Name("fromsearch"), object: nil)
        }else if sender.tag == 3 {
            print(sender.tag)
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "RequestServiceVC") as! RequestServiceVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if sender.tag == 4 {
            print(sender.tag)
            DispatchQueue.main.async {
                            self.requestAccess { accessGranted in
                                print("contact request --",accessGranted)
                               // if accessGranted{
                                    DispatchQueue.global(qos: .background).async {
                                        self.getContactList()
                                        DispatchQueue.main.async {
                                            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.autopush), userInfo: nil, repeats: false)
                                        }
                                    }
                
                
                                //}
                
                            }
                
                        }
        }else if sender.tag == 5 { //contact BTn
            print(sender.tag)
            ContactlistvcContainer.isHidden = false
            menuView.isHidden = true
            menuViewblur.isHidden = true
            settingsContainer.isHidden = true
            faqContainer.isHidden = true
            feedbackToEzContainer.isHidden = true
           // vcinnavstack()
            NotificationCenter.default.post(name: Notification.Name("fromsearch"), object: nil)
        }
    }
    
    
    
    func vcinnavstack(){
        if let viewControllers = self.navigationController?.viewControllers
        {
            print("***",viewControllers)
            if viewControllers.contains(where: {
                return $0 is ContactsmainVC
            })
            {
                print("u r here")
            }
        }
    }
    
   
    
}
//dashboard codes
extension CustomTabbarVC{
    func getContactList(){
        self.contacDictionary.removeAll()
        let predicate = CNContact.predicateForContactsInContainer(withIdentifier:store.defaultContainerIdentifier())
        let contactz = try! store.unifiedContacts(matching: predicate, keysToFetch: [CNContactGivenNameKey as
                                                                                     CNKeyDescriptor,CNContactFamilyNameKey as CNKeyDescriptor ,CNContactPhoneNumbersKey as CNKeyDescriptor])
        contactsCount = contactz.count
        
        for contact in contactz {
            let name = "\(contact.givenName) \(contact.familyName)"
            var numbers: [String] = []
            for ph in contact.phoneNumbers{
                numbers.append(ph.value.stringValue)
            }
            let dictionaryObject: [String : Any] = [ "name": name, "phoneNumber": numbers]
            self.contacDictionary.append(dictionaryObject)
        }
       // print("before sorting",contacDictionary)
        let sortedArray = contacDictionary.sorted { ($0["name"] as! String) < ($1["name"] as! String) }
        //print("after sorting",sortedArray)
//        DispatchQueue.main.async {
//            self.viewOfCongrats.isHidden = false
//        }
        self.tempArr = sortedArray
//        DispatchQueue.global(qos: .background).async {
            self.syncAPIParameter()
//            DispatchQueue.main.async {
//                // Update the UI here
//            }
//        }
        
//        DispatchQueue.main.async {
//            print("Total of ",contactz.count," contacts found in your Phone list")
//            self.messageLbl.text = "Please wait while we finish syncing \(contactz.count) contacts from your phone book"
//        }
    }
    
    func syncAPIParameter() {
        self.uploadIsFaild = 3
        self.contactParam.removeAll()
        
        if self.tempArr.isEmpty { return }
        
        if self.tempArr.count <= self.uploadCount {
            self.contactParam = self.tempArr
            self.tempArr.removeAll()
            //  SVProgressHUD.dismiss()
            
            //                    DispatchQueue.main.async {
            //                        print("Total of ",self.contacDictionary.count,"has been synced")
            //                        self.messageLbl.text = "We have synced \(self.contacDictionary.count) contacts from your phone book"
            //                    }
        } else {
            self.contactParam = Array(self.tempArr.prefix(self.uploadCount))
            self.tempArr = Array(self.tempArr.dropFirst(self.uploadCount))
        }
        self.syncAPI(contacDictionary: self.contactParam)
    }
    
    
    //MARK: - Api call
    func syncAPI(contacDictionary: [[String:Any]] ){
        
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
        let countrycode : String = UserDefaults.standard.value(forKey: "Kcountrycode") as! String
        let params: [String : Any] = [
            "userId": userid,
            "countryCode": countrycode,
            "syncdata": contacDictionary
        ]
        print("params",params)
//        print("contactParam:",contactParam)
      //  let url = "http://13.234.177.61/api7/syncContact"
        let url = kBaseUrl+"syncContact"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            //             print("params:\(params)")
            //             print("response:\(response)")
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    syncContact = true
                    // print("syncContact :",syncContact)
                    DispatchQueue.main.async {
                        self.syncAPIParameter()
                    }
                    
                    
                    print("responsedata :",responsedata)
                    if responsedata["message"] as? String == "Contact sync successfully"{
                        //                        hideActivityIndicator()
                        //                        congrats.isHidden = false
                        //                        continueBtn.isUserInteractionEnabled = true
                        //                        continueBtn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
                        //                        DispatchQueue.main.async {
                        //                            self.messageLbl.text = "We have synced your \(contactsCount) contacts from your phone book"
                        //                        }
                        //                        showDefaultAlert(viewController: self, msg: responsedata["message"] as? String ?? "")
                    }
                    else{
                        hideActivityIndicator()
//                        showDefaultAlert(viewController: self, msg: responsedata["message"] as? String ?? "")
                    }
                }
            case .failure(let error):
                if self.uploadIsFaild > 0 {
                    self.syncAPI(contacDictionary: self.contactParam)
                }
                self.uploadIsFaild -= 1
                showDefaultAlert(viewController: self, msg: "Request error: \(error.localizedDescription)")
                print("Request error: \(error.localizedDescription)")
            }
        }
    }
    
    //MARK: - contact authorization
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            self.showSettingsAlert(completionHandler)
        case .restricted, .notDetermined:
            self.store.requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                } else {
                    DispatchQueue.main.async {
                        self.showSettingsAlert(completionHandler)
                    }
                }
            }
        }
    }
    //MARK: - private alert for granting access again from settings
    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)
        if
            let settings = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(settings) {
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
                completionHandler(false)
                UIApplication.shared.open(settings)
            })
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            completionHandler(false)
        })
        present(alert, animated: true)
    }
    
    @objc func autopush(){
        timer.invalidate()
    }

}

extension CustomTabbarVC{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }
    
    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
}





// pop back in viewcontroller
extension ViewController{
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
}

