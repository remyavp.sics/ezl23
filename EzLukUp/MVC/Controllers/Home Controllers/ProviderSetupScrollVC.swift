//
//  ProviderSetupScrollVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 14/07/23.
//

import UIKit
import Alamofire
import SwiftUI
import Kingfisher

var checkclose = ""

class ProviderSetupScrollVC: UIViewController {

    var SsearchArrRes = [getcatDataModel]()
    var Snewcatarray = [getcatDataModel]()
    var Scategorylist : getCategoriesResponse?
    var Sservices = [SerViceModel]()
    var Scityarray = [[String:Any]]()
    var Sstatearray = [[String:Any]]()
    var SstateListModel : SearchStateList?
    var SstateListArray = [StateList]()
    var SSearchstateList = [StateList]()
    var ScityList : SearchCityList?
    var ScityListArray = [CityList]()
    var SSearchcityList = [CityList]()
    var getcheckpnumdetails : getchecknumberResponse?
    
   //Otlets
    @IBOutlet weak var categoriesView: UIView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var closeCategoriesButton: UIButton!
    @IBOutlet weak var categoryTableView: UITableView!
    @IBOutlet weak var stateTBL: UITableView!
    @IBOutlet weak var cityTBL: UITableView!
    @IBOutlet weak var radiusTBL: UITableView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var searchIMG: UIImageView!
    @IBOutlet weak var categoriesTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var companyNameTF: UITextField!
    @IBOutlet weak var operationsTF: UITextField!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var stateDropview: UIView!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var cityDropview: UIView!
    @IBOutlet weak var detailsTxtView: UITextView!

    @IBOutlet weak var radiusTF: UITextField!
    @IBOutlet weak var dropIMG: UIImageView!
    @IBOutlet weak var radiusview: BaseView!
    @IBOutlet weak var radiusdropView: BaseView!
    @IBOutlet weak var finishBTN: UIButton!
    @IBOutlet weak var Popupview: BaseView!
    @IBOutlet weak var popupGotitBTN: BaseButton!
    
    
    
    //Variables
    var radiusArray = ["25","50","75","100"]
    var token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    var userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
    var getmobNum : String = UserDefaults.standard.value(forKey: "mynumber") as? String ?? ""
    var getCode : String = UserDefaults.standard.value(forKey: "Kcountrycode") as? String ?? ""
    var popupStatus = ""
    var latitude = "38.00000000"
    var longitude = "-97.00000000"
    var state = ""
    var city = ""
    var mobnumber = ""
    var name = ""
    var radius = "25 miles"
    var typedabout = ""
    
    var dropdownClicked = false
    var pickedimage :UIImage?
    var imagePicker = UIImagePickerController()
    var selectedProfileImage : UIImage!
    var imageData = Data()
    var imageStr = ""
    var fileName = ""
    var mimetype = ""
    var uploadedImageStr = ""
    var imageupload = ""
    var activityView: UIActivityIndicatorView?
    let defaults = UserDefaults.standard
    
    var EPSselectedcategoryID = [String]()
    var editCatID = String()
    var profileimage = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDetailsapi()
        self.getCategoryAPI()
        self.getstateAPI()
        setupUI()
    }
    
    func setupUI(){
        self.phoneTF.text = getmobNum
        //getDetailsapi()
        self.radiusview.isHidden = true
        self.Popupview.isHidden = true
        detailsTxtView.delegate = self
        detailsTxtView.text = "About Business"
        detailsTxtView.textColor = UIColor.lightGray
        detailsTxtView.returnKeyType = .done
        finishBTN.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
        popupGotitBTN.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
        dropShadowForView(view: Popupview)
        defaults.set("false", forKey: "poupvalue")
        popupStatus = defaults.string(forKey: "poupvalue") ?? ""

     //   print("popupStatus",popupStatus)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showpopuponClick(notification:)), name: Notification.Name("setup_pro_popupView"), object: nil)
    }
    
    @objc func showpopuponClick(notification: Notification) {
        print("popupStatus",popupStatus)
        if popupStatus == "false"{
            Popupview.isHidden = false
           // setupTableView.isUserInteractionEnabled = false
        }

    }
    
    @IBAction func radiusdropBTNAction(_ sender: UIButton) {
        self.radiusdropView.isHidden = !self.radiusdropView.isHidden
        
//        if dropdownClicked{
//            self.radiusdropView.isHidden = false
//            UIView.animate(withDuration: 0, animations: {
//                self.dropIMG.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
//            })
//
//        }else{
//            self.radiusdropView.isHidden = true
//            UIView.animate(withDuration: 0, animations: {
//                 self.dropIMG.transform = CGAffineTransform.identity
//            })
//        }
        
    }

    @IBAction func removeCategorySelectionBTNAction(_ sender:UIButton){
            self.categoryLabel.isHidden = true
            self.categoryLabel.text = ""
            self.categoriesTF.text = ""
            self.closeCategoriesButton.isHidden = true
            self.categoriesTF.isHidden = false
            self.searchIMG.isHidden = false
        
    }
    
    @IBAction func FinishBTNAction(_ sender: UIButton) {
            print("Name ",self.nameTF.text ?? "")
            print("category ",EPSselectedcategoryID)
            print("Mobile number",UserDefaults.standard.value(forKey: "Kphone") ?? "")
            print("About",typedabout)
            
        if self.nameTF.text == ""{
                showDefaultAlert(viewController: self, title: "Message", msg: "Please enter Name")
            }
            else if typedabout == ""{
                showDefaultAlert(viewController: self, title: "Message", msg: "Please enter About")
            }
            
            else{
               // welcomename = self.nameTF.text ?? ""
                finishProviderDetails()
                
            }
            
    }
    
    @IBAction func areaOfServiceBTN(_ sender: UIButton) {
        self.Popupview.isHidden = true
         defaults.set("true", forKey: "poupvalue")

        popupStatus = defaults.string(forKey: "poupvalue") ?? ""
       // setupTableView.isUserInteractionEnabled = true
        print("popupStatus",popupStatus)
    }
    
    
}


//MARK: -UITextFieldDelegates & UITextViewDelegate
extension ProviderSetupScrollVC: UITextFieldDelegate,UITextViewDelegate {
    
    //MARK: - textview delegate functions
        func textViewDidBeginEditing(_ textView: UITextView) {
                if detailsTxtView.text == "About Business" {
                    detailsTxtView.text = ""
                    detailsTxtView.textColor = UIColor.black
                    detailsTxtView.font = UIFont(name: "jost", size: 15.0)
                }
            }
            
            func textView(_ descriptionTxt: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
                if text == "\n" {
                    descriptionTxt.resignFirstResponder()
                }
                return true
            }
            
            func textViewDidEndEditing(_ textView: UITextView) {
                if detailsTxtView.text == "" {
                    detailsTxtView.text = "About Business"
                    detailsTxtView.textColor = UIColor.lightGray
                    detailsTxtView.font = UIFont(name: "jost", size: 15.0)
                }else{
                    typedabout = detailsTxtView.text
                }
                
            }

    
//MARK: - Textfield delegate functions
    //MARK: - Textfield delegate & functions
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("area of service")
        
//       if textField == stateTF{
//           NotificationCenter.default.post(name: Notification.Name("setup_pro_popupView"), object: nil)
//           print("state service")
//           statedropstatus = true
//          // callback?()
////           self.stateHeightView.constant =  350
//
//           stateTBL.reloadData()
//       }
        if textField == stateTF{
            if checkareastatus == false{
                let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "AreaOfServiceMsgVC") as! AreaOfServiceMsgVC
                checkclose = "closevc"
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                
            }
        }
        else if textField == cityTF{
            if checkareastatus == false{
                let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "AreaOfServiceMsgVC") as! AreaOfServiceMsgVC
                checkclose = "closevc"
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                
            }
        }
   }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == categoriesTF {
            guard let text = textField.text else { return true }
            let newText = (text as NSString).replacingCharacters(in: range, with: string)
            self.categoriesView.isHidden = newText == ""
            
            self.SsearchArrRes = newText == "" ? self.Snewcatarray : self.Snewcatarray.filter({ $0.name?.range(of: newText, options: .caseInsensitive) != nil })
            self.categoryTableView.reloadData()

        }
        else if textField == stateTF{
          //  self.Popupview.isHidden = false
            //self.stateDropview.isHidden = false
            Scityarray.removeAll()
            self.cityTF.text?.removeAll()
            self.radiusview.isHidden = true
            cityTBL.reloadData()

            guard let text = textField.text else { return true }
            let newText = (text as NSString).replacingCharacters(in: range, with: string)
            self.stateDropview.isHidden = newText == ""

            self.SSearchstateList = newText == "" ? self.SstateListArray :
            self.SstateListArray.filter({ $0.name?.range(of: newText, options: .caseInsensitive) != nil })
            self.stateTBL.reloadData()
            
               

        }
        else if textField == cityTF{
            
//            if self.cityTF.text == ""{
//                self.radiusview.isHidden = true
//                self.radiusTF.text?.removeAll()
//
//            }
            
            guard let text = textField.text else { return true }
                let newText = (text as NSString).replacingCharacters(in: range, with: string)
                self.cityDropview.isHidden = newText == ""
                
                self.SSearchcityList = newText == "" ? self.ScityListArray :
                self.ScityListArray.filter({ $0.name?.range(of: newText, options: .caseInsensitive) != nil })
                self.cityTBL.reloadData()
            
            self.radiusview.isHidden = false
            self.radiusTF.text = "25 miles"
            
            
           
        }
        return true
    }
}


//MARK: -UITableViewDelegates
extension ProviderSetupScrollVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.categoryTableView {
            return self.SsearchArrRes.count
        } else if tableView == self.stateTBL{
            return SSearchstateList.count
        } else if tableView == self.cityTBL{
            return SSearchcityList.count
        } else{
            return radiusArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.categoryTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: SCategorytvcell.identifier) as! SCategorytvcell
            cell.categoryLbl.text = self.SsearchArrRes[indexPath.row].name
            return cell
        } else if tableView == self.stateTBL{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sstatecell") as! Sstatecell
            cell.statenameLBL.text = SSearchstateList[indexPath.row].name
           // cell.statenameLBL.text = PMPEstatearray[indexPath.row] as? String
            return cell
        } else if tableView == self.cityTBL{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Scitycell") as! Scitycell
            cell.citynameLBL.text = SSearchcityList[indexPath.row].name
            return cell
        } else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sradiuscell") as! Sradiuscell
            cell.radiusLBL.text = radiusArray[indexPath.row] + " miles"
            return cell
        }
          
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == categoryTableView {
            EPSselectedcategoryID.removeAll()
            self.categoryLabel.isHidden = false
            self.closeCategoriesButton.isHidden = false
            self.categoriesTF.isHidden = true
            self.searchIMG.isHidden = true
            self.categoriesView.isHidden = true
            self.categoryLabel.text = self.SsearchArrRes[indexPath.row].name
            editCatID = self.SsearchArrRes[indexPath.row].id ?? ""
            EPSselectedcategoryID.append(editCatID)
        } else if tableView == stateTBL{
            let stName = self.SSearchstateList[indexPath.row].name ?? ""
            stateTF.text = stName
           
            let index = SstateListArray.firstIndex(where: { ($0.name) == stName }) ?? 0
            self.state = SstateListModel?.Data[index].isoCode ?? ""
            print(self.SstateListModel?.Data[index].isoCode ?? "")

            self.Scityarray.removeAll()
            self.cityTF.text?.removeAll()
            self.cityTBL.reloadData()
            self.radiusview.isHidden = true
            getcityAPI(stateiso: SstateListModel?.Data[index].isoCode ?? "")
            self.stateDropview.isHidden = true
            
            self.latitude = self.SSearchstateList[indexPath.row].latitude ?? ""
            self.longitude = self.SSearchstateList[indexPath.row].longitude ?? ""
            
            print(self.SSearchstateList[indexPath.row].latitude ?? "")
            print(self.SSearchstateList[indexPath.row].longitude ?? "")
            
        } else if tableView == cityTBL {
            let ctName = self.SSearchcityList[indexPath.row].name ?? ""
            cityTF.text = ctName
            self.city = self.SSearchcityList[indexPath.row].name ?? ""
            self.cityDropview.isHidden = true
            
            self.latitude = SSearchcityList[indexPath.row].latitude ?? ""
            self.longitude = SSearchcityList[indexPath.row].longitude ?? ""
            
//            self.radiusview.isHidden = false
//            self.radiusTF.text = "25 miles"
            
        }  else{
            self.radiusTF.text = radiusArray[indexPath.row] + " miles"
            radius = radiusArray[indexPath.row]
            self.radiusdropView.isHidden = true
            
        }
        }
        
    }


//MARK: - UIImagePickerControllerDelegates & functions
extension ProviderSetupScrollVC:UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }

    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
    
    //mime types
      func mimeType(for data: Data) -> String {
          
          var b: UInt8 = 0
          data.copyBytes(to: &b, count: 1)

          switch b {
          case 0xFF:
              return "i.jpeg"
          case 0x89:
              return ".png"
          case 0x47:
              return ".gif"
          case 0x4D, 0x49:
              return ".tiff"
          case 0x25:
              return "application/pdf"
          case 0xD0:
              return "application/vnd"
          case 0x46:
              return "text/plain"
          default:
              return "application/octet-stream"
          }
      }
   
    
    @IBAction func imgPickerBTN(_ sender: UIButton) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = true //If you want edit option set "true"
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
           }
       
    //MARK: image picker delegate method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[.editedImage] as! UIImage

        self.selectedProfileImage = tempImage
        pickedimage  = tempImage
        userImage.image = pickedimage
        print("image ==== ",pickedimage as Any)

        picker.dismiss(animated: true) {
            self.dismiss(animated: false, completion: nil)
        }
        
        if pickedimage != nil{
            uploadImage()
        }
    }
    
    
    //MARK: - PROFILE UPLOAD API CALL
         func uploadImage(){
             if selectedProfileImage != nil{
                 //showActivityIndicator()
                 let uiImage : UIImage = self.selectedProfileImage
                 imageData = uiImage.jpegData(compressionQuality: 0.2)!
                 
                 imageStr = imageData.base64EncodedString()
                 fileName = "image"
                 mimetype = mimeType(for: imageData)
                 print("mime type = \(mimetype)")
             }else{
                 imageData = Data()
                 fileName = ""
             }
           
             ServiceManager.sharedInstance.uploadSingleDataa("saveProfileImage", headerf: token, parameters: ["userId":userid], image: imageData, filename: fileName, mimetype: mimetype, withHud: false) { (success, response, error) in
                 print(response as Any)
                 if success {
                     if response!["status"] as! Bool == true {
                        // self.hideActivityIndicator()
                         print("image upload success")
                         
                     }else {
                         showDefaultAlert(viewController: self, title: "", msg: "image upload unsuccessfull")
                     }
                 }else{
                     print(error?.localizedDescription as Any)
                     showDefaultAlert(viewController: self, title: "", msg: error?.localizedDescription ?? "API Failed...!")
                 }
             }
         }
}


//MARK: - API CALL
extension ProviderSetupScrollVC{
    
    //MARK: - category api
    func getCategoryAPI() {
    
        let url = kBaseUrl+"getCategories"
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<500) .responseJSON { [self] response in
            print(response)
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    self.Scategorylist = getCategoriesResponse(from: responsedata)
                    self.Snewcatarray = self.Scategorylist?.Data ?? []
                    DispatchQueue.main.async {
                        self.SsearchArrRes = self.Snewcatarray
                        self.categoryTableView.reloadData()
                    }
                }
                
                
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        }
    }
    
    
    func getstateAPI(){
        // showActivityIndicator()
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
             
             let params: [String : Any] = [
                           "country_iso_code": "US"]
         
             let url = kBaseUrl+"statesList"
         AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
             print(response)
              switch (response.result)      {
                  
                case .success( let JSON):
                  
                  if let responsedata =  JSON as? [String:Any]  {
                      print("responsedata",responsedata)
                      self.SstateListModel = SearchStateList(from:responsedata)
                      self.SstateListArray = self.SstateListModel?.Data ?? []
                      if let dataa = responsedata["data"] as? [[String:Any]]{
     //                        print("dataa = ", dataa)
                         
                          for i in dataa{
                              print("looped data =",i)
                              self.Sstatearray.append(i)
                          }
                      }
                      self.stateTBL.reloadData()
                  }
                      
                     
                           
                
              case .failure(let error):
                  print("Request error: \(error.localizedDescription)")
              }
          }
       }
    
    func getcityAPI(stateiso:String){
        //   showActivityIndicator()
               let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
               let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
               
               let params: [String : Any] = [
                             "country_iso_code": "US",
                             "state_iso_code": stateiso]
           
               let url = kBaseUrl+"citiesList"
           AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
                switch (response.result)      {
                  case .success( let JSON):
                    
                    if let responsedata =  JSON as? [String:Any]  {
                        print("responsedata",responsedata)
                        self.ScityList = SearchCityList(from:responsedata)
                        self.ScityListArray = self.ScityList?.Data ?? []
                        if let dataa = responsedata["data"] as? [[String:Any]]{

                            for i in dataa{
                                print("looped data =",i)
                                self.Scityarray.append(i)
                            }
                        }
                    }
                    
                        self.cityTBL.reloadData()
                    
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
            }
       }

    
    
    func finishProviderDetails(){
//
        if isNetworkReachable {
            showActivityIndicator()

            let id = UIDevice.current.identifierForVendor?.uuidString ?? ""
                    print("idddd",id)
            let devicename = UIDevice.current.model
            let osversion = UIDevice.current.systemVersion
            let phonetype = UIDevice.current.systemName

            let params = [
//                "userid":"64a3bbbb26fc79c3fa0a4b00",
                "userid":userid,
                "name": self.nameTF.text ?? "",
                "countrycode": getCode,
                "phonenumber": getmobNum,
                "categoryimage":profileimage,
                "country" : "us",
                "radiusOfService":radius,
                "state": state,
                "city": city,
                "latitude": latitude,
                "longitude": longitude,
                "companyname": self.companyNameTF.text ?? "",
                "operationalhours": self.operationsTF.text ?? "",
                "aboutbusiness": typedabout,
                "categoryids": EPSselectedcategoryID,
                "phoneMake": devicename,
                "osVersion": osversion,
                "phoneType": phonetype,
                "deviceToken": id] as? [String : Any]

            print(params)
            
            let url = kBaseUrl+"saveProvider"
            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<500) .responseJSON { response in
                print("Response from server",response)
                switch (response.result) {
                case .success( let JSON):
                    print("Json - ",JSON)
                    self.hideActivityIndicator()
                    loggedin = true
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let vc = storyboard.instantiateViewController(identifier: "CustomTabbarVC") as! CustomTabbarVC
                    self.navigationController?.pushViewController(vc, animated: true)


                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                    showDefaultAlert(viewController: self, msg: "something has happend, try again")
                }
            }
        }
    }
    
   // 8089138037
    //8593911384
    //9400456865
    
    //Get details
    func getDetailsapi(){
       // showActivityIndicator()
        if isNetworkReachable {

            let params = ["phoneNumber":getmobNum,"countryCode":"+1"] as [String : Any]
            print("Params = \(params)")
            let url = kBaseUrl+"checkPhoneNumber"
            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: nil).validate(statusCode: 200..<500) .responseJSON { response in
                print(response)
                switch (response.result) {
                case .success( let JSON):

                    if let responsedata =  JSON as? [String:Any]  {
                        print("responsedata",responsedata)
                        self.getcheckpnumdetails = getchecknumberResponse(from:responsedata)
                       
                        self.closeCategoriesButton.isHidden = false
                        self.categoriesTF.isHidden = true
                        self.searchIMG.isHidden = true
                        self.categoriesView.isHidden = true
                        self.categoryLabel.isHidden = false
                        self.categoryLabel.text = self.getcheckpnumdetails?.Data?.categoryIds.first?.name
                        self.editCatID = self.getcheckpnumdetails?.Data?.categoryIds.first?.id ?? ""
                        self.EPSselectedcategoryID.append(self.editCatID)
                        self.userImage.kf.setImage(with: URL(string: kImageUrl + (self.getcheckpnumdetails?.Data?.categoryIds.first?.categoryImage ?? "")),placeholder: UIImage(named: "profileicon"))
                        self.profileimage = self.getcheckpnumdetails?.Data?.categoryIds.first?.categoryImage ?? ""
                    }

                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                    showDefaultAlert(viewController: self, msg: "something has happend, try again")
                }


            }

        }
    }
    
}



//MARK: - Cell classes
class SCategorytvcell : UITableViewCell{
    static let identifier = String(describing: SCategorytvcell.self)

    @IBOutlet weak var categoryLbl: UILabel!
}

class Sstatecell : UITableViewCell{
    @IBOutlet weak var statenameLBL:UILabel!
}

class Scitycell: UITableViewCell{
    @IBOutlet weak var citynameLBL: UILabel!
    
}

class Sradiuscell: UITableViewCell{
    @IBOutlet weak var radiusLBL: UILabel!
    
}
