//
//  ContactsmainVC.swift
//  EzLukUp
//
//  Created by Srishti on 15/11/22.
//

import UIKit
import Contacts
import Alamofire
import Algorithms
import CCBottomRefreshControl
import Firebase
import FirebaseDynamicLinks
import IPImage
import CoreData
import MessageUI
import ContactsUI
import SVProgressHUD
import Kingfisher

protocol updateProviderContactsTable:NSObjectProtocol{
    func reloadData(sender:ContactsmainVC)
}


var headcount = 0
var tagheadcount = 0
var selectedBTN  = 0
var tagcvcount = 0
var tagimgselect = ""

//var recomendedcount = 0

@available(iOS 14.0, *)
class ContactsmainVC: UIViewController {
    
    //MARK: - VARIABLES AND CONSTANTS
    var offsetpg = 0
    var providerpaginationremainingcount = 0
    // if 0 selected is generalbtn , 1- providerbtn selected
    var selectedType = "Personal"
    var selectedsection = -1
    var isdropdownBTNselected = false
    var issectionzeroOpened = true
    var issectiononeOpened = true
    let store = CNContactStore()
    var contactlist : SingleContactListResponse?{
        didSet{
            self.taggedContacts = self.contactlist?.getFilteredTaggedContactlist(self.filtertype, searchtexter)
            self.possibleProviders = self.contactlist?.getFilteredPossibleProviderlist(self.filtertype, searchtexter)
            self.chunkedlist = self.contactlist?.getfilterednormallist(self.filtertype, searchtexter) ?? []
            contactsTBL.reloadData()
        }
    }//core added
    var providerlist : ProviderSingleContactListResponse?{
        didSet{
            self.providerlistpg = self.providerlist?.getFilteredProviderlist(self.providerfiltertype , self.searchtexter).compactMap({ProviderCategoryListModelC(data: $0)})
            contactsTBL.reloadData()
            
        }
    }
    var searchtexter = ""{
            didSet{
                if selectedBTN == 0{
                self.taggedContacts = self.contactlist?.getFilteredTaggedContactlist(self.filtertype, searchtexter)
                self.possibleProviders = self.contactlist?.getFilteredPossibleProviderlist(self.filtertype, searchtexter)
                self.chunkedlist = self.contactlist?.getfilterednormallist(self.filtertype, searchtexter) ?? []
                
                }else{
                    self.providerlistpg = self.providerlist?.getFilteredProviderlist(self.providerfiltertype , self.searchtexter).compactMap({ProviderCategoryListModelC(data: $0)})
                }
                contactsTBL.reloadData()
        }
    }
    
    var providerfiltertype : Providerfiltertype = .all{
        didSet{
            self.providerlistpg = self.providerlist?.getFilteredProviderlist(self.providerfiltertype , self.searchtexter).compactMap({ProviderCategoryListModelC(data: $0)})
            contactsTBL.reloadData()
            
        }
    }
    
    var UPcontactlist : SingleContactListResponse?//core added
    var contactlistpg : SingleContactListResponse?//core added
    var chunkedlist : [getcontactDataListModelcodable] = []
    var taggedContacts : [SingleContactListData]?
    var possibleProviders : [SingleContactListData]?
    //   var contactlist : getcontactDataModel?
    //    var contactlistpg : getcontactDataModel?
    var remainingcount =  0
    var Allcontactlist : getcontactDataModel?
    
    var providerlistpg : [ProviderCategoryListModelC]?
//    var providerlist : providerResponseModel?
//    var providerlistpg : providerResponseModel?
    let refreshControl = UIRefreshControl()
    var getCountry = String()
    var getState = String()
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as? String ?? ""
    let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
    
    let imageArray: [UIImage?] = [UIImage(named: "profile_icon"),UIImage(named: "profile_icon"),UIImage(named: "profile_icon"),UIImage(named: "profile_icon"),UIImage(named: "profile_icon"),UIImage(named: "profile_icon")]
    let generaltipstatus:Bool = UserDefaults.standard.value(forKey: "generalTipStatus") as? Bool ?? false
    let providertipstatus:Bool = UserDefaults.standard.value(forKey: "providerTipStatus") as? Bool ?? false
    
    //sync contacts variables
    //    var blurView : UIView!
    //    let kScreenWidth = UIScreen.main.bounds.width
    //    let kScreenHeight = UIScreen.main.bounds.height
    //  let store = CNContactStore()
    var syncContact : Bool = false
    var contacDictionary = [[String:Any]]()
    var tempArr : [[String:Any]] = []
    let uploadCount = 1500
    var contactParam = [[String:Any]]()
    var uploadIsFaild = 3
    //  var activityView: UIActivityIndicatorView?
    var contactsCount = 0
    var timer = Timer()
    var usertype = UserDefaults.standard.value(forKey: "Ktype") ?? ""
    
    
    
    //MARK: - OUTLETS
    @IBOutlet weak var filterIMG: UIImageView!
    @IBOutlet weak var ButtonStack: UIStackView!
    @IBOutlet weak var generalBTNview:UIView!
    @IBOutlet weak var providerBTNview:UIView!
    @IBOutlet weak var generalLBL: UILabel!
    @IBOutlet weak var providerLBL: UILabel!
    @IBOutlet weak var GeneralTipviewHeight: NSLayoutConstraint!
    @IBOutlet weak var ProviderTipviewHeight: NSLayoutConstraint!
    @IBOutlet weak var filterview: UIView!
    @IBOutlet weak var FilterviewBlur: UIView!
    
    @IBOutlet weak var contactsTBL: UITableView!
    @IBOutlet weak var GeneralTipview: BaseView!
    @IBOutlet weak var providerTipview: BaseView!
    @IBOutlet weak var nodataLBL: UILabel!
    
    @IBOutlet weak var FilteroneLBL: UILabel!
    @IBOutlet weak var FiltertwoLBL: UILabel!
    @IBOutlet weak var FilterthreeLBL: UILabel!
    @IBOutlet weak var FilterfourLBL: UILabel!
    @IBOutlet weak var inreviewView: UIView!
    @IBOutlet weak var FilterfiveLBL: UILabel!
    @IBOutlet weak var filterseperatorline: UIView!
    @IBOutlet weak var inreviewBTNout: UIButton!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    @IBOutlet weak var NeedsBlurView: UIView!
    @IBOutlet weak var NeedsPopupView: BaseView!
    @IBOutlet weak var syncContAlertView: BaseView!
   
    
    var contactid = ""
    var fkey = ""
    var totalcountfrom3sections = 0
    var isloadinglist = false
    var providerlistpaginationcount = 0
    var providerlistpagecount = 0
    var inviteid = ""
    var filtertype : filtertype = .all{
        didSet{
            self.taggedContacts = self.contactlist?.getFilteredTaggedContactlist(self.filtertype, searchtexter)
            self.possibleProviders = self.contactlist?.getFilteredPossibleProviderlist(self.filtertype, searchtexter)
            self.chunkedlist = self.contactlist?.getfilterednormallist(self.filtertype, searchtexter) ?? []
            contactsTBL.reloadData()
            
        }
    }
    var RecomendImgArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchAllData()
        fetchAllProviderData()
        NeedsBlurView.isHidden = true
        NeedsPopupView.isHidden = true
        setupUI()
        if #available(iOS 15, *) {
            contactsTBL.sectionHeaderTopPadding = 0
        }
        refreshControl.triggerVerticalOffset = 100.0
        
        refreshControl.tintColor = UIColor.blue
        tabselector()
        print("selectedButton == \(selectedBTN)")
        //                checktipStatus()
        NotificationCenter.default.addObserver(self, selector: #selector(self.goToHomeTab), name: Notification.Name("hometab"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onGeneralApiCalled(notification:)), name: Notification.Name("callgeneralapi"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onProviderApiCalled(notification:)), name: Notification.Name("callproviderapi"), object: nil)
        checkconsumerdashinez()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("selectedButton in viewllappear== \(selectedBTN)")
        tabselector()
    }
    func checktipStatus(){
        if generaltipstatus{
            GeneralTipview.isHidden = true
        }else{
            GeneralTipview.isHidden = false
        }
        if providertipstatus{
            providerTipview.isHidden = true
        }else{
            providerTipview.isHidden = false
        }
    }
    
    @objc func onGeneralApiCalled(notification: Notification) {
        callWebServiceToGetSigleContactList()
        
    }
    
    @objc func onProviderApiCalled(notification: Notification) {
        callWebServiceToGetSigleProviderContactList()
    }
    
    func tabselector(){
        if selectedBTN == 0{
            offsetpg = 0
            //            self.contactlist = nil
            //            api(filterkey: "all")
            providerBTNview.backgroundColor = UIColor.clear
            generalBTNview.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
            generalLBL.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
            providerLBL.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        }else{
            //            providerlist = nil
            providerlistpaginationcount = 0
            //            providerlistAPI(searchkey: "", filterkey: "all")
            generalBTNview.backgroundColor = UIColor.clear
            providerBTNview.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
            generalLBL.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
            providerLBL.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        }
    }
    
    @objc func goToHomeTab(){
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func reloadList(){
        isloadinglist = true
        if self.contactlistpg?.gettotalcount() ?? 0 > totalcountfrom3sections{
            if isloadinglist == true {
                offsetpg += 10
                self.paginationApicaller()
                
            }
        }
    }
    
    func paginationApicaller(){
        self.isloadinglist = false
        self.api(filterkey: "")
        self.refreshControl.endRefreshing()
    }
    
    
    //MARK: - FUNCTIONS
    func setupUI(){
        selectedBTN = 0
        contactsTBL.decelerationRate = UIScrollView.DecelerationRate.fast
        ButtonStack.layer.cornerRadius = ButtonStack.frame.height / 2
        providerBTNview.backgroundColor = UIColor.clear
        generalLBL.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        providerLBL.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        self.inreviewView.isHidden = true
        self.filterseperatorline.isHidden = true
        self.syncContAlertView.isHidden = true
        //show only general tip view in selectedbtn = 0
        
        //tip view open if api value is true
        
        //            if UserDefaults.standard.value(forKey: "generalTipStatus") as! String == "true"{
        //                providerTipview.isHidden = true
        //                GeneralTipview.isHidden = true
        //            }else{
        //                providerTipview.isHidden = false
        //                GeneralTipview.isHidden = true
        //            }
        //
        //            if UserDefaults.standard.value(forKey: "providerTipStatus") as! String == "true"{
        //
        //                providerTipview.isHidden = true
        //                GeneralTipview.isHidden = true
        //            }else{
        //                providerTipview.isHidden = true
        //                GeneralTipview.isHidden = true
        //
        //            }
        
        
        filterview.isHidden = true
        FilterviewBlur.isHidden = true
        filterview.layer.cornerRadius = 13
        filterview.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
    }
    
    //MARK: - COREDATA THINGS STARTING------------
    
    var contactData : SingleContactListResponse?
    var ProviderData : ProviderSingleContactListResponse?
    var homeresponsenewdata =  [NSManagedObject]()
    var Phomeresponsenewdata =  [NSManagedObject]()
    var limiter = 100
    
    //MARK: - COREDATA READ RECORDS
    func readRecords(fromCoreData tableName: String, predicate: NSPredicate? = nil, sortDescriptor: [Any]? = nil, limit: Int = 0, context: NSManagedObjectContext) -> [Any] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entity = NSEntityDescription.entity(forEntityName: tableName, in: context)
        fetchRequest.entity = entity
        fetchRequest.returnsObjectsAsFaults = false
        if predicate != nil {
            fetchRequest.predicate = predicate
        }
        if let sortDescriptors = sortDescriptor  as? [NSSortDescriptor] {
            fetchRequest.sortDescriptors = sortDescriptors
        }
        if limit != 0 {
            fetchRequest.fetchLimit = limit
        }
        let records: [Any]? = try? context.fetch(fetchRequest)
        return records!
    }
    
    func fetchAllData(){
        if (CoreDataManager.sharedManager.fetchAll() != nil) && (CoreDataManager.sharedManager.fetchAll() != []){
            Dataconversion()
        }else{
            print("call general api")
            callWebServiceToGetSigleContactList()
        }
    }
    
    func fetchAllProviderData(){
        if (CoreDataManager.sharedManager.fetchAll() != nil) && (CoreDataManager.sharedManager.fetchAll() != []){
            providerDataConversion()
        }else{
            print("call provider api")
            callWebServiceToGetSigleProviderContactList()
        }
    }
    
    //MARK: - COREDATA DATA TO MODEL CONVERSION
    func providerDataConversion(){
        Phomeresponsenewdata = CoreDataManager.sharedManager.fetchAllprovider()!
        self.ProviderData = ProviderSingleContactListResponse()
        for data in Phomeresponsenewdata{
            
            if let response = data.value(forKey:"providerResponse") as? [String:Any]
            {
                let jsonData = try! JSONSerialization.data(withJSONObject: response, options: [])
                let decoder = JSONDecoder()
                if let model = try? decoder.decode(ProviderSingleContactListResponse.self, from: jsonData) {
                    ProviderData = model
                }
            }
        }
        providerlist = ProviderData
//        if selectedBTN == 1 {
//        self.nodataLBL.isHidden = self.providerlistpg?.count != 0
            contactsTBL.reloadData()
//        }
        print("provider count ==",ProviderData?.data.first?.name)
    }
    func Dataconversion(){
        /* converting coredata dictionary to model*/
        homeresponsenewdata = CoreDataManager.sharedManager.fetchAll()!
        //        print("++||++",CoreDataManager.sharedManager.fetchAll()!)
        self.contactData = SingleContactListResponse()
        
        for data in homeresponsenewdata{
            
            if let response = data.value(forKey:"generalResponse") as? [String:Any]
            {
                let jsonData = try! JSONSerialization.data(withJSONObject: response, options: [])
                let decoder = JSONDecoder()
                if let model = try? decoder.decode(SingleContactListResponse.self, from: jsonData) {
                    contactData = model
                }
            }
            
        }
        contactlist = contactData
        self.contactsTBL.reloadData()
        
        let povidercontactsDataFromCoredata = self.readRecords(fromCoreData: "GeneralContactsData", context: CoreDataManager.sharedManager.persistentContainer.viewContext)
        print(povidercontactsDataFromCoredata)
        //        print("Modl ---",contactData?)
        print("tagged count ==",contactData?.taggedContactCount ?? 0)
        print("tagged array count ==",contactData?.taggedContact?.count ?? 0)
        print("possibleProviders count ==",contactData?.possibleProvidersCount ?? 0)
        print("possibleProviders array count ==",contactData?.possibleProviders?.count ?? 0)
        print("normalList count ==",contactData?.normalListCount ?? 0)
        print("normalList array count ==",contactData?.normalList?.count ?? 0)
        
    }
    
    //MARK: - COREDATA GENERAL API CALL
    func callWebServiceToGetSigleContactList() {
        ContactService().getSingleContactList(limit: "\(limiter)", userid: userid) { response, error in
            if let data = response {
                print("RESPONSE FROM SERVER",response)
                CoreDataManager.sharedManager.Deleteall {
                    CoreDataManager.sharedManager.insertHomeresponse(Response:data.dictionary)
                }
            }
            self.Dataconversion()
            if let error = error {
                print("error",error)
            }
        }
    }
    
    //MARK: - COREDATA PROVIDER API CALL
    func callWebServiceToGetSigleProviderContactList() {
        ContactService().getProviderContactList(userid: userid){ response, error in
            if let data = response {
                
                CoreDataManager.sharedManager.Deleteallprovider {
                    CoreDataManager.sharedManager.insertproviderresponse(Response:data.dictionary)
                }
            }
            self.providerDataConversion()
            if let error = error {
                print("error",error)
            }
        }
    }
    //MARK: ---------------------------------------------------------------------------------
    
    
    
    func dotBTNcalled(){
        //MARK: - UImenu for 3dot button
        
    }
    
    func checkconsumerdashinez(){
        if checkdashcontacts == "contacts"{
            filtertype = .inez
            self.searchTF.text = ""
            self.contactsTBL.reloadData()
        }
    }
    
    func checkfeedreportdata(){
        print("chakka",checkfeedback)
        if checkprovidercont == "providercontacts"{
            providerlistpaginationcount = 0
            selectedType = "Business"
            filtertype = .all
            self.searchTF.text = ""
            generalBTNview.backgroundColor = UIColor.clear
            providerBTNview.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
            generalLBL.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
            providerLBL.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
            self.inreviewView.isHidden = false
            self.filterseperatorline.isHidden = false
            // Needs review popup showing
            if self.providerlist?.reviewCount ?? 0 > 0{
                self.NeedsBlurView.isHidden = false
                self.NeedsBlurView.isHidden = false
            }
            else{
                self.NeedsBlurView.isHidden = true
                self.NeedsBlurView.isHidden = true
            }
        }
    }
    
    //check contact sync or not
    
    //MARK: - BUTTON ACTIONS
    //needs contact syncing button action
    @IBAction func syncContactBTNTapped(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.requestAccess { accessGranted in
                print("contact request --",accessGranted)
                // if accessGranted{
                DispatchQueue.global(qos: .background).async {
                    self.getContactList()
                    DispatchQueue.main.async {
                        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.autopush), userInfo: nil, repeats: false)
                    }
                }
                
                
                //}
                
            }
            
        }
    }
    //needs review popup button action
    @IBAction func NeedsReviewPopup_OK_Btn(_ sender: UIButton) {
        self.NeedsBlurView.isHidden = true
        self.NeedsPopupView.isHidden = true
    }
    //filterbutton actions
    @IBAction func FilterBTNone(_ sender: UIButton) {
       // fkey = "all"
        
        self.searchTF.text = ""
        if selectedBTN == 0 {
            fetchAllData()
            self.filtertype = .all
            self.contactsTBL.reloadData()
        }
        else{
           fetchAllProviderData()
            self.providerfiltertype = .all
            self.contactsTBL.reloadData()
        }
        filterview.isHidden = true
        FilterviewBlur.isHidden = true
        self.filterIMG.image = UIImage(named: "filtericon")
    }
    @IBAction func FilterBTNtwo(_ sender: UIButton) {
        contactlist?.taggedContact = contactlist?.taggedContact?.filter({$0.isUser == false && $0.isReferred == false})
        contactlist?.possibleProviders = contactlist?.possibleProviders?.filter({$0.isUser == false && $0.isReferred == false})
        
        self.searchTF.text = ""
        
        if selectedBTN == 0 {
           // fkey = "invite"
            self.filtertype = .invite
            contactsTBL.reloadData()
            
        }
        else{
            fkey = "recomendbyme"
            self.providerfiltertype = .inezRecomendedbyMe
            self.contactsTBL.reloadData()
        }
        filterview.isHidden = true
        FilterviewBlur.isHidden = true
        self.filterIMG.image = UIImage(named: "filtericon")
    }
    @IBAction func FilterBTNthree(_ sender: UIButton) {
        self.searchTF.text = ""
        if selectedBTN == 0 {
           // fkey = "invited"
            contactlist?.taggedContact = contactlist?.taggedContact?.filter({ $0.isReferred == true})
            contactlist?.possibleProviders = contactlist?.possibleProviders?.filter({ $0.isReferred == true})
            self.filtertype = .invited
            contactsTBL.reloadData()
        }
        else{
            fkey = "recomendbyothers"
           self.providerfiltertype = .inezRecomendedbyOthers
            self.contactsTBL.reloadData()
        }
        filterview.isHidden = true
        FilterviewBlur.isHidden = true
        self.filterIMG.image = UIImage(named: "filtericon")
    }
    @IBAction func FilterBTNfour(_ sender: UIButton) {
        self.searchTF.text = ""
        if selectedBTN == 0 {
           // fkey = "inez"
            contactlist?.taggedContact = contactlist?.taggedContact?.filter({ $0.isUser == true})
            contactlist?.possibleProviders = contactlist?.possibleProviders?.filter({ $0.isUser == true})
            self.filtertype = .inez
            contactsTBL.reloadData()
            
        }
        else{
            fkey = "connected"
            self.providerfiltertype = .inezjustConnected
            self.contactsTBL.reloadData()
            
        }
        filterview.isHidden = true
        FilterviewBlur.isHidden = true
        self.filterIMG.image = UIImage(named: "filtericon")
    }
    
    @IBAction func FilterBTNfive(_ sender: UIButton) {
        if selectedBTN == 1{
           // fkey = "needsreview"
            self.providerfiltertype = .inezNeedsreview
            self.contactsTBL.reloadData()
        }
        filterview.isHidden = true
        FilterviewBlur.isHidden = true
        self.filterIMG.image = UIImage(named: "filtericon")
    }
    
    @IBAction func AddBTNtapped(_ sender: UIButton) {
        if selectedBTN == 0 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "AddGeneralContactVC") as! AddGeneralContactVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "AddProviderContactVC") as! AddProviderContactVC
            //            let vc = storyboard.instantiateViewController(identifier: "AddProviderVC_Scrollview") as! AddProviderVC_Scrollview
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func FilterBlurBTN(_ sender: UIButton) {
        
        if filterview.isHidden{
            filterview.isHidden = false
            FilterviewBlur.isHidden = false
        }
        else{
            filterview.isHidden = true
            FilterviewBlur.isHidden = true
        }
    }
    
    
    @IBAction func FilterBTNtapped(_ sender: UIButton) {
        if filterview.isHidden{
            filterview.isHidden = false
            FilterviewBlur.isHidden = false
            self.filterIMG.image = UIImage(named: "filterselected")
        }
        else{
            filterview.isHidden = true
            FilterviewBlur.isHidden = true
            self.filterIMG.image = UIImage(named: "filtericon")
        }
        if selectedBTN == 0 {
            FilteroneLBL.text = "All"
            FiltertwoLBL.text = "Invite"
            FilterthreeLBL.text = "invited"
            FilterfourLBL.text = "In ez"
            selectedType = "Personal"
        }
        else{
            FilteroneLBL.text = "All"
            FiltertwoLBL.text = "In ez - Recommended by Me"
            FilterthreeLBL.text = "In ez - Recommended by Others"
            FilterfourLBL.text = "In ez - Connected (Not Recommending)"
            FilterfiveLBL.text = "Provider - Needs Review"
            selectedType = "Business"
        }
        
    }
    
    @IBAction func GeneralBTNtapped(_ sender: UIButton) {
        selectedBTN = 0
        searchTF.text = ""
        filtertype = .all
        self.contactsTBL.reloadData()
        //        contactsTBL.isHidden = false
        //tip condtion checking
        if UserDefaults.standard.value(forKey: "generalTipStatus") as! String == "true"{
            //if tip shown once
            GeneralTipview.isHidden = true
            providerTipview.isHidden = true
        }
        else{
            GeneralTipview.isHidden = false
            providerTipview.isHidden = true
        }
        
        selectedType = "Personal"
        
        providerBTNview.backgroundColor = UIColor.clear
        generalBTNview.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1) //= UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1).cgColor
        generalLBL.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        providerLBL.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        self.inreviewView.isHidden = true
        self.filterseperatorline.isHidden = true
    }
    
    @IBAction func ProviderBTNtapped(_ sender: UIButton) {
        selectedBTN = 1
        searchTF.text = ""
        providerDataConversion()
        self.contactsTBL.reloadData()
        //        contactsTBL.isHidden = true
        //        if UserDefaults.standard.value(forKey: "providerTipStatus") as! String == "true"{
        //                    providerTipview.isHidden = true
        //                    GeneralTipview.isHidden = true
        //                }
        //                else{
        //                    providerTipview.isHidden = false
        //                    GeneralTipview.isHidden = true
        //                }
       
        providerlistpaginationcount = 0
        selectedType = "Business"
        
        generalBTNview.backgroundColor = UIColor.clear
        providerBTNview.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        generalLBL.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
        providerLBL.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        self.inreviewView.isHidden = false
        self.filterseperatorline.isHidden = false
        // Needs review popup showing
        if self.providerlist?.reviewCount ?? 0 > 0{
            self.NeedsBlurView.isHidden = false
            self.NeedsPopupView.isHidden = false
        }
        else{
            self.NeedsBlurView.isHidden = true
            self.NeedsPopupView.isHidden = true
        }
        
        //        let povidercontactsDataFromCoredata = readRecords(fromCoreData: "ProviderContactsEntity", context: self.container!.viewContext)
        //        print("coredata fetched == ",povidercontactsDataFromCoredata)
    }
    @IBAction func searchBTNTapped(_sender : UIButton){
        if selectedBTN == 0 {
            
        }
        else{
//            providerlist?.Data.removeAll()
//            providerlistpaginationcount = 0
//            providerlistAPI(searchkey: searchTF.text ?? "", filterkey: fkey)
            
        }
        
    }
    //MARK: - Tip Close Button functions
    @IBAction func TipcloseBTNtapped(_ sender: UIButton) {
        print("tag = \(sender.tag)")
        if selectedBTN == 0{
            GeneralTipview.isHidden = true
            tipcloseAPI(keyvalue: "tooltip.contactswipe")
        }else{
            providerTipview.isHidden = true
            tipcloseAPI(keyvalue: "tooltip.contactswipeprovider")
        }
    }
    
    @IBAction func HeaderDropBTNtapped(_ sender: UIButton) {
        print("section :",sender.tag)
        selectedsection = sender.tag
        isdropdownBTNselected = !isdropdownBTNselected
        
        if selectedBTN == 0 {
            if sender.tag == 0 {
                issectionzeroOpened = !issectionzeroOpened
                contactsTBL.reloadData()
                
            }
            else if sender.tag == 1 {
                issectiononeOpened = !issectiononeOpened
                contactsTBL.reloadData()
                
            }else {
                self.chunkedlist[sender.tag - 2].switchCellexpansionStatus()
            }
            
        }else{
//            self.providerlistpg?[sender.tag].sectionOpened = !(self.providerlistpg?[sender.tag].sectionOpened ?? true)
            self.providerlistpg?[sender.tag].switchCellexpansionStatus()
        }
        contactsTBL.reloadData()
        
    }
    
    @IBAction func dotBTNtapped(_ sender: UIButton) {
        
        print("indexpath :",sender.tag)
        
    }
    
    
    @IBAction func providerDotsBTNTapped(_ sender: UIButton) {
        print("indexpath :",sender.tag)
    }
    
}

//MARK: - EXtension (Tableview)
@available(iOS 14.0, *)
extension ContactsmainVC:UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate{
    
    //    func readRecords(fromCoreData tableName: String, predicate: NSPredicate? = nil, sortDescriptor: [Any]? = nil, limit: Int = 0, context: NSManagedObjectContext) -> [Any] {
    //            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
    //            let entity = NSEntityDescription.entity(forEntityName: tableName, in: context)
    //            fetchRequest.entity = entity
    //            fetchRequest.returnsObjectsAsFaults = false
    //            if predicate != nil {
    //                fetchRequest.predicate = predicate
    //            }
    //            if let sortDescriptors = sortDescriptor  as? [NSSortDescriptor] {
    //                fetchRequest.sortDescriptors = sortDescriptors
    //            }
    //            if limit != 0 {
    //                fetchRequest.fetchLimit = limit
    //            }
    //            let records: [Any]? = try? context.fetch(fetchRequest)
    //            return records!
    //        }
    
    //MARK: - Fetched Result Controller - Retrieve data from CoreData
    //    func retreiveProviderContactsDatafromCoreData(){
    //        if let context = self.container?.viewContext{
    ////            let request : NSFetchRequest<ProviderContactsEntity> = ProviderContactsEntity.fetchRequest()
    //
    //
    //            let fetchRequest: NSFetchRequest<ProviderContactsEntity> = ProviderContactsEntity.fetchRequest()
    //            let sortByTitle = NSSortDescriptor(key: "name", ascending: true)
    //            fetchRequest.sortDescriptors = [sortByTitle]
    //            let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
    //
    //
    //
    //
    //            self.fetchedresultsController = NSFetchedResultsController(
    //                fetchRequest: fetchRequest,
    //                managedObjectContext: context,
    //                sectionNameKeyPath: nil,
    //                cacheName: nil
    //            )
    //
    //            // Notiies the tableview when any changes have occured to the data
    //            fetchedresultsController?.delegate = self
    //
    //
    //            do {
    //                let result = try self.container?.viewContext.fetch(fetchRequest)
    //                for data in result! {
    //                    print("user name = ",data.value(forKey: "name") as? String ?? "Nothing in DB")
    //                    print("phon number =",data.value(forKey: "phoneNumber") as? String ?? "Nothing in DB")
    //                }
    //            } catch {
    //                print("failed to fetch ")
    //            }
    //
    //
    //
    //            // Fetch data
    //            do{
    //                try self.fetchedresultsController?.performFetch()
    //                //fetchData()
    //            }catch{
    //                print("Failed to Inititalize FetchedResultController: \(error)")
    //            }
    //        }
    //    }
    
    
    
    // Changes have happend in FetchedResultsController so we need to notify the tableview
    //    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    //        //Update the tableView
    //        self.delegate?.reloadData(sender: self)
    //    }
    
    //TableView Datasource function
    //    func numberOfRowsInSection(section : Int) -> Int{
    //        return fetchedresultsController?.sections?[section].numberOfObjects ?? 0
    //    }
    
    //    func object(indexpath:IndexPath) -> ProviderContactsEntity?{
    //        return fetchedresultsController?.object(at: indexpath)
    //    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedBTN == 0{
            return (self.chunkedlist.count ?? 0 ) + 2
        }else{
            return (self.providerlistpg?.count ?? 0)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedBTN == 0{
            if section == 0 {
                
                return self.taggedContacts?.count ?? 0
            }
            else if section == 1 {
                return self.possibleProviders?.count ?? 0
            }else {
                return self.chunkedlist[section - 2].contactsList?.count ?? 0
            }
        }
        
        return self.providerlistpg?[section].data?.contacts.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = contactsTBL.dequeueReusableCell(withIdentifier: "headercell") as! headercell
        cell.dropBTN.tag = section
        
        if selectedBTN == 0{
            cell.nameLBL.text = section == 0 ? "Tagged by other users as Provider": (section == 1 ? "ezlukup suggests possible Provider" : self.chunkedlist[section - 2].Headername?.capitalized)
        }
        else{
            //            var providerdata : ProviderContactsEntity?
            //            print("names from core",fetchedresultsController?.sections?[section])
//            if self.providerlist?.data[section].contacts.count ?? 0 > 0 {
                cell.nameLBL.text = self.providerlistpg?[section].data?.name
//            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = contactsTBL.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! contactCell
        
        if selectedBTN == 0{
            if let contactList = self.contactlist{
                cell.generalViewHeight.constant = 110
                //MARK: - Cell for row - Tagged Section
                if indexPath.section == 0 {
                    cell.generalViewHeight.constant = 110
                    DispatchQueue.main.async {
                       // if cell.cardheight == true{
//                            cell.generalViewHeight.constant = 85
//                        }else{
                           
                        cell.taggedContacts = self.taggedContacts
                        cell.recomStackview.isHidden = false
                       // }
                    }
                    //tagged user name
                    cell.nameLBL.text = taggedContacts?[indexPath.row].name ??  taggedContacts?[indexPath.row].twilioCallerName
                    
                    //tagged user image , category name
                    DispatchQueue.main.async {
                        if self.taggedContacts?[indexPath.row].contactUserId != nil {
                            cell.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.taggedContacts?[indexPath.row].contactUserId?.profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                            
                            cell.categorynameLBL.textColor = #colorLiteral(red: 0, green: 0.2823529412, blue: 0.1882352941, alpha: 1)
                            cell.statusView.isHidden = true
                        if self.taggedContacts?[indexPath.row].contactUserId?.categoryIds?.count ?? 0 > 0 {
                            
                            cell.categorynameLBL.text = self.taggedContacts?[indexPath.row].contactUserId?.categoryIds?.first?.name
                        }
                        else{
                            cell.categorynameLBL.text = self.taggedContacts?[indexPath.row].contactUserId?.taggedCategories?.first?.taggedCategory?.name ?? ""
                        }
                        
                    }
                        else{
                            cell.categorynameLBL.text = ""
                            cell.profileIMG.image = UIImage(named: "profileimgIfnodata")
                        }
                    }
                    cell.statusBTNtapped.tag = indexPath.row
                    
                    DispatchQueue.main.async {
                        tagimgselect = self.taggedContacts?[indexPath.row].contactUserId?.recommendedUserId?.first?.profilePic ?? ""
                    }
            //Mark:-TagRecommended
                   // DispatchQueue.main.async {
                        let rec = self.taggedContacts?[indexPath.row].contactUserId?.recommendedUserId?.count
                        tagheadcount = rec ?? 0
                       
                        tagcvcount = self.taggedContacts?[indexPath.row].contactUserId?.recommendedUserId?.count ?? 0
                        
                        print("tagheadcount",rec ?? 0)
                         // cell.tagrecommendedCVwidth.constant = CGFloat(25 * tagheadcount)
                        if tagheadcount > 0 {
                            cell.tagRecomCount.text = "\(tagheadcount)"
                            cell.tagRecommendCV.reloadData()
                            
                        }
                        else {
                            cell.tagRecomCount.text = ""
                            cell.tagRecommendCV.reloadData()
                        }
                   // }
                   
                    
                    //inez , invite status
                    DispatchQueue.main.async {
//                        if self.taggedContacts?[indexPath.row].notInUs == true {
//                            cell.statusBTNtapped.isUserInteractionEnabled = false
////                            cell.categorynameLBL.text = "Not in USA"
//                            cell.statusView.backgroundColor = self.hexStringToUIColor(hex: "#828685")
//                            cell.statusView.isHidden = true
//                            cell.dotBTN.isUserInteractionEnabled = false
//
//                        }
                        if  self.taggedContacts?[indexPath.row].isUser == true{
//                            cell.statusnameLBL.text = ""
                            cell.statusnameLBL.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                            cell.statusBTNtapped.isUserInteractionEnabled = false
                            //  cell.statusView.backgroundColor = self.hexStringToUIColor(hex:"#33CB98")
                            cell.statusView.isHidden = true
                            cell.profileIMG.layer.borderColor = #colorLiteral(red: 0.2, green: 0.7960784314, blue: 0.5960784314, alpha: 1)
                            cell.profileIMG.layer.borderWidth = 2.0
                        }
//                        else if self.taggedContacts?[indexPath.row].isReferred == true && self.taggedContacts?[indexPath.row].notInUs == false
//                        {
////                            cell.statusnameLBL.text = "invited"
//                            cell.statusnameLBL.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//                            cell.statusBTNtapped.isUserInteractionEnabled = true
//                            cell.statusView.backgroundColor = self.hexStringToUIColor(hex:"#FBBC05")
//                            cell.statusView.isHidden = false
//
//                        }
//                                                else if self.taggedContacts?[indexPath.row].isReferred == false && self.taggedContacts?[indexPath.row].isUser == false {
//                                                    cell.statusnameLBL.text = ""
//                                                    cell.statusBTNtapped.isUserInteractionEnabled = false
//                                                    cell.statusView.isHidden = true
//                                                    // cell.statusView.backgroundColor = self.hexStringToUIColor(hex:"#33CB98")
//                                                }
                    }
//                    if cell.categorynameLBL.text == "Not in USA" {
//                        cell.categorynameLBL.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
//                    }
//                    else{
                        
//                    }
                    let recomendedcount = self.taggedContacts?[indexPath.row].contactUserId?.recommendedUserId?.count ?? 0
                 
                    if recomendedcount >= 4 {
                               //  stackviewHeight.constant = 27 * 3
                        cell.recomTagView1.isHidden = false
                        cell.recomTagView2.isHidden = false
                        cell.recomTagView3.isHidden = false
                        cell.recomTagView4.isHidden = false
                //cell.recomstackIMG1.image = UIImage(named: "profileimgIfnodata")
                        cell.recomstackIMG1.kf.setImage(with: URL(string: kImageUrl + (self.taggedContacts?[indexPath.row].contactUserId?.recommendedUserId?[0].profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                        cell.recomstackIMG2.kf.setImage(with: URL(string: kImageUrl + (self.taggedContacts?[indexPath.row].contactUserId?.recommendedUserId?[1].profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                        cell.recomstackIMG3.kf.setImage(with: URL(string: kImageUrl + (self.taggedContacts?[indexPath.row].contactUserId?.recommendedUserId?[2].profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                        cell.recomstackIMG4.kf.setImage(with: URL(string: kImageUrl + (self.taggedContacts?[indexPath.row].contactUserId?.recommendedUserId?[3].profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                        cell.recomstackLbl.text = String(recomendedcount)
                             }
            else  if recomendedcount == 3 {
                cell.recomTagView1.isHidden = false
                cell.recomTagView2.isHidden = false
                cell.recomTagView3.isHidden = false
                cell.recomTagView4.isHidden = true
                cell.recomstackIMG1.kf.setImage(with: URL(string: kImageUrl + (self.taggedContacts?[indexPath.row].contactUserId?.recommendedUserId?[0].profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                cell.recomstackIMG2.kf.setImage(with: URL(string: kImageUrl + (self.taggedContacts?[indexPath.row].contactUserId?.recommendedUserId?[1].profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                cell.recomstackIMG3.kf.setImage(with: URL(string: kImageUrl + (self.taggedContacts?[indexPath.row].contactUserId?.recommendedUserId?[2].profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                cell.recomstackLbl.text = String(recomendedcount)
                     }
                    
            else  if recomendedcount == 2 {
                        //  stackviewHeight.constant = 27 * 3
                 cell.recomTagView1.isHidden = false
                 cell.recomTagView2.isHidden = false
                 cell.recomTagView3.isHidden = true
                 cell.recomTagView4.isHidden = true
                 cell.recomstackIMG1.kf.setImage(with: URL(string: kImageUrl + (self.taggedContacts?[indexPath.row].contactUserId?.recommendedUserId?[0].profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                 cell.recomstackIMG2.kf.setImage(with: URL(string: kImageUrl + (self.taggedContacts?[indexPath.row].contactUserId?.recommendedUserId?[1].profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                cell.recomstackLbl.text = String(recomendedcount)
                      }
            else  if recomendedcount == 1 {
                        //  stackviewHeight.constant = 27 * 3
                 cell.recomTagView1.isHidden = false
                 cell.recomTagView2.isHidden = true
                 cell.recomTagView3.isHidden = true
                 cell.recomTagView4.isHidden = true
                 cell.recomstackIMG1.kf.setImage(with: URL(string: kImageUrl + (self.taggedContacts?[indexPath.row].contactUserId?.recommendedUserId?[0].profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                cell.recomstackLbl.text = String(recomendedcount)
                      }
                    else {
                        
                         }
                    
                    
                }
                //MARK: - Cell for row - Possible providers
                else if indexPath.section == 1 {
                    cell.generalViewHeight.constant = 85
                    cell.recomStackview.isHidden = true
                    cell.tagRecommendedView.isHidden = true
                    cell.categorynameLBL.text = ""
                    cell.nameLBL.text = self.possibleProviders?[indexPath.row].name ??  self.possibleProviders?[indexPath.row].twilioCallerName
                    // possible contact img
                    DispatchQueue.main.async {
                        if self.possibleProviders?[indexPath.row].contactUserId != nil && self.possibleProviders?[indexPath.row].isUser == true {
                            cell.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.possibleProviders?[indexPath.row].contactUserId?.profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                        }
                    }
                    //                    DispatchQueue.main.async {
                    //
                    //                            let count = contactList.possibleProviders[indexPath.row].recommendedCount as? Int
                    //                            if count != 0{
                    //                                cell.generalthumbIMG.isHidden = false
                    //                                cell.generalthumbcount.text = "\(count ?? 0)"
                    //                            }
                    //                            else{
                    //                                cell.generalthumbIMG.isHidden = true
                    //                                cell.generalthumbcount.text = ""
                    //                            }
                    //                        }
                    
                    
                    //inez , invite status
                    DispatchQueue.main.async {
                        if self.possibleProviders?[indexPath.row].notInUs == true {
                            cell.statusBTNtapped.isUserInteractionEnabled = false
                            cell.categorynameLBL.text = "Not in USA"
                            cell.statusView.backgroundColor = self.hexStringToUIColor(hex: "#828685")
                            cell.statusView.isHidden = true
                            cell.dotBTN.isUserInteractionEnabled = false
                            cell.profileIMG.layer.borderWidth = 0.0
                        }else if  self.possibleProviders?[indexPath.row].isUser == true{
                            cell.statusnameLBL.text = ""
                            cell.statusnameLBL.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                            cell.statusBTNtapped.isUserInteractionEnabled = false
                            //cell.statusView.backgroundColor = self.hexStringToUIColor(hex:"#33CB98")
                            cell.statusView.isHidden = true
                            cell.profileIMG.layer.borderColor = #colorLiteral(red: 0.2, green: 0.7960784314, blue: 0.5960784314, alpha: 1)
                            cell.profileIMG.layer.borderWidth = 2.0
                        }
                       // else if self.possibleProviders?[indexPath.row].isReferred == true && self.possibleProviders?[indexPath.row].notInUs == false
//                        {
//                            cell.statusnameLBL.text = "invited"
//                            cell.statusBTNtapped.isUserInteractionEnabled = true
//                            cell.statusnameLBL.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//                            cell.statusView.backgroundColor = self.hexStringToUIColor(hex:"#FBBC05")
//                            cell.statusView.isHidden = false
//                            cell.profileIMG.layer.borderWidth = 0.0
//
//                        }
                        //                        else if self.possibleProviders?[indexPath.row].isReferred == false && self.possibleProviders?[indexPath.row].isUser == false {
                        //                            cell.statusnameLBL.text = ""
                        //                            cell.statusBTNtapped.isUserInteractionEnabled = false
                        //                            cell.statusView.isHidden = true
                        //                            // cell.statusView.backgroundColor = self.hexStringToUIColor(hex:"#33CB98")
                        //                        }
                        
                    }
                    
                    if cell.categorynameLBL.text == "Not in USA" {
                        cell.categorynameLBL.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
                        
                    }
                    else{
                        cell.categorynameLBL.textColor = #colorLiteral(red: 0, green: 0.2823529412, blue: 0.1882352941, alpha: 1)
                        
                    }
                    
                    //MARK:- Normal List
                }else {
                    //MARK: - Cell forrow --Normal list ABCD....
                    cell.generalViewHeight.constant = 85
                    DispatchQueue.main.async {
                        
                        cell.recomStackview.isHidden = true
                        cell.tagRecommendedView.isHidden = true
                        var cityl = ""
                        var statel = ""
                        cell.categorynameLBL.text = ""
                        cell.nameLBL.text = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].name ??  self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].twilioCallerName
                        cell.Mobnumber = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].phoneNumber ?? ""
                        cell.CCode = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].countryCode ?? ""
                        cell.CId = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row]._id ?? ""
                        let city = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].contactUserId?.city ?? ""
                        let state = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].contactUserId?.state ?? ""
                        if city != ""{
                            cityl = city
                        }
                        if state != "" {
                            statel = " , \(state)"
                        }
                        cell.categorynameLBL.text = "\(city)\(state)"
                        print("normal list locations",city,state)
                        cell.categorynameLBL.textColor = #colorLiteral(red: 0, green: 0.2823529412, blue: 0.1882352941, alpha: 1)
                    }
                    //nomrmal list ccontact img
                    DispatchQueue.main.async {
                        
                        cell.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].contactUserId?.profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                        
                    }
                    // recomended thumb
                    
                    //                    DispatchQueue.main.async {
                    //
                    //                            let count = contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].recommendedCount as? Int
                    //                            if count != 0{
                    //                                cell.generalthumbIMG.isHidden = false
                    //                                cell.generalthumbcount.text = "\(count ?? 0)"
                    //                            }
                    //                            else{
                    //                                cell.generalthumbIMG.isHidden = true
                    //                                cell.generalthumbcount.text = ""
                    //                            }
                    //
                    //
                    //                    }
                    //                    print("chakkaz")
                    //                    print(.normalList[indexPath.section - 2].contactsList[indexPath.row].name ??  contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].twilioCallerName)
                    //                    print(contactList.taggedContact[indexPath.row].notInUs)
                    DispatchQueue.main.async {
                        if self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].notInUs == true {
                            cell.statusBTNtapped.isUserInteractionEnabled = false
                            cell.categorynameLBL.text = "Not in USA"
                            cell.categorynameLBL.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
                            cell.dotBTN.isUserInteractionEnabled = false
                            cell.statusView.isHidden = true
                            cell.profileIMG.layer.borderWidth = 0.0
                        }else if  self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].isUser == true{
                            cell.statusnameLBL.text = ""
                            cell.statusnameLBL.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                            cell.statusBTNtapped.isUserInteractionEnabled = false
                            // cell.statusView.backgroundColor = self.hexStringToUIColor(hex:"#33CB98")
                            cell.statusView.isHidden = true
                            cell.profileIMG.layer.borderColor = #colorLiteral(red: 0.2, green: 0.7960784314, blue: 0.5960784314, alpha: 1)
                            cell.profileIMG.layer.borderWidth = 2.0
                        }
                        else if self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].isReferred == true && self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].notInUs == false
                        {
                            cell.statusnameLBL.text = "invited"
                            cell.statusnameLBL.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                            cell.statusBTNtapped.isUserInteractionEnabled = true
                            cell.statusView.backgroundColor = self.hexStringToUIColor(hex:"#FBBC05")
                            cell.statusView.isHidden = false
                            cell.profileIMG.layer.borderWidth = 0.0
                            
                        }else if self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].isReferred == false && self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].isUser == false {
                            cell.statusnameLBL.text = "invite"
                            cell.statusnameLBL.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                            cell.statusBTNtapped.isUserInteractionEnabled = true
                            cell.statusView.isHidden = false
                            cell.statusView.backgroundColor = self.hexStringToUIColor(hex:"#4285F4")
                            cell.profileIMG.layer.borderWidth = 0.0
                        }
                        
                    }
                    if cell.categorynameLBL.text == "Not in USA" {
                        cell.categorynameLBL.textColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
                    }
                    else{
                        cell.categorynameLBL.textColor = #colorLiteral(red: 0, green: 0.2823529412, blue: 0.1882352941, alpha: 1)
                    }
                    
                    cell.statusBTNtapped.tag = indexPath.row
                    
                }
                //
                
            }
        }else{
            cell.providerViewHeight.constant = 110
            cell.prorecomStackview.isHidden = false
            //Provider contact name
            cell.providerNameLbl.text = self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row].name
//            ?? providerlist?.data[indexPath.row].contacts[indexPath.row].twilioCallerName
            
            if self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row].isUser ?? false{
                cell.prosignupIMG.isHidden = false // img will show
            }else{
                cell.prosignupIMG.isHidden = true // img will be hidden
            }
            
            //Location:-
            let city = self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row].areaOfService.first?.city ?? ""
            let stateshortcode = self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row].areaOfService.first?.stateShortCode ?? ""
            
            if city != "" && stateshortcode != ""{
                cell.proLocationLbl.text = "\(city) , \(stateshortcode)"
            }else{
                if city == ""{
                    cell.proLocationLbl.text = stateshortcode
                }else{
                    cell.proLocationLbl.text = city
                }
            }
          //  9495667581
            //provider contact image
            DispatchQueue.main.async {
                cell.providerProfileIMG.kf.setImage(with: URL(string: kImageUrl + (self.providerlistpg?[indexPath.section].data?.categoryImage ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                
                //                if contactList.taggedContact[indexPath.row].contactUserId != nil && contactList.taggedContact[indexPath.row].isUser == true {
                //                    cell.profileIMG.kf.setImage(with: URL(string: kImageUrl + (contactList.taggedContact[indexPath.row].contactUserId?.profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                //                  }
                //
            }
            
            //            joined icon showing
            
            
            //            if self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].needCatagoryReview ?? false{
            //                cell.ProviderView.backgroundColor = UIColor.cyan
            //            }
            //            else{
            //                cell.ProviderView.backgroundColor = UIColor.systemBackground
            //            }
            
            //needs review color / img
            if self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row].needCatagoryReview == true{
                cell.providerthumbIMG.isHidden = true
                cell.ProviderView.backgroundColor = UIColor(red: 0.92, green: 0.26, blue: 0.21, alpha: 0.10)
                cell.providerneedsreviewIMG.isHidden = false
                cell.providerthumbIMG.image = UIImage(named: "needsReview")
                
            }
            else{
                cell.providerneedsreviewIMG.isHidden = true
                cell.ProviderView.backgroundColor = UIColor.white
            }
            
            //recomended like
            if self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row].isRecommended == true{
                cell.providerthumbIMG.isHidden = false
            }
            else{
                cell.providerthumbIMG.isHidden = true
            }
            
            
            //head section
            let rec = self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row].recomended.first?.user.count
            headcount = rec ?? 0
            //            print("headcount",rec)
            //
            cell.recommendedCVwidth.constant = CGFloat(20 * headcount)
            if headcount > 0 {
                cell.countLabel.text = "\(headcount)"
                cell.recommendedCV.reloadData()
            }
            else {
                cell.countLabel.text = ""
            }
            
         
            if let contact = self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row] {

                let recomcontact = contact.recomended
                let prorecomendedcount = recomcontact.first?.user.count ?? 0
                let recommendedUsers = recomcontact.first?.user ?? []

                if prorecomendedcount >= 4 {
                    cell.prorecomTagView1.isHidden = false
                    cell.prorecomTagView2.isHidden = false
                    cell.prorecomTagView3.isHidden = false
                    cell.prorecomTagView4.isHidden = false
                    cell.prorecomstackIMG1.kf.setImage(with: URL(string: kImageUrl + (recommendedUsers[0].profilePic)), placeholder: UIImage(named: "profileimgIfnodata"))
                    cell.prorecomstackIMG2.kf.setImage(with: URL(string: kImageUrl + (recommendedUsers[1].profilePic)), placeholder: UIImage(named: "profileimgIfnodata"))
                    cell.prorecomstackIMG3.kf.setImage(with: URL(string: kImageUrl + (recommendedUsers[2].profilePic)), placeholder: UIImage(named: "profileimgIfnodata"))
                    cell.prorecomstackIMG4.kf.setImage(with: URL(string: kImageUrl + (recommendedUsers[3].profilePic)), placeholder: UIImage(named: "profileimgIfnodata"))
                    cell.prorecomstackLbl.text = String(prorecomendedcount)
                }

               else if prorecomendedcount == 3 {
                    cell.prorecomTagView1.isHidden = false
                    cell.prorecomTagView2.isHidden = false
                    cell.prorecomTagView3.isHidden = false
                    cell.prorecomTagView4.isHidden = true
                    cell.prorecomstackIMG1.kf.setImage(with: URL(string: kImageUrl + (recommendedUsers[0].profilePic)), placeholder: UIImage(named: "profileimgIfnodata"))
                    cell.prorecomstackIMG2.kf.setImage(with: URL(string: kImageUrl + (recommendedUsers[1].profilePic)), placeholder: UIImage(named: "profileimgIfnodata"))
                    cell.prorecomstackIMG3.kf.setImage(with: URL(string: kImageUrl + (recommendedUsers[2].profilePic)), placeholder: UIImage(named: "profileimgIfnodata"))
                   cell.prorecomstackLbl.text = String(prorecomendedcount)
                }

               else if prorecomendedcount == 2 {
                    cell.prorecomTagView1.isHidden = false
                    cell.prorecomTagView2.isHidden = false
                    cell.prorecomTagView3.isHidden = true
                    cell.prorecomTagView4.isHidden = true
                    cell.prorecomstackIMG1.kf.setImage(with: URL(string: kImageUrl + (recommendedUsers[0].profilePic)), placeholder: UIImage(named: "profileimgIfnodata"))
                    cell.prorecomstackIMG2.kf.setImage(with: URL(string: kImageUrl + (recommendedUsers[1].profilePic)), placeholder: UIImage(named: "profileimgIfnodata"))
                }

               else if prorecomendedcount == 1 {
                    cell.prorecomTagView1.isHidden = false
                    cell.prorecomTagView2.isHidden = true
                    cell.prorecomTagView3.isHidden = true
                    cell.prorecomTagView4.isHidden = true
                    cell.prorecomstackIMG1.kf.setImage(with: URL(string: kImageUrl + (recommendedUsers[0].profilePic)), placeholder: UIImage(named: "profileimgIfnodata"))
                   cell.prorecomstackLbl.text = String(prorecomendedcount)
                }
            }
            
            
        }
        
        // general,provider tab view show and hide
        if selectedBTN == 0 {
            cell.generalView.isHidden = false
            cell.ProviderView.isHidden = true
        }else{
            cell.ProviderView.isHidden = false
            cell.generalView.isHidden = true
        }
        
        
        cell.dotBTN.tag = indexPath.row
        //        cell.
        cell.dotBTN.showsMenuAsPrimaryAction = true
        
        
        cell.providerDotsBTN.tag = indexPath.row
        cell.providerDotsBTN.showsMenuAsPrimaryAction = true
        //MARK: - popup menu
        if selectedBTN == 0 {
            //MARK: - Call
            let Call = UIAction(title: "Call",
                                image: UIImage(named: "Call")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { _ in
                // Perform action
                var getmobile = ""
                if indexPath.section == 0 {
                    print("call fromtagged")
                    getmobile = self.taggedContacts?[indexPath.row].phoneNumber ?? ""
                }else if indexPath.section == 1{
                    print("call possible provider")
                    getmobile = self.possibleProviders?[indexPath.row].phoneNumber ?? ""
                }
                else{
                    print("call normal list")
                    getmobile = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].phoneNumber ?? ""
                }
                
                let url:NSURL = NSURL(string: "telprompt:\(getmobile)")!
                UIApplication.shared.openURL(url as URL)
                //                print("calling")
            }
            //MARK: - Text
            let Text = UIAction(title:"Text",
                                image: UIImage(named: "Chat")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { action in
                // Perform action
                var getmobile = ""
                if indexPath.section == 0 {
                    print("call fromtagged")
                    getmobile = self.taggedContacts?[indexPath.row].phoneNumber ?? ""
                }else if indexPath.section == 1{
                    print("call possible provider")
                    getmobile = self.possibleProviders?[indexPath.row].phoneNumber ?? ""
                }
                else{
                    print("call normal list")
                    getmobile = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].phoneNumber ?? ""
                }
                
                var url:NSURL = NSURL(string: "sms:\(getmobile)")!
                UIApplication.shared.openURL(url as URL)
                print("texting")
            }
            
            //MARK: - Share
            let Share = UIAction(title: "Share",
                                 image: UIImage(named: "Share")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { action in
                // Perform action
                if indexPath.section == 0 {
                    print("share fromtagged")
                    let getname = self.taggedContacts?[indexPath.row].name ?? ""
                    let getmobile = self.taggedContacts?[indexPath.row].phoneNumber ?? ""
                    
                    let contact = createContact(fname: getname, lname: "", mob: getmobile)
                    
                    do {
                        try self.shareContacts(contacts: [contact])
                    }
                    catch {
                        print("failed to share contact with some unknown reasons")
                    }
                }else if indexPath.section == 1{
                    print("share possible provider")
                    let getname = self.possibleProviders?[indexPath.row].name ?? ""
                    let getmobile = self.possibleProviders?[indexPath.row].phoneNumber ?? ""
                    let contact = createContact(fname: getname, lname: "", mob: getmobile)
                    
                    do {
                        try self.shareContacts(contacts: [contact])
                    }
                    catch {
                        print("failed to share contact with some unknown reasons")
                    }
                }
                else{
                    print("share normal list")
                    let getmobile = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].phoneNumber ?? ""
                    let getname = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].name ?? ""
                    let contact = createContact(fname: getname, lname: "", mob: getmobile)
                    
                    do {
                        try self.shareContacts(contacts: [contact])
                    }
                    catch {
                        print("failed to share contact with some unknown reasons")
                    }
                }
                func createContact(fname:String,lname:String,mob:String) -> CNContact {
                    // Creating a mutable object to add to the contact
                    let contact = CNMutableContact()
                    contact.imageData = NSData() as Data // The profile picture as a NSData object
                    contact.givenName = fname
                    contact.familyName = lname
                    contact.phoneNumbers = [CNLabeledValue(
                        label:CNLabelPhoneNumberiPhone,
                        value:CNPhoneNumber(stringValue:mob))]
                    
                    return contact
                }
                // text to share
                let text = "This is some text that I want to share."
                
                // set up activity view controller
                let textToShare = [ text ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                
                // exclude some activity types from the list (optional)
                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                
                // present the view controller
                //                self.present(activityViewController, animated: true, completion: nil)
                print("sharing")
            }
            let Edit = UIAction(title: "Edit",
                                image: UIImage(named: "Editcontact")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
            ) { action in
                // Perform action
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "EditGeneralContactVC") as! EditGeneralContactVC
                if indexPath.section == 0{
                    vc.getcontactid = self.taggedContacts?[indexPath.row]._id ?? ""
                    vc.getname = self.taggedContacts?[indexPath.row].name ?? ""
                    vc.getmobile = self.taggedContacts?[indexPath.row].phoneNumber ?? ""
                    vc.getcountry = self.taggedContacts?[indexPath.row].countryCode ?? ""
                }else if indexPath.section == 1{
                    vc.getcontactid = self.possibleProviders?[indexPath.row]._id ?? ""
                    vc.getname = self.possibleProviders?[indexPath.row].name ?? ""
                    vc.getmobile = self.possibleProviders?[indexPath.row].phoneNumber ?? ""
                    vc.getcountry = self.possibleProviders?[indexPath.row].countryCode ?? ""
                }else{
                    vc.getcontactid = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row]._id ?? ""
                    vc.getmobile = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].phoneNumber ?? ""
                    vc.getname = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].name ?? ""
                    vc.getcountry = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].countryCode ?? ""
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
                //                print("opening")
            }
            
            cell.dotBTN.menu = UIMenu(title:"", children: [Call, Text, Share])
        }else{
            //MARK: - menu functions for provider tab
            let Call = UIAction(title: "Call",
                                image: UIImage(named: "Call")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { _ in
                // Perform action
                let number = "" ?? ""
                var url:NSURL = NSURL(string: "telprompt:\(number)")!
                UIApplication.shared.openURL(url as URL)
                //                print("calling")
            }
            
            let Text = UIAction(title:"Text",
                                image: UIImage(named: "Chat")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { action in
                // Perform action
                let number = 5555555555
                var url:NSURL = NSURL(string: "sms:\(number)")!
                UIApplication.shared.openURL(url as URL)
                //                print("texting")
            }
            
            
            let Share = UIAction(title: "Share",
                                 image: UIImage(named: "Share")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { action in
                // Perform action
                func createContact() -> CNContact {
                    
                    // Creating a mutable object to add to the contact
                    let contact = CNMutableContact()
                    
                    contact.imageData = NSData() as Data // The profile picture as a NSData object
                    contact.givenName = "John"
                    contact.familyName = "Appleseed"
                    contact.phoneNumbers = [CNLabeledValue(
                        label:CNLabelPhoneNumberiPhone,
                        value:CNPhoneNumber(stringValue:"(408) 555-0126"))]
                    
                    return contact
                }
                // text to share
                let text = "This is some text that I want to share."
                
                // set up activity view controller
                let textToShare = [ text ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                
                // exclude some activity types from the list (optional)
                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                
                // present the view controller
                self.present(activityViewController, animated: true, completion: nil)
                //                print("sharing")
            }
            
            let Feedback = UIAction(title: "Feedback",
                                    image: UIImage(named: "Feedback")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
            ) { action in
                // Perform action
                UserDefaults.standard.setValue(self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row].contactUserId, forKey: "KcontactuserId")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "FeedBackVC") as! FeedBackVC
                vc.getcontactid = self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row].contactUserId ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                //                print("typing")
            }
            
            let Report = UIAction(title: "Report",
                                  image: UIImage(named: "Report")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
            ) { action in
                // Perform action
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "ReportVC") as! ReportVC
                vc.getcontactid = self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row].contactUserId ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                //                print("reporting")
            }
            
            let Edit = UIAction(title: "Edit",
                                image: UIImage(named: "Editcontact")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
            ) { action in
                // Perform action
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "EditProviderScrollVC") as! EditProviderScrollVC
                vc.getcontactid = self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row]._id ?? ""
                vc.getmobile = self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row].phoneNumber ?? ""
                
                vc.getname = self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row].name ?? ""
                vc.getcountry = self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row].countryCode ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                //                print("opening")
            }
            
            cell.providerDotsBTN.menu = UIMenu(title:"", children: [Call, Text, Share, Feedback, Report, Edit])
        }
        
        return cell
    }
    //MARK: - image with name
    func imageWith(name: String?) -> UIImage? {
        let frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        let nameLabel = UILabel(frame: frame)
        nameLabel.textAlignment = .center
        nameLabel.backgroundColor = .lightGray
        nameLabel.textColor = .white
        nameLabel.font = UIFont.boldSystemFont(ofSize: 20)
        var initials = ""
        if let initialsArray = name?.components(separatedBy: " ") {
            if let firstWord = initialsArray.first {
                if let firstLetter = firstWord.first {
                    initials += String(firstLetter).capitalized }
            }
            if initialsArray.count > 1, let lastWord = initialsArray.last {
                if let lastLetter = lastWord.first { initials += String(lastLetter).capitalized
                }
            }
        } else {
            return nil
        }
        nameLabel.text = initials
        UIGraphicsBeginImageContext(frame.size)
        if let currentContext = UIGraphicsGetCurrentContext() {
            nameLabel.layer.render(in: currentContext)
            let nameImage = UIGraphicsGetImageFromCurrentImageContext()
            return nameImage
        }
        return nil
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------------
    
    // provider Swipe actions
    func tableView(_ tableView: UITableView,
                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let action = UIContextualAction(style: .normal,
                                        title: "Provider") { [weak self] (action, view, completionHandler) in
            self?.handleLeftaction()
            completionHandler(true)
            
            //  let contactList = self?.contactlist
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "TagProviderScrollVC") as! TagProviderScrollVC
            
            if indexPath.section == 0{
                vc.fromwhichlist = "tagged"
                vc.getcontactid = self?.taggedContacts?[indexPath.row]._id ?? ""
                vc.getProfileName = self?.taggedContacts?[indexPath.row].name ?? ""
                vc.getProfileIMG = self?.taggedContacts?[indexPath.row].contactUserId?.profilePic ?? ""
                
                //                vc.phNo = contactList?.taggedContact[indexPath.row].phoneNumber ?? ""
                vc.phNo = self?.taggedContacts?[indexPath.row].contactFullPhoneNumber ?? ""
                //                vc.countrycode = contactList?.taggedContact[indexPath.row].countryCode ?? ""
                //if contactList?.taggedContact[indexPath.row].isUser == true{
                //                    contactList.taggedContact[indexPath.row].contactUserId?.taggedCategories[0].taggedCategoryIds?.name
                if self?.taggedContacts?[indexPath.row].contactUserId?.categoryIds?.count ?? 0 > 0 {
                    
                    vc.itemz = self?.taggedContacts?[indexPath.row].contactUserId?.categoryIds?.first?.name ?? ""
                    vc.tagCatID = self?.taggedContacts?[indexPath.row].contactUserId?.categoryIds?.first?._id ?? ""
                }
                else{
                    vc.itemz = self?.taggedContacts?[indexPath.row].contactUserId?.taggedCategories?.first?.taggedCategory?.name ?? ""
                    vc.tagCatID = self?.taggedContacts?[indexPath.row].contactUserId?.taggedCategories?.first?.taggedCategory?._id ?? ""
                }
//                print(self?.taggedContacts?[indexPath.row].contactUserId?.taggedCategories?.first?.taggedCategoryIds?.name ?? "")
//                vc.itemz = self?.taggedContacts?[indexPath.row].contactUserId?.taggedCategories?.first?.taggedCategoryIds?.name ?? ""
//                vc.tagCatID = self?.taggedContacts?[indexPath.row].contactUserId?.taggedCategories?.first?.taggedCategoryIds?.id ?? ""
                
                // }
                //                else{
                ////                    vc.items = contactList.taggedContact[indexPath.row].contactUserId?.taggedCategories
                //                }
                self?.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.section == 1{
                vc.getcontactid = self?.possibleProviders?[indexPath.row]._id ?? ""
                vc.getProfileName = self?.possibleProviders?[indexPath.row].name ?? ""
                vc.fromwhichlist = "possible"
                self?.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                
                vc.getcontactid = self?.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row]._id ?? ""
                vc.getProfileName = self?.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].name ?? ""
                vc.fromwhichlist = "normal"
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        action.backgroundColor = UIColor(red: 0.2, green: 0.8, blue: 0.6, alpha: 0.2)
        if selectedBTN == 0{
            if indexPath.section == 0{
                if self.taggedContacts?[indexPath.row].notInUs == true {
                    print("No swipe needed")
                }
                else{
                    return UISwipeActionsConfiguration(actions: [action])
                }
            }
            else if indexPath.section == 1 {
                if self.possibleProviders?[indexPath.row].notInUs == true {
                    print("No swipe needed")
                }
                else{
                    return UISwipeActionsConfiguration(actions: [action])
                }
            }
            else{
                if self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].notInUs == true {
                    print("No swipe needed")
                }
                else{
                    return UISwipeActionsConfiguration(actions: [action])
                }
            }
            
            
        }
        return UISwipeActionsConfiguration()
    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if selectedBTN == 0{
            if indexPath.section == 0 || indexPath.section == 1 {
                
                if indexPath.section == 0{
                    if self.taggedContacts?[indexPath.row].notInUs == true {
                        print("No swipe needed")
                    }
                    else{
                        let rightaction = UIContextualAction(style: .normal,
                                                             title: "General") {(action, view, completionHandler) in
                            
                            print("call api for swipe")
                            let getcontactid = self.taggedContacts?[indexPath.row]._id ?? ""
                            self.taggeneralapi(contactidfromlist: getcontactid)
                            completionHandler(true)
                        }
                        rightaction.backgroundColor = UIColor(red: 0.984, green: 0.737, blue: 0.02, alpha: 0.5)
                        return UISwipeActionsConfiguration(actions: [rightaction])
                    }
                    
                }
                else if indexPath.section == 1{
                    if self.possibleProviders?[indexPath.row].notInUs == true {
                        print("No swipe needed")
                    }
                    else{
                        let rightaction = UIContextualAction(style: .normal,
                                                             title: "General") {(action, view, completionHandler) in
                            print("call api for swipe")
                            let getcontactid = self.possibleProviders?[indexPath.row]._id ?? ""
                            self.taggeneralapi(contactidfromlist: getcontactid)
                            completionHandler(true)
                        }
                        rightaction.backgroundColor = UIColor(red: 0.984, green: 0.737, blue: 0.02, alpha: 0.5)
                        return UISwipeActionsConfiguration(actions: [rightaction])
                    }
                    
                }
            }
        }
        else {
            let rightaction = UIContextualAction(style: .normal,
                                                 title: "General") { [weak self] (action, view, completionHandler) in
                let getcontactid = self?.providerlist?.data[indexPath.row].contacts[indexPath.row]._id ?? ""
                self?.changeproviderapi(contactidfromlist: getcontactid)
                //                self?.handleRightaction()
                
                completionHandler(true)
                
            }
            
            rightaction.backgroundColor = UIColor(red: 0.984, green: 0.737, blue: 0.02, alpha: 0.5)
            return UISwipeActionsConfiguration(actions: [rightaction])
        }
        
        return UISwipeActionsConfiguration()
    }
    
    
    func tableView(_ tableView: UITableView,
                   editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    //  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    //       return true
    //   }
    
    
    //
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //       return self.contactlist[section].contactsList.count == 0 ? 0 : 22
        
        if selectedBTN == 0 {
            if  section == 0 {
                return self.taggedContacts?.count ?? 0 != 0 ? 40 : 0
            }else if section == 1{
                return self.possibleProviders?.count ?? 0 != 0 ? 40 : 0
            }
            else{
                return self.chunkedlist[section - 2].contactsList?.count ?? 0 != 0 ? 40 : 0
            }
        }else{
            return self.providerlistpg?[section].data?.contacts.count != 0 ? 40 : 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedBTN == 0 {
            
            if  indexPath.section == 0 {
                if issectionzeroOpened{
                    return UITableView.automaticDimension
                }
                else{
                    return 0
                }
                //
                //                return self.taggedContacts?.count == 0 ? 0 : (self.taggedContacts?[0].sectionOpened ?? false ? UITableView.automaticDimension : 0)
            }else if indexPath.section == 1{
                if issectiononeOpened{
                    return UITableView.automaticDimension
                }else{
                    return 0
                }
                //                return self.possibleProviders?.count == 0 ? 0 :  (self.possibleProviders?[0].sectionOpened ?? false ? UITableView.automaticDimension : 0)
            }
            else{
                return self.chunkedlist[indexPath.section - 2].contactsList?.count == 0 ? 0 : (self.chunkedlist[indexPath.section - 2].sectionOpened ?? true ? UITableView.automaticDimension : 0)
            }
        }else{
//            self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row]._id ?? ""
            
            return self.providerlistpg?[indexPath.section].data?.contacts.count == 0 ? 0 : (self.providerlistpg?[indexPath.section].sectionOpened ?? true)  ? UITableView.automaticDimension : 0
//            return self.providerlistpg?.count == 0 ? 0 : UITableView.automaticDimension
//            return UITableView.automaticDimension
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = contactsTBL.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! contactCell
        
        if selectedBTN == 0{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "GeneralContactProfileVC") as! GeneralContactProfileVC
            if indexPath.section == 0 {
                UserDefaults.standard.setValue(self.taggedContacts?[indexPath.row]._id, forKey: "Kcontactid")
                //                print(contactlist?.taggedContact[indexPath.row].id)
                vc.getname = self.taggedContacts?[indexPath.row].name ?? ""
                vc.selecteduserid = self.taggedContacts?[indexPath.row]._id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if indexPath.section == 1 {
                UserDefaults.standard.setValue(self.possibleProviders?[indexPath.row]._id, forKey: "Kcontactid")
                //                print(contactlist?.possibleProviders[indexPath.row].id)
                vc.getname = self.possibleProviders?[indexPath.row].name ?? ""
                vc.selecteduserid = self.possibleProviders?[indexPath.row]._id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
                UserDefaults.standard.setValue(self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row]._id, forKey: "Kcontactid")
                //                print(contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].id)
                vc.getname = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row].name ?? ""
                vc.selecteduserid = self.chunkedlist[indexPath.section - 2].contactsList?[indexPath.row]._id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
        
        
        else {
            //            print("provider")
            //            print(self.providerlist?.data[indexPath.section].contacts[indexPath.row].name ?? "")
            //            (self.providerlist?.data[indexPath.section].contacts[indexPath.row].id ?? "")
            
            UserDefaults.standard.setValue(self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row]._id, forKey: "Kcontactid")
            
            cell.ProviderView.isExclusiveTouch = true
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "ProviderContactProfileVC") as! ProviderContactProfileVC
            vc.selecteduserid = self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row]._id ?? ""
            vc.getname = self.providerlistpg?[indexPath.section].data?.contacts[indexPath.row].name ?? ""
            checkcollectionstatus = "fromlist"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if selectedBTN == 1{ // only for provider tab
//            let lastItem = (self.providerlist?.data[indexPath.section].contacts.count ?? 0) - 1
//            if indexPath.row == lastItem {
//                print("IndexRow\(indexPath.row)")
//                if self.providerlistpaginationcount <= providerlistpagecount && self.contactsTBL.isScrollEnabled{
//                    //                        self.contactsTBL.isScrollEnabled = false
//                    if searchTF.text ?? "" == ""{
//                        providerlistpaginationcount += 1
//                        self.providerlistAPI(searchkey: "", filterkey: "all")
//                    }
//
//                }
//                else{
//                    //                        self.contactsTBL.isScrollEnabled = true
//                    return
//                }
//            }
        }
        
    }
    
    //MARK: - share contact card from 3dot menu
    func shareContacts(contacts: [CNContact]) throws {
        
        guard let directoryURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else {
            return
        }
        
        var filename = NSUUID().uuidString
        
        // Create a human friendly file name if sharing a single contact.
        if let contact = contacts.first, contacts.count == 1 {
            
            if let fullname = CNContactFormatter().string(from: contact) {
                filename = fullname.components(separatedBy: " ").joined(separator: "")
            }
        }
        
        let fileURL = directoryURL
            .appendingPathComponent(filename)
            .appendingPathExtension("vcf")
        
        let data = try CNContactVCardSerialization.data(with: contacts)
        
        print("filename: \(filename)")
        print("contact: \(String(describing: String(data: data, encoding: String.Encoding.utf8)))")
        
        try data.write(to: fileURL, options: [.atomicWrite])
        
        let activityViewController = UIActivityViewController(
            activityItems: [fileURL],
            applicationActivities: nil
        )
        
        present(activityViewController, animated: true, completion: nil)
    }
    //MARK: - SHARE INVTIE LINK WITH TYPE
    func shareShow(inviteto:String = "",type:String = "") {
        var shareText = "Hello, world!"
        var sh = ""
        var ln = ""
        //        let linkToShare = "https://mashoproduct.page.link/?pdtid=\(self.productID)&link=\(self.ProductDetailsData.productdetails.webshare!)"
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "eizlukup.page.link"
        components.path = "/invite"
        
        let queryItem1 = URLQueryItem(name: "invitername", value: "ashik")
        let queryItem2 = URLQueryItem(name: "type", value: "personal")
        components.queryItems = [queryItem1,queryItem2]
        
        guard let linkParameter = components.url else {return}
        print("sharing Link :\(linkParameter.absoluteString)")
        guard let shareLink = DynamicLinkComponents.init(link: linkParameter, domainURIPrefix: "https://eizlukup.page.link") else { return }
        // IOS PARAMETERS
        if let bundleID = Bundle.main.bundleIdentifier {
            shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: bundleID)
        }
        shareLink.iOSParameters?.appStoreID = "1663135116"
        // Android PARAMETERS
        shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: "com.ezlukup")
        // Config MetaData
        shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters ()
        //        shareLink.socialMetaTagParameters?.title = ""
        //        shareLink.socialMetaTagParameters?.descriptionText = self.ProductDetailsData.productdetails.product_description
        //        if let imageString = "" , let imageURL = URL(string: imageString) {
        //            shareLink.socialMetaTagParameters?.imageURL = imageURL
        //        }
        
        guard let longURL = shareLink.url else { return }
        print("The long dynamcLink is :\(longURL)")
        ln = "\(longURL)"
        
        shareLink.shorten { (url, warnings, error) in
            if let error = error {
                print("Oh no! got an error :\(error.localizedDescription)")
                return
            }
            
            if let warnings = warnings {
                for warning in warnings {
                    print("FDL warning :\(warning)")
                }
            }
            
            guard let url = url else { return }
            print("Short url :\(url.absoluteString)")
            sh = "\(url.absoluteString)"
            let vc = UIActivityViewController(activityItems: ["Invite \("") to ezlukup",url], applicationActivities: [])
            self.present(vc, animated: true, completion: nil)
        }
        shareText = "\(sh)"
        print("long URL = \(ln)")
        print("short URL = \(sh)")
        
        
        //            let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        //             present(vc, animated: true, completion: nil)
        //
    }
    
    
    
    //MARK: - Did scroll
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if selectedBTN == 0 {
            if (fkey == "all" || fkey == "") && searchtexter == "" {
                let pos = contactsTBL.contentOffset.y
                if pos > contactsTBL.contentSize.height-100 - scrollView.frame.size.height{
                    guard !isloadinglist else{
                        return
                    }
                    let totalcount = self.contactlistpg?.gettotalcount() ?? 0
                    print("total count showing when pagination = \(totalcount)")
                    print("offset value before increment \(offsetpg)")
                    if totalcount > offsetpg{
                        print("call api")
                        offsetpg += 100
                        //                        self.api()
                        print("offset value after apicall \(offsetpg)")
                    }
                    else{
                        return
                    }
                    
                    
                }
            }
        }
        else{
            let pos = contactsTBL.contentOffset.y
            if pos > contactsTBL.contentSize.height-100 - scrollView.frame.size.height{
                guard !isloadinglist else{
                    return
                }
                
                let totalcount = self.providerlist?.data.count ?? 0
                print("total count showing when pagination = \(totalcount)")
                
                if self.providerlistpaginationcount <= providerlistpagecount && self.contactsTBL.isScrollEnabled{
                    //                    self.contactsTBL.isScrollEnabled = false
                    providerlistpaginationcount += 1
                    //                    self.providerlistAPI(searchkey: "", filterkey: "all")
                }
                else{
                    //                    self.contactsTBL.isScrollEnabled = true
                    return
                }
            }
        }
        
        
    }
    
    //MARK: - Handlers
    private func handleLeftaction() {
        print("tag provider swipe success")
        
    }
    private func handleRightaction() {
        print(" general swipe success")
        
        
    }
    //MARK: - API FUNCTIONS-----------------------------------------------------------------------------------------------------------------------------
    
    //MARK: - OLD LISTING API FUNCTION WITH GENERAL TAB AND PROVIDER TAB
    func api(key:String = "",type:String = "",filterkey:String = "" /*, completion: @escaping(Error?) -> ()*/){
        showActivityIndicator()
        self.isloadinglist = false
        self.nodataLBL.isHidden = true
        //        contactsTBL.isScrollEnabled = false
        var endapi = ""
        var filter = ""
        var limit = 0
        print("dynamic userid = \(userid)")
        print("dynamic tocken = \(token)")
        
        
        if selectedBTN == 0 {
            if filterkey == "" || filterkey == "all"{
                if key == ""{
                    limit = 100
                    print("pagination needed")
                }
                else{
                    limit = 0
                    print("pagination not needed")
                }
            }
            else{
                limit = 0
                print("limit = 0 , no pagination")
            }
        }else{
            limit = 0
            print("limit = 0 , no pagination")
        }
        var params : [String:Any] = [:]
        
        if selectedBTN == 0 {
            endapi = "getContactsSingleApi"
            filter = filterkey
            params = [
                "userId":userid ,
                //                "userId":"63db53e00fca2c6d036dda86",
                "searchKey":key,
                "limit":limit,
                "offset" : offsetpg,
                "filter":filter,
            ] as [String : Any]
            
            let url = kBaseUrl+"\(endapi)"
            print("parameters =",params ?? [] ,"url = ",url)
            
            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
                print("Response from general listing ",response)
                switch (response.result) {
                case .success( let JSON):
                    if let responsedata =  JSON as? [String:Any]  {
                        //                        print("response data = \(responsedata)")
                        //                        let contact = getcontactDataModel(from:responsedata)
                        //                        self.contactlistpg = contact
                        //                        var normalList :[getcontactDataListModel] = []
                        //                        let contactx = (self.contactlistpg?.apinormalList.chunked(on: \.name.first) ?? []).map({ (String($0.0!),Array($0.1)) }).sorted(by: {$0.0.compare($1.0, options: .caseInsensitive) == .orderedAscending })
                        //
                        //                        for contact in contactx {
                        //                            normalList.append(getcontactDataListModel(headername: contact.0, contacts: contact.1))
                        //                        }
                        //                        self.contactlistpg?.normalList = normalList
                        //                        if key != "" || filterkey != "" {
                        //                            self.contactlist = getcontactDataModel(normalList: self.contactlistpg?.normalList ?? [], taggedContact: self.contactlistpg?.taggedContact ?? [], possibleProviders: self.contactlistpg?.possibleProviders ?? [])
                        //                        } else {
                        //                            if let taggedcontactpg = self.taggedContacts, let possibleproviderpg = self.possibleProviders , let normallistpg = self.contactlist?.normalList{
                        //                                self.taggedContacts.append(contentsOf:self.contactlistpg?.taggedContact ?? [] )
                        //                                self.possibleProviders.append(contentsOf: self.contactlistpg?.possibleProviders ?? [])
                        //                                self.contactlist?.normalList.append(contentsOf: self.contactlistpg?.normalList ?? [])
                        //                            }
                        //                            else{
                        //                                self.contactlist = getcontactDataModel(normalList: self.contactlistpg?.normalList ?? [], taggedContact: self.contactlistpg?.taggedContact ?? [], possibleProviders: self.contactlistpg?.possibleProviders ?? [])
                        //                            }
                        //                        }
                        //                                                self.contactlist?.normalList.insert(getcontactDataListModel(headername: "#", contacts: []), at: 0)
                        //                        self.contactlist?.normalList = self.contactlist?.normalList.sorted{$0.Headername?.compare($1.Headername ?? "", options: .caseInsensitive) == .orderedAscending } ?? []
                        //                        let hashIndex = self.contactlist?.normalList.firstIndex(where: {$0.Headername == "#"}) ?? 0
                        //                        for index in 0 ..< (self.contactlist?.normalList.count ?? 0) {
                        //                            if index+1 <= (self.contactlist?.normalList.count ?? 0) - 1 {
                        //                                if self.contactlist?.normalList[index].Headername?.compare(self.contactlist?.normalList[index+1].Headername ?? "", options: .caseInsensitive) == .orderedSame {
                        //                                    self.contactlist?.normalList[index].contactsList.append(contentsOf: self.contactlist?.normalList[index+1].contactsList ?? [])
                        //                                    self.contactlist?.normalList[index+1].contactsList.removeAll()
                        //                                }
                        //                            }
                        //                            let allowedCharacters = CharacterSet(charactersIn: "0123456789+")
                        //                            let characterSet = CharacterSet(charactersIn: self.contactlist?.normalList[index].Headername ?? "")
                        //                            if allowedCharacters.isSuperset(of: characterSet){
                        //                                self.contactlist?.normalList[hashIndex].contactsList.append(contentsOf: self.contactlist?.normalList[index].contactsList ?? [])
                        //                                self.contactlist?.normalList[index].contactsList.removeAll()
                        //                            }
                        //                        }
                        //                        self.taggedContacts = Array(_immutableCocoaArray: NSOrderedSet(array: self.taggedContacts ?? []))
                        //                        self.possibleProviders = Array(_immutableCocoaArray: NSOrderedSet(array: self.possibleProviders ?? []))
                        //                        self.contactlist?.normalList.forEach({
                        //                            $0.contactsList = Array(_immutableCocoaArray: NSOrderedSet(array: $0.contactsList ))
                        //                        })
                        //                        self.nodataLBL.isHidden = self.contactlistpg?.gettotalcount() != 0
                        //                        self.Allcontactlist = self.contactlist
                        //
                        //                        self.remainingcount = (self.taggedContacts.count != 0 ? 1 : 0) +  (self.possibleProviders.count != 0 ? 1 : 0)
                        //
                        //
                        //                        var synccont = UserDefaults.standard.value(forKey: "synccontgeneral") ?? false
                        //
                        //                        if synccont as! Bool == false && self.contactlistpg?.gettotalcount() == 0{
                        //                            self.syncContAlertView.isHidden = false
                        //                        }
                        
                    }
                    //                    self.contactsTBL.reloadData()
                    //                    self.hideActivityIndicator()
                    //                    self.contactsTBL.isScrollEnabled = true
                    
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
            }
        }
    }
    //MARK: - PROVIDER LIST API
    func providerlistAPI(searchkey:String,filterkey:String){
//        showActivityIndicator()
        print("dynamic userid = \(userid)")
        print("dynamic tocken = \(token)")
        print("search key = \(searchkey)")
        print("filterkey = \(filterkey)")
        
        let params = [
            "userId":userid,
            "searchKey":searchkey,
            "filter":filterkey,
            "catgoryIndex":providerlistpaginationcount ] as? [String:Any]
        
        let url = kBaseUrl+"getProviderContacts"
        print("pagination count = \(providerlistpaginationcount)")
        let request =  AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
            print(response)
            switch (response.result) {
            case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
//                    self.providerlistpg = providerResponseModel(fromData:responsedata)
//                    self.providerlist?.count = self.providerlistpg?.count
//
//
//                    self.providerlist?.status = self.providerlistpg?.status
//
//                    if let _ = self.providerlist?.data{
//                        self.providerlist?.data.append(contentsOf: self.providerlistpg?.Data ?? [])
//                    }
//                    else{
//                        self.providerlist = providerResponseModel(fromData: [:])
//                        self.providerlist?.data = self.providerlistpg?.Data ?? []
//                        //save data to Core Data
//                        //                        CoreData.sharedInstance.saveDataOf(contacts: (self.providerlistpg?.Data.first!.providercontacts)!)
//                        //
//
//
//                    }
//
//                    if self.providerlist?.data.count == 0{
//                        self.nodataLBL.isHidden = false
//                    }
//                    if searchkey.isEmpty{
//                        if self.providerlistpaginationcount == 0 {
//                            self.providerlistpagecount = self.providerlistpg?.count ?? 0
//                        }
//                    }
//                    else{
//                        self.providerlistpaginationcount = 0
//                    }
//
//                    self.contactsTBL.reloadData()
                }
                if selectedBTN == 1{
                    self.nodataLBL.isHidden = self.providerlistpg?.count != 0
//                    self.hideActivityIndicator()
                    
                    //                    self.contactsTBL.isScrollEnabled = true
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        }
    }
    //MARK: - TIP CLOSE
    func tipcloseAPI(keyvalue:String = ""){
        
        let params: [String : Any] = [
            "userid": userid,
            "key": keyvalue,
            "value": true
        ]
        
        let url = kBaseUrl+"changeSettings"
        let request =  AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
            print(response)
            switch (response.result){
            case .success(let JSON):
                if let responsedata = JSON as? [String:Any] {
                    print(responsedata)
                    if keyvalue == "tooltip.contactswipe"{
                        UserDefaults.standard.set("true", forKey: "generalTipStatus")
                        //self.generaltipstatus = "true"
                    }else{
                        UserDefaults.standard.set("true", forKey: "providerTipStatus")
                        //self.providertipstatus = "true"
                    }
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    //MARK: - TAG API FUNCTION
    func taggeneralapi(contactidfromlist : String){
        //        showActivityIndicator()
        print("contactidfromlist = \(contactidfromlist)")
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        
        let params: [String : Any] = [
            "contactid": contactidfromlist]
        
        let url = kBaseUrl+"markAsGeneralContact"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata :",responsedata)
                    //                        hideActivityIndicator()
                    var alert = UIAlertController(title: "", message: "\(responsedata["data"] ?? "")", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                        if responsedata["data"] as? String ?? "" == "Updated successfully"{
                            selectedBTN = 0
                            self.tabselector()
                            self.contactsTBL.reloadData()
                        }else{
                            alert = UIAlertController(title: "", message: "\(responsedata["data"] ?? "")", preferredStyle: .alert)
                            
                        }
                        
                    }))
                    self.present(alert, animated: true)
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        }
        
    }
    
    //MARK: - change provider contact to General contact
    //    changeToGeneralContact
    func changeproviderapi(contactidfromlist : String){
        //        showActivityIndicator()
        print("contactidfromlist = \(contactidfromlist)")
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        
        let params: [String : Any] = [
            "contactid": contactidfromlist]
        AF.request(kBaseUrl+"changeToGeneralContact", method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata :",responsedata)
                    //                        hideActivityIndicator()
                    let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                        if responsedata["message"] as? String ?? "" == "Changed to general contact successfully"{
                            selectedBTN = 0
                            self.tabselector()
                            self.contactsTBL.reloadData()
                        }else{
                            let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                            
                        }
                        
                    }
                                                  
                                                 ))
                    self.present(alert, animated: true)
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        }
        
    }
}



@available(iOS 14.0, *)
extension ContactsmainVC:UITextFieldDelegate{
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        searchtexter = textField.text ?? ""
        if selectedBTN == 0 {
            searchtexter = textField.text ?? ""
        }
        else{
//            if searchtexter.count > 0{
//                var filteredContacts: [ProviderModelC] = []
//                for providerCategory in self.providerlist?.data ?? [] {
//                    let filteredContactsForCategory = providerCategory.contacts.filter { contact in
//                        if let contactName = contact.name {
//                            return contactName.localizedCaseInsensitiveContains(searchtexter)
//                        }
//                        return false
//                    }
//                    filteredContacts.append(contentsOf: filteredContactsForCategory)
//                }
//                if filteredContacts.count > 0{
//                    var updatedProviderSingleContactResponse = ProviderSingleContactListResponse()
//                                        updatedProviderSingleContactResponse.status = self.providerlist?.status
//                                        updatedProviderSingleContactResponse.reviewCount = self.providerlist?.reviewCount
//                                        var filteredCategories = [ProviderCategoryListModel]()
//                                        for providerCategory in self.providerlist?.data ?? [] {
//                                            let filteredContactsForCategory = providerCategory.contacts.filter { contact in
//                                                return filteredContacts.contains { filteredContact in
//                                                    return contact._id == filteredContact._id
//                                                }
//                                            }
//
//                                            if !filteredContactsForCategory.isEmpty {
//                                                var filteredCategory = providerCategory
//                                                filteredCategory.contacts = filteredContactsForCategory
//                                                filteredCategories.append(filteredCategory)
//                                            }
//                                        }
//                                        updatedProviderSingleContactResponse.data = filteredCategories
//
//                                        self.providerlist?.data = updatedProviderSingleContactResponse.data
//                                        self.contactsTBL.reloadData()
//                }
//            }else {
//                fetchAllProviderData()
//            }
           
        }
    }
}


//MARK: - EXtension (RecommendedCollectionview)
@available(iOS 14.0, *)
extension contactCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == recommendedCV{
            return headcount
        }else{
          //  if tagcvcount <= 4{
                return tagheadcount
//            }else{
//                return 4
//            }
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == recommendedCV{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recommendedcollectioncell", for: indexPath) as! recommendedcollectioncell
            //            cell.recomIMG.kf.setImage(with: URL(string: kImageUrl + (self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].recomended.first?.user.first?.profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tagrecommendedcvcell", for: indexPath) as! tagrecommendedcvcell
            
            DispatchQueue.main.async {
            cell.tagrecomIMG.kf.setImage(with: URL(string: kImageUrl + (tagimgselect)),placeholder: UIImage(named: "profileimgIfnodata"))
               // cell.tagrecomIMG.image = UIImage(named: "image 8")
                
            }
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let w:CGFloat = 25
            let h:CGFloat = 25
            return CGSize(width: w, height: h)
    }
    
}


@available(iOS 14.0, *)
extension ContactsmainVC{
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func showActivityIndicator() {
        activityView?.startAnimating()
    }
    
    func hideActivityIndicator(){
        activityView?.stopAnimating()
    }
}


//MARK: - SYNC CONTACTS CODE
extension ContactsmainVC{
    func getContactList(){
        self.contacDictionary.removeAll()
        let predicate = CNContact.predicateForContactsInContainer(withIdentifier:store.defaultContainerIdentifier())
        let contactz = try! store.unifiedContacts(matching: predicate, keysToFetch: [CNContactGivenNameKey as
                                                                                     CNKeyDescriptor,CNContactFamilyNameKey as CNKeyDescriptor ,CNContactPhoneNumbersKey as CNKeyDescriptor])
        contactsCount = contactz.count
        
        for contact in contactz {
            let name = "\(contact.givenName) \(contact.familyName)"
            var numbers: [String] = []
            for ph in contact.phoneNumbers{
                numbers.append(ph.value.stringValue)
            }
            let dictionaryObject: [String : Any] = [ "name": name, "phoneNumber": numbers]
            self.contacDictionary.append(dictionaryObject)
        }
        // print("before sorting",contacDictionary)
        let sortedArray = contacDictionary.sorted { ($0["name"] as! String) < ($1["name"] as! String) }
        //print("after sorting",sortedArray)
        //        DispatchQueue.main.async {
        //            self.viewOfCongrats.isHidden = false
        //        }
        self.tempArr = sortedArray
        //        DispatchQueue.global(qos: .background).async {
        self.syncAPIParameter()
        //            DispatchQueue.main.async {
        //                // Update the UI here
        //            }
        //        }
        
        //        DispatchQueue.main.async {
        //            print("Total of ",contactz.count," contacts found in your Phone list")
        //            self.messageLbl.text = "Please wait while we finish syncing \(contactz.count) contacts from your phone book"
        //        }
    }
    
    
    func syncAPIParameter() {
        self.uploadIsFaild = 3
        self.contactParam.removeAll()
        
        if self.tempArr.isEmpty { return }
        
        if self.tempArr.count <= self.uploadCount {
            self.contactParam = self.tempArr
            self.tempArr.removeAll()
            //  SVProgressHUD.dismiss()
            
            //                    DispatchQueue.main.async {
            //                        print("Total of ",self.contacDictionary.count,"has been synced")
            //                        self.messageLbl.text = "We have synced \(self.contacDictionary.count) contacts from your phone book"
            //                    }
        } else {
            self.contactParam = Array(self.tempArr.prefix(self.uploadCount))
            self.tempArr = Array(self.tempArr.dropFirst(self.uploadCount))
        }
        self.syncAPI(contacDictionary: self.contactParam)
    }
    
    //MARK: - Api call
    func syncAPI(contacDictionary: [[String:Any]] ){
        
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
        let countrycode : String = UserDefaults.standard.value(forKey: "Kcountrycode") as! String
        let params: [String : Any] = [
            "userId": userid,
            "countryCode": countrycode,
            "syncdata": contacDictionary
        ]
        print("params",params)
        //        print("contactParam:",contactParam)
        let url = kBaseUrl+"syncContact"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            //             print("params:\(params)")
            //             print("response:\(response)")
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    syncContact = true
                    // print("syncContact :",syncContact)
                    DispatchQueue.main.async {
                        self.syncAPIParameter()
                    }
                    
                    
                    print("responsedata :",responsedata)
                    if responsedata["message"] as? String == "Contact sync successfully"{
                        
                    }
                    else{
                        //  hideActivityIndicator()
                        //                        showDefaultAlert(viewController: self, msg: responsedata["message"] as? String ?? "")
                    }
                }
            case .failure(let error):
                if self.uploadIsFaild > 0 {
                    self.syncAPI(contacDictionary: self.contactParam)
                }
                self.uploadIsFaild -= 1
                showDefaultAlert(viewController: self, msg: "Request error: \(error.localizedDescription)")
                print("Request error: \(error.localizedDescription)")
            }
        }
    }
    
    @available(iOS 14.0, *)
    @objc func autopush(){
        timer.invalidate()
    }
    
    //MARK: - contact authorization
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            self.showSettingsAlert(completionHandler)
        case .restricted, .notDetermined:
            self.store.requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                } else {
                    DispatchQueue.main.async {
                        self.showSettingsAlert(completionHandler)
                    }
                }
            }
        }
    }
    //MARK: - private alert for granting access again from settings
    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)
        if
            let settings = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(settings) {
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
                completionHandler(false)
                UIApplication.shared.open(settings)
            })
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            completionHandler(false)
        })
        present(alert, animated: true)
    }
    
    
    //    func showActivityIndicator() {
    //        activityView = UIActivityIndicatorView(style: .large)
    //        activityView?.center = self.view.center
    //        self.view.addSubview(activityView!)
    //        activityView?.startAnimating()
    //    }
    //
    //    func hideActivityIndicator(){
    //        if (activityView != nil){
    //            activityView?.stopAnimating()
    //        }
    //    }
    
}


//MARK: - contacts cell
class contactCell:UITableViewCell{
    //general list outlets and connections
    var invitenumber = ""
    
    @IBOutlet weak var generalView: BaseView!
    @IBOutlet weak var generalViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameLBL:UILabel!
    @IBOutlet weak var profileIMG:UIImageView!
    @IBOutlet weak var generalthumbIMG: UIImageView!
    @IBOutlet weak var generalthumbcount:UILabel!
    @IBOutlet weak var categorynameLBL:UILabel!
    @IBOutlet weak var statusnameLBL:UILabel!
    @IBOutlet weak var statusView:UIView!
    @IBOutlet weak var dotBTN:UIButton!
    @IBOutlet weak var dotimg:UIImageView!
    @IBOutlet weak var statusBTNtapped:UIButton!
    //provider view outlets and connnections
    @IBOutlet weak var ProviderView: UIView!
    @IBOutlet weak var providerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var providerProfileIMG: UIImageView!
    @IBOutlet weak var providerthumbIMG: UIImageView!
    @IBOutlet weak var providerneedsreviewIMG: UIImageView!
    @IBOutlet weak var providerNameLbl: UILabel!
    @IBOutlet weak var proLocationLbl: UILabel!
    @IBOutlet weak var proDotsIMG: UIImageView!
    @IBOutlet weak var prosignupIMG: UIImageView!
    @IBOutlet weak var providerDotsBTN: UIButton!
    
    @IBOutlet weak var recommendedCV: UICollectionView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var recommendedCVwidth: NSLayoutConstraint!
    
    @IBOutlet weak var tagRecommendedView: UIView!
    @IBOutlet weak var tagRecommendCV: UICollectionView!
    @IBOutlet weak var tagrecommendedCVwidth: NSLayoutConstraint!
    @IBOutlet weak var tagRecomCount: UILabel!
    
    @IBOutlet weak var recomStackview: UIStackView!
    @IBOutlet weak var recomstackIMG1: UIImageView!
    @IBOutlet weak var recomTagView1: UIView!
    @IBOutlet weak var recomstackIMG2: UIImageView!
    @IBOutlet weak var recomTagView2: UIView!
    @IBOutlet weak var recomstackIMG3: UIImageView!
    @IBOutlet weak var recomTagView3: UIView!
    @IBOutlet weak var recomstackIMG4: UIImageView!
    @IBOutlet weak var recomTagView4: UIView!
    @IBOutlet weak var recomstackLbl: UILabel!
    
    @IBOutlet weak var prorecomStackview: UIStackView!
    @IBOutlet weak var prorecomstackIMG1: UIImageView!
    @IBOutlet weak var prorecomTagView1: UIView!
    @IBOutlet weak var prorecomstackIMG2: UIImageView!
    @IBOutlet weak var prorecomTagView2: UIView!
    @IBOutlet weak var prorecomstackIMG3: UIImageView!
    @IBOutlet weak var prorecomTagView3: UIView!
    @IBOutlet weak var prorecomstackIMG4: UIImageView!
    @IBOutlet weak var prorecomTagView4: UIView!
    @IBOutlet weak var prorecomstackLbl: UILabel!
    
    
    var chunkedlist : [getcontactDataListModelcodable] = []
    var taggedContacts : [SingleContactListData]?
    var Mobnumber = ""
    var CCode = ""
    var CId = ""
    var cardheight : Bool = false
    
    func invitesetup(){
        self.statusView.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.737254902, blue: 0.01960784314, alpha: 1)
        self.statusnameLBL.text = "Invited"
    }
    
    @IBAction func inviteBTNtapped(_ sender: UIButton) {
        
        print("indexpath :",sender.tag)
        print("mobnumber :",Mobnumber)
        print("countrycode :",CCode)
        print("contactid :",CId)
        
        self.inviteapi(number: CCode.appending(Mobnumber))
        // self.inviteapi()
    }
    
    func inviteapi(number : String){
        //  func inviteapi(){
        
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        //  let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
        
        
        let params: [String : Any] = ["id": CId]
        print("params",params)
        let url = kBaseUrl+"inviteContact"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            //             print("params:\(params)")
            //             print("response:\(response)")
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    
                    print("responsedata :",responsedata)
                    if responsedata["data"] as? String == "Updated successfully"{
                        
                        self.invitesetup()
                        self.invitetoShow(number: number)
                        
                    }
                    else{
                        //  hideActivityIndicator()
                        
                    }
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        }
    }
    
    
    func invitetoShow(number:String){
        print(number)
        print("invite normal list")
        let sms = "sms:"+number+"&body="+invitelink
        // let sms = "sms:+919567155224&body="+invitelink
        
        let strURL = sms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        UIApplication.shared.open(URL(string: strURL)!, options: [:], completionHandler: nil)
        
    }
}

class headercell:UITableViewCell{
    @IBOutlet weak var nameLBL:UILabel!
    @IBOutlet weak var dropIMG:UIImageView!
    @IBOutlet weak var dropBTN:UIButton!
}

//Provider
class recommendedcollectioncell:UICollectionViewCell{
    @IBOutlet weak var recomIMG: UIImageView!
    @IBOutlet weak var viewIMGBg: BaseView!
    override func awakeFromNib() {
        recomIMG.layer.cornerRadius = recomIMG.frame.width / 2
        recomIMG.clipsToBounds = true
    }
}

//Tagged by other users as Provider
class tagrecommendedcvcell:UICollectionViewCell{
    @IBOutlet weak var tagrecomIMG: UIImageView!
    @IBOutlet weak var tagviewIMGBg: BaseView!
    
    var tagrec = 0
    
    override func awakeFromNib() {
        tagrecomIMG.layer.cornerRadius = tagrecomIMG.frame.height / 2
        tagrecomIMG.clipsToBounds = true
    }
}


extension UIColor {
    
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat
        
        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}
extension Encodable {
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}
