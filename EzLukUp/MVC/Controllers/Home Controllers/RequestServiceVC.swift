//
//  RequestServiceVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 09/06/23.
//

import UIKit

class RequestServiceVC: UIViewController {

    @IBOutlet weak var ContinueBTN: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ContinueBTN.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
    }
    
    @IBAction func ContinueBTN(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
