//
//  ProMyProfileServiceVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 05/04/23.
//

import UIKit
import Alamofire
import SwiftUI
import Kingfisher

class ProMyProfileServiceVC: UIViewController {

    @IBOutlet weak var servicestableview: UITableView!
    @IBOutlet weak var noDataAbout: BaseView!
    @IBOutlet weak var noDataService: BaseView!
    
    var userdetails : getProviderUserDetailsResponse?
    var Aboutbusiness = ""
    var servicesArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getProviderProfileDetails()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getProviderProfileDetails()
    }
  
}

//MARK: -UITableViewDelegates
extension ProMyProfileServiceVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return servicesArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = servicestableview.dequeueReusableCell(withIdentifier: "EditServicesTableCell", for: indexPath) as! EditServicesTableCell
            print(Aboutbusiness)
            cell.cellLbl1.text = Aboutbusiness
            return cell
        }else{
            let cell = servicestableview.dequeueReusableCell(withIdentifier: "ProviderServicesListTVCell", for: indexPath) as! ProviderServicesListTVCell
            cell.cellLbl.text = servicesArray[indexPath.row]
           
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK: - TableCell
class EditServicesTableCell: UITableViewCell {
    
    @IBOutlet weak var cellLbl1: UILabel!
   
}

class ProviderServicesListTVCell:UITableViewCell{
    @IBOutlet weak var cellLbl: UILabel!
    
}


extension ProMyProfileServiceVC{
    //MARK: - User details API Call
        func getProviderProfileDetails(){
              
            let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
            let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
            
            let params = ["userId": userid] as [String : Any]
            
           // let url = "http://13.234.177.61/api7/getUserDetails"
            let url = kBaseUrl+"getUserDetails"
            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
                    switch (response.result) {
                    case .success( let JSON):
                        
                        if let responsedata =  JSON as? [String:Any]  {
                            //print("responsedata",responsedata)
                            self.userdetails = getProviderUserDetailsResponse(from:responsedata)
//                            self.Aboutbusiness = self.userdetails?.Data?.aboutBusiness ?? ""
//                            self.servicesArray = self.userdetails?.Data?.serviceDetails as! [String]
                        }
                        DispatchQueue.main.async {
                            
                            
                            if self.userdetails?.Data?.aboutBusiness == ""{
                                self.noDataAbout.isHidden = false
                            }else{
                                self.Aboutbusiness = self.userdetails?.Data?.aboutBusiness ?? ""
                                self.noDataAbout.isHidden = true
                            }
                            
                            if self.userdetails?.Data?.serviceDetails == []{
                                self.noDataService.isHidden = false
                            }else{
                                self.servicesArray = self.userdetails?.Data?.serviceDetails as! [String]
                                self.noDataService.isHidden = true
                            }
//                            if self.servicesArray.count == 0{
//                                self.lblNoServices.isHidden = false
//                            }else{
//                                self.lblNoServices.isHidden = true
//                            }
                            
                            self.servicestableview.reloadData()
                        }
                        
                    case .failure(let error):
                        print("Request error: \(error.localizedDescription)")
                    }
                
                
            }
        }
}
