//
//  MainConsumerProfileVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 27/03/23.
//

import UIKit
import Alamofire
import IPImage

class MainConsumerProfileVC: UIViewController {

 //MARK: - Outlets
    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet weak var profileIMG: BaseImageView!
    @IBOutlet weak var nameLabel: UILabel!
   
    
    var userdetails : getUserDetailsResponse?
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userdeatilsapi()
    }
    
    
    @IBAction func editBTNTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ConsumerProfile") as! ConsumerProfile
        vc.getlocality = self.userdetails?.Data?.city ?? ""
        vc.getname = self.self.userdetails?.Data?.fullName ?? ""
        vc.getcountry = self.self.userdetails?.Data?.countryCode ?? ""
        vc.getphonenumber = self.self.userdetails?.Data?.phoneNumber ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
 
}

//MARK: - INFO UITableViewDelegate,UITableViewDataSource
extension MainConsumerProfileVC: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = infoTableView.dequeueReusableCell(withIdentifier: "ConsumerInfoCell", for: indexPath) as! ConsumerInfoCell
        cell.cell1.text = self.userdetails?.Data?.formattedPhoneNumber ?? ""
        cell.cell2.text = self.userdetails?.Data?.city ?? ""
        return cell
    }

}

extension MainConsumerProfileVC{
//MARK: - API Call
func userdeatilsapi(){
      
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
    
    let params = ["userId": userid] as [String : Any]
    
   // let url = "http://13.234.177.61/api7/getUserDetails"
    let url = kBaseUrl+"getUserDetails"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in

            switch (response.result) {
            case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata",responsedata)
                    self.userdetails = getUserDetailsResponse(from:responsedata)
                    
                    self.nameLabel.text = self.userdetails?.Data?.fullName ?? ""
                        let ipimage = IPImage(text: self.nameLabel.text ?? "", radius: 30, font: UIFont(name: "Jost-Regular", size: 25), textColor: nil, randomBackgroundColor: false)
                    
                        self.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.userdetails?.Data?.profilePic ?? "")),placeholder: ipimage.generateImage())
                    
                    self.infoTableView.reloadData()
                   // print("headername",self.getservivedetails?.Data?.serviceDetails[0].title)
                         
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        
        
    }
}

}

class ConsumerInfoCell: UITableViewCell{
    @IBOutlet weak var cell1: UILabel!
    @IBOutlet weak var cell2: UILabel!
}
