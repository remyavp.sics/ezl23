//
//  DashProvProfileInfoVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 02/07/23.
//

import UIKit
import Alamofire

class DashProvProfileInfoVC: UIViewController {

    @IBOutlet weak var infotableview: UITableView!
    var userdetails : getProviderUserDetailsResponse?
    var phonenumber = ""
    var areaofservices = ""
    var housefoperation = ""
    var getinfoid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getuserdetails()
      
    }
 
}


//MARK: - INFO UITableViewDelegate,UITableViewDataSource
extension DashProvProfileInfoVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = infotableview.dequeueReusableCell(withIdentifier: "DashProInfoTVCell", for: indexPath) as! DashProInfoTVCell
        cell.cellLbl2.text = phonenumber
        cell.areaofservices.text = areaofservices
        cell.houseofOperation.text = housefoperation
        if areaofservices == ""{
            cell.viewAreaofService.isHidden = true
        }else{
            cell.viewAreaofService.isHidden = false
        }
        if housefoperation == ""{
            cell.viewHouseofOperation.isHidden = true
        }else{
            cell.viewHouseofOperation.isHidden = false
        }
        if phonenumber == ""{
            cell.viewPhoneNumber.isHidden = true
        }else{
            cell.viewPhoneNumber.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK: - TableViewCell
class DashProInfoTVCell: UITableViewCell {
    
    @IBOutlet weak var cellLbl2: UILabel!
    @IBOutlet weak var areaofservices : UILabel!
    @IBOutlet weak var houseofOperation : UILabel!
    @IBOutlet weak var viewAreaofService : UIView!
    @IBOutlet weak var viewPhoneNumber:UIView!
    @IBOutlet weak var viewHouseofOperation:UIView!
    
}


extension DashProvProfileInfoVC{
    //MARK: - User details API Call
        func getuserdetails(){
              
            let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
            let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
            let providerid : String = UserDefaults.standard.value(forKey: "KproviderId") as! String
            
            let params = ["userId": providerid] as [String : Any]
            
          //  let url = "http://13.234.177.61/api7/getUserDetails"
            let url = kBaseUrl+"getUserDetails"
            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
                    print(response)
                    switch (response.result) {
                    case .success( let JSON):
                        
                        if let responsedata =  JSON as? [String:Any]  {
                            print("responsedata",responsedata)
                            self.userdetails = getProviderUserDetailsResponse(from:responsedata)
                            self.phonenumber = self.userdetails?.Data?.phoneNumber ?? ""
                            let cityname = self.userdetails?.Data?.areaOfService.first?.city ?? ""
                            let countryname = self.userdetails?.Data?.areaOfService.first?.country ?? ""
                            
                            if cityname != "" && countryname != ""{
                                self.areaofservices = "\(cityname) , \(countryname)"
                            }else{
                                if cityname == ""{
                                    self.areaofservices = countryname
                                }else{
                                    self.areaofservices = cityname
                                }
                            }
                            
                            
                          //  self.areaofservices = "\(cityname),\(countryname)"
                            print(self.userdetails?.Data?.operationalHours ?? "")
                            self.housefoperation = self.userdetails?.Data?.operationalHours ?? ""
                            
                        }
                        DispatchQueue.main.async {
                            self.infotableview.reloadData()
                        }
                        
                    case .failure(let error):
                        print("Request error: \(error.localizedDescription)")
                    }
                
                
            }
        }
}
