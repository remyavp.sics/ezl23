//
//  DashProvProfileServiceVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 02/07/23.
//

import UIKit
import Alamofire
import SwiftUI
import Kingfisher

class DashProvProfileServiceVC: UIViewController {

    @IBOutlet weak var servicestableview: UITableView!
    @IBOutlet weak var noDataAbout: BaseView!
    @IBOutlet weak var noDataService: BaseView!
    
    var userdetails : getProviderUserDetailsResponse?
    var aboutbusiness = ""
    var servicesArray = [String]()
    var getserviceid = ""
   // let providerid : String = UserDefaults.standard.value(forKey: "KproviderId") as! String
    
    override func viewDidLoad() {
        super.viewDidLoad()

       getuserservicedetails()
    }
    

}

//MARK: -UITableViewDelegates
extension DashProvProfileServiceVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return servicesArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = servicestableview.dequeueReusableCell(withIdentifier: "DashProAboutTVCell", for: indexPath) as! DashProAboutTVCell
            print(aboutbusiness)
            cell.cellLbl1.text = aboutbusiness
            return cell
        }else{
            let cell = servicestableview.dequeueReusableCell(withIdentifier: "DashProServiceTVCell", for: indexPath) as! DashProServiceTVCell
            cell.cellLbl.text = servicesArray[indexPath.row]
           
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK: - TableCell
class DashProAboutTVCell: UITableViewCell {
    @IBOutlet weak var cellLbl1: UILabel!
   
}

class DashProServiceTVCell:UITableViewCell{
    @IBOutlet weak var cellLbl: UILabel!
    
}


extension DashProvProfileServiceVC{
    //MARK: - User details API Call
        func getuserservicedetails(){
              
            let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
            let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
            let providerid : String = UserDefaults.standard.value(forKey: "KproviderId") as! String
            
            let params = ["userId": providerid] as [String : Any]
            
           // let url = "http://13.234.177.61/api7/getUserDetails"
            let url = kBaseUrl+"getUserDetails"
            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
                    switch (response.result) {
                    case .success( let JSON):
                        
                        if let responsedata =  JSON as? [String:Any]  {
                            //print("responsedata",responsedata)
                            self.userdetails = getProviderUserDetailsResponse(from:responsedata)
//                            self.Aboutbusiness = self.userdetails?.Data?.aboutBusiness ?? ""
//                            self.servicesArray = self.userdetails?.Data?.serviceDetails as! [String]
                        }
                        DispatchQueue.main.async {
                            
                            
                            if self.userdetails?.Data?.aboutBusiness == ""{
                                self.noDataAbout.isHidden = false
                            }else{
                                self.aboutbusiness = self.userdetails?.Data?.aboutBusiness ?? ""
                                self.noDataAbout.isHidden = true
                            }
                            
                            if self.userdetails?.Data?.serviceDetails == []{
                                self.noDataService.isHidden = false
                            }else{
                                self.servicesArray = self.userdetails?.Data?.serviceDetails as! [String]
                                self.noDataService.isHidden = true
                            }
//                            if self.servicesArray.count == 0{
//                                self.lblNoServices.isHidden = false
//                            }else{
//                                self.lblNoServices.isHidden = true
//                            }
                            
                            self.servicestableview.reloadData()
                        }
                        
                    case .failure(let error):
                        print("Request error: \(error.localizedDescription)")
                    }
                
                
            }
        }
}

