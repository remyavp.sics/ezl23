//
//  ConsumerProfile.swift
//  EzLukUp
//
//  Created by REMYA V P on 20/03/23.
//

import UIKit
import Alamofire
import CoreLocation
//import CountryPickerView
import Kingfisher
import PhoneNumberKit


class ConsumerProfile: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate {

//MARK: -Outlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var profileIMG: BaseImageView!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var locationTF: UITextField!
    @IBOutlet weak var locationTV: UITableView!
    @IBOutlet weak var locationViewHeight: NSLayoutConstraint!
    @IBOutlet weak var saveBTN: BaseButton!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var flagview: UIView!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var countryLbl: UILabel!
    //@IBOutlet weak var countrypickerview: CountryPickerView!

//MARK: -View Outlets
    
    @IBOutlet weak var otpMainView: UIView!
    @IBOutlet weak var numberTxt1: BaseTextField!
    @IBOutlet weak var numberTxt2: UITextField!
    @IBOutlet weak var numberTxt3: UITextField!
    @IBOutlet weak var numberTxt4: UITextField!
    @IBOutlet weak var receivecodeBTN: UIButton!
    @IBOutlet weak var otpPopupview: UIView!
    
    
//VARIABLES
    var userdetails : getUserDetailsResponse?
    let appDelegateInstance = UIApplication.shared.delegate as! AppDelegate
    var pickedimage :UIImage?
    var imagePicker = UIImagePickerController()
    var selectedProfileImage : UIImage!
    var imageData = Data()
    var imageStr = ""
    var fileName = ""
    var mimetype = ""
    var uploadedImageStr = ""
    var imageupload = ""
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
   // var userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
    let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
    
    var code = ""
    var activityView: UIActivityIndicatorView?
    var autocompleteresults :[GApiResponse.Autocomplete] = []
    var kstatus = 0
    var isLocationHasValue = false
   // let locationManager = CLLocationManager()
    var getlocality = ""
    var getcountry = ""
    var getlat = ""
    var getlong = ""
    
    var getname = ""
    var getphonenumber = ""
    var getimage = ""
    var lat :Double = 0
    var long :Double = 0
    
 //OTP view variables
    var getphNo = String()
    var getcode = String()
    var fromdisabled : Bool = false
    var message = String()
    var getMobileNumber = String()
    var otpinfo =  Dictionary<String, AnyObject>()
    var otpVerifyinfo =  Dictionary<String, AnyObject>()
    var countTimer:Timer!
    var counter = 30
    var OTP = String()
    
    var normalphonenum = ""
    var normalname = ""
    var normallocation = ""
    var editedphonenum = ""
    var editedname = ""
    var editedlocation = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        userdeatilsapi()
       // otpviewSetup()
        locationTF.delegate = self
        NotificationCenter.default.addObserver(self,selector:#selector(keyboardWillShow),name:UIResponder.keyboardWillShowNotification, object: nil)
              
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                kstatus = Int(keyboardSize.height)
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.mainView.frame.origin.y != 0 {
            self.mainView.frame.origin.y = 0
        }
    }
    
    
    func setupUI(){
        self.flagview.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
        self.flagview.layer.borderWidth = 1
        self.numberView.layer.borderWidth = 1
        self.numberView.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
        self.saveBTN.addCornerForView(cornerRadius: 14.0)
        self.flagview.addCornerForView(cornerRadius: 10.0)
        self.numberView.addCornerForView(cornerRadius: 10.0)
        mobileTF.delegate = self
        
        saveBTN.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
      //  saveBTN.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
       // self.saveBTN.isEnabled = false
        self.imagePicker.allowsEditing = true
        print("image ==== ",pickedimage as Any)
        self.otpMainView.isHidden = true
        self.otpPopupview.isHidden = true
    }
    
    func otpviewSetup(){
        initializeHideKeyboard()
        print(getMobileNumber)

        self.navigationController?.isNavigationBarHidden = true
        self.intialSetup(numberTxt1: numberTxt1, numberTxt2: numberTxt2, numberTxt3: numberTxt3, numberTxt4: numberTxt4)
        otpinfo["mobile"] = getMobileNumber as AnyObject
        otpinfo["user_type"] = "APP_USER" as AnyObject
        otpTimerStart()
        textBorderSetup()
//        self.otpMainView.isHidden = true
//        self.otpPopupview.isHidden = true
    }
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        mobileTF.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
      //  changeotpapi()
        mobileTF.resignFirstResponder()
    }
   
    
    //MARK: - Delegates & Functions
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == mobileTF
        {
            mobileTF.placeholder = "(123) 456-7890"
            editedphonenum = self.mobileTF.text ?? ""
        }
        
      else if textField == locationTF{
            let point = CGPoint(x: 0, y: (self.view.frame.height) / 4) // 200 or any value you like.
            self.scrollView.contentOffset = point
            editedlocation = self.locationTF.text ?? ""
        }
        else if textField == nameTF{
            editedname = self.nameTF.text ?? ""
        }
        
//        else{
//            let startPosition: UITextPosition = textField.beginningOfDocument
//            let endPosition: UITextPosition = textField.endOfDocument
//            let selectedRange: UITextRange? = textField.selectedTextRange
//            if let selectedRange = textField.selectedTextRange {
//
//                let cursorPosition = numberTxt1.offset(from: textField.beginningOfDocument, to: selectedRange.start)
//
//                print("\(cursorPosition)")
//            }
//            let newPosition = textField.beginningOfDocument
//            textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
//        }
        
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == mobileTF{
            editedphonenum = self.mobileTF.text ?? ""
        }else if textField == locationTF{
            editedlocation = self.locationTF.text ?? ""
        }
        else{
            editedname = self.nameTF.text ?? ""
        }
//        else{
//            self.saveBTN.isEnabled = false
//            saveBTN.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
//        }
        
//        if nameTF.text != editedname || locationTF.text != editedlocation || mobileTF.text != editedphonenum{
////            self.saveBTN.isEnabled = true
////            saveBTN.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
//        }else
//        if mobileTF.text != editedphonenum && nameTF.text == editedname && locationTF.text == editedlocation{
//            self.saveBTN.isEnabled = true
//            saveBTN.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
//        }else if mobileTF.text == editedphonenum && nameTF.text == editedname && locationTF.text != editedlocation{
//            self.saveBTN.isEnabled = true
//            saveBTN.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
//        }else if mobileTF.text == editedphonenum && nameTF.text != editedname && locationTF.text == editedlocation{
//            self.saveBTN.isEnabled = true
//            saveBTN.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
//        }
//
//        else{
//            self.saveBTN.isEnabled = false
//            saveBTN.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
//        }
        

        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        editedphonenum = self.mobileTF.text ?? ""
        editedlocation = self.locationTF.text ?? ""
        editedname = self.nameTF.text ?? ""
        return true
    }
    
    func showResults(string:String){
      var input = GInput()
        input.keyword = string
        GoogleApi.shared.callApi(input: input) { (response) in
            if response.isValidFor(.autocomplete) {
                DispatchQueue.main.async {
                 
                    self.autocompleteresults = response.data as! [GApiResponse.Autocomplete]
                   
                    self.locationViewHeight.constant = CGFloat(self.autocompleteresults.count * 30)
                    print("loc table count",self.autocompleteresults.count)
                    
                    self.locationTV.reloadData()
                    
//                    if self.kstatus == 0  {
                    print("Mview origin is - ",self.mainView.frame.origin.y)
                        
//                    }

                }
            } else { print(response.error ?? "ERROR") }
        }
    }
     
    func hideresults(){
        locationViewHeight.constant = 0
        autocompleteresults.removeAll()
        locationTV.reloadData()
        print("mainview origin is - ",self.mainView.frame.origin.y)
    }
    
    @IBAction func closePopupBTN(_ sender: UIButton) {
        self.otpPopupview.isHidden = true
        self.otpMainView.isHidden = true
        self.mobileTF.text = editedphonenum
    }
    
    
}


//MARK: - @IBActions & functions
extension ConsumerProfile{
    
      @IBAction func closeBTNTapped(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
      }
     
    //MARK: - profile image picker =======================
      @IBAction func EditprofileBTN(_ sender: UIButton) {
          let imagePickerController = UIImagePickerController()
          imagePickerController.allowsEditing = true //If you want edit option set "true"
          imagePickerController.sourceType = .photoLibrary
          imagePickerController.delegate = self
          present(imagePickerController, animated: true, completion: nil)
      }
      
    //MARK: image picker delegate method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[.editedImage] as! UIImage

        self.selectedProfileImage = tempImage
        pickedimage  = tempImage
        profileIMG.image = pickedimage
        print("image ==== ",pickedimage as Any)

        picker.dismiss(animated: true) {
            self.dismiss(animated: false, completion: nil)
        }
        
        if pickedimage != nil{
            uploadImage()
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    //mime types
      func mimeType(for data: Data) -> String {
          
          var b: UInt8 = 0
          data.copyBytes(to: &b, count: 1)

          switch b {
          case 0xFF:
              return "i.jpeg"
          case 0x89:
              return ".png"
          case 0x47:
              return ".gif"
          case 0x4D, 0x49:
              return ".tiff"
          case 0x25:
              return "application/pdf"
          case 0xD0:
              return "application/vnd"
          case 0x46:
              return "text/plain"
          default:
              return "application/octet-stream"
          }
      }
    
    //MARK: - PROFILE UPLOAD API CALL
         func uploadImage(){
             if selectedProfileImage != nil{
                 //showActivityIndicator()
                 let uiImage : UIImage = self.selectedProfileImage
                 imageData = uiImage.jpegData(compressionQuality: 0.2)!
                 
                 imageStr = imageData.base64EncodedString()
                 fileName = "image"
                 mimetype = mimeType(for: imageData)
                 print("mime type = \(mimetype)")
             }else{
                 imageData = Data()
                 fileName = ""
             }
           
             ServiceManager.sharedInstance.uploadSingleDataa("saveProfileImage", headerf: token, parameters: ["userId":userid], image: imageData, filename: fileName, mimetype: mimetype, withHud: false) { (success, response, error) in
                 print(response as Any)
                 if success {
                     if response!["status"] as! Bool == true {
                        // self.hideActivityIndicator()
                         print("image upload success")
                         
                     }else {
                         showDefaultAlert(viewController: self, title: "", msg: "image upload unsuccessfull")
                     }
                 }else{
                     print(error?.localizedDescription as Any)
                     showDefaultAlert(viewController: self, title: "", msg: error?.localizedDescription ?? "API Failed...!")
                 }
             }
         }
    
    
      @IBAction func saveBTNTapped(_ sender: UIButton) {
          print(normalphonenum)
          print(self.mobileTF.text ?? "")

          if normalphonenum == self.mobileTF.text ?? "" {
              saveUserDetailsapi()
          }else{
              changeotpapi()
          }
     }
}

//MARK: - OTP View Actions & Functions
extension ConsumerProfile{

    //MArK:- Otp screen design
      func textBorderSetup(){
          numberTxt1.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
          numberTxt1.layer.borderWidth = 1
          numberTxt1.addCornerForView(cornerRadius: 14.0)
          numberTxt2.layer.borderWidth = 1
          numberTxt2.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
          numberTxt2.addCornerForView(cornerRadius: 14.0)
          numberTxt3.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
          numberTxt3.layer.borderWidth = 1
          numberTxt3.addCornerForView(cornerRadius: 14.0)
          numberTxt4.layer.borderWidth = 1
          numberTxt4.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
          numberTxt4.addCornerForView(cornerRadius: 14.0)

      }

    func otpTimerStart(){
        counter = 30
        guard countTimer == nil else { return }
        self.countTimer = Timer.scheduledTimer(timeInterval: 1 ,
        target: self,
        selector: #selector(self.changeTitle),
        userInfo: nil,
        repeats: true)
    }

    func stopTimer() {

        guard countTimer != nil else { return }
        countTimer?.invalidate()
        countTimer = nil
    }

    @objc func changeTitle(){
         if counter != 0
         {
            // self.lblTime.text = "The Code is valid for 5 minutes \n\nResend code in \(counter) sec"
             self.receivecodeBTN.isEnabled = false
             self.receivecodeBTN.alpha = 0.5
             counter -= 1
         }
         else
         {
              countTimer.invalidate()

             // self.lblTime.text = "The Code is valid for 5 minutes \n\nResend code in \(counter) sec"
              self.receivecodeBTN.isEnabled = true
              self.receivecodeBTN.alpha = 1.0
         }
    }

    @IBAction func receivecodeBTNTapped(_ sender: UIButton) {
        otpinfo["mobile"] = getMobileNumber as AnyObject
        otpinfo["user_type"] = "APP_USER" as AnyObject
        self.callResendOTP()
        otpTimerStart()
    }
}


//MARK: - UITableview Functions
extension ConsumerProfile: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return autocompleteresults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditLocationcell") as! EditLocationcell
        
        cell.locationLbl.text = autocompleteresults[indexPath.row].formattedAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("bla bla")
        locationTF.text = autocompleteresults[indexPath.row].formattedAddress
        
//        let placeid = autocompleteresults[indexPath.row].placeId
//        print(placeid)
//
//      //  print("Plce id ==",autocompleteResults[indexPath.row].placeId)
//
//                var input = GInput()
//                input.keyword = autocompleteresults[indexPath.row].placeId
//                GoogleApi.shared.callApi(.placeInformation,input: input) { (response) in
//                    if let place =  response.data as? GApiResponse.PlaceInfo, response.isValidFor(.placeInformation)
//                    {
//                        print("Lat = ",place.latitude,"Long",place.longitude)
//                        self.lat = place.latitude ?? 0
//                        self.long = place.longitude ?? 0
//        }
//                }
        locationTF.resignFirstResponder()
        hideresults()
    }
    
    
   // func tableView(_ tableView: UITableView,
//                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//
//        let action = UIContextualAction(style: .normal,
//                                        title: "Provider") { [weak self] (action, view, completionHandler) in
//           print("Hiiiii")
//            completionHandler(true)
//
//
//        }
//
//        action.backgroundColor = UIColor(red: 0.2, green: 0.8, blue: 0.6, alpha: 0.2)
//        return UISwipeActionsConfiguration()
//    }
}


//MARK: - OTP TextfieldDelegates
extension ConsumerProfile{

    func intialSetup(numberTxt1: UITextField, numberTxt2: UITextField, numberTxt3: UITextField, numberTxt4: UITextField){

        numberTxt1.delegate = self
        numberTxt2.delegate = self
        numberTxt3.delegate = self
        numberTxt4.delegate = self

        numberTxt1.becomeFirstResponder()

        numberTxt1.addTarget(self, action: #selector(handleOtp(_:)), for: UIControl.Event.editingChanged)
        numberTxt2.addTarget(self, action: #selector(handleOtp(_:)), for: UIControl.Event.editingChanged)
        numberTxt3.addTarget(self, action: #selector(handleOtp(_:)), for: UIControl.Event.editingChanged)
        numberTxt4.addTarget(self, action: #selector(handleOtp(_:)), for: UIControl.Event.editingChanged)

    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @objc private func handleOtp(_ textField: UITextField) {

        let text = textField.text
        if (text?.utf16.count)! >= 1
        {
            switch textField {
                case numberTxt1: self.numberTxt2.becomeFirstResponder()
                case numberTxt2: self.numberTxt3.becomeFirstResponder()
                case numberTxt3: self.numberTxt4.becomeFirstResponder()
                case numberTxt4: self.numberTxt4.resignFirstResponder()

                do {
                    numberTxt4.resignFirstResponder()

                    let otp = "\(numberTxt1.text!)\(numberTxt2.text!)\(numberTxt3.text!)\(numberTxt4.text!)"
                    print("-----Call Api-----OTP : \(otp)")

                    otpVerifyinfo["mobile"] = getMobileNumber as AnyObject
                    otpVerifyinfo["otp"] = otp as AnyObject
                    otpVerifyinfo["user_type"] = "APP_USER" as AnyObject

                    callVerifyOTPApi()
                }

                default: numberTxt4.resignFirstResponder()
            }
        }
        else {

            switch textField {
            case numberTxt4:

                numberTxt4.text!.isEmpty ? self.numberTxt3.becomeFirstResponder() : self.numberTxt4.becomeFirstResponder()

            case numberTxt3:

                numberTxt3.text!.isEmpty ? self.numberTxt2.becomeFirstResponder() : self.numberTxt4.becomeFirstResponder()

            case numberTxt2:

                numberTxt2.text!.isEmpty ? self.numberTxt1.becomeFirstResponder() : self.numberTxt3.becomeFirstResponder()

            case numberTxt1:

                numberTxt1.text!.isEmpty ? self.numberTxt1.becomeFirstResponder() : self.numberTxt2.becomeFirstResponder()

                default: break
            }
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

            if textField == mobileTF{
                let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                
                   mobileTF.placeholder = "(123) 456-7890"
                   editedphonenum = self.mobileTF.text ?? ""

            }
          else if textField == locationTF{
            let text = textField.text! as NSString
            let fullText = text.replacingCharacters(in: range, with: string)
            if fullText.count > 0 {
                showResults(string: fullText)
            }else{
                hideresults()
            }
           editedlocation = self.locationTF.text ?? ""
        }
        else if textField == nameTF{
            editedname = self.nameTF.text ?? ""
        }
       
        else{
        if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil {

           let newLength = (textField.text?.count)! + string.count - range.length

            if newLength == 2 {

                switch textField {
                case numberTxt4:

                    numberTxt4.text!.isEmpty ? self.numberTxt3.becomeFirstResponder() : self.numberTxt4.becomeFirstResponder()

                case numberTxt3:

                    numberTxt3.text!.isEmpty ? self.numberTxt2.becomeFirstResponder() : self.numberTxt4.becomeFirstResponder()

                case numberTxt2:

                    numberTxt2.text!.isEmpty ? self.numberTxt1.becomeFirstResponder() : self.numberTxt3.becomeFirstResponder()

                case numberTxt1:

                    numberTxt1.text!.isEmpty ? self.numberTxt1.becomeFirstResponder() : self.numberTxt2.becomeFirstResponder()

                    default: break
                }
            }

           return newLength <= 1

        }
        }
       return true
    }

    func clearTextFields(){
        numberTxt1.text = ""
        numberTxt2.text = ""
        numberTxt3.text = ""
        numberTxt4.text = ""
    }
}


//MARK: - API CALLS
extension ConsumerProfile{
    
//MARK: - User details API Call
    func userdeatilsapi(){
          
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
        
        let params = ["userId": userid] as [String : Any]
        
         let url = kBaseUrl+"getUserDetails"
        
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in

                switch (response.result) {
                case .success( let JSON):
                    
                    if let responsedata =  JSON as? [String:Any]  {
                        print("responsedata",responsedata)
                        self.userdetails = getUserDetailsResponse(from:responsedata)
                        
                        self.nameTF.text = self.userdetails?.Data?.fullName ?? ""
                        self.locationTF.text = self.userdetails?.Data?.city ?? ""
                        self.mobileTF.text = self.userdetails?.Data?.formattedPhoneNumber ?? ""
                       
                        self.normalphonenum = self.userdetails?.Data?.formattedPhoneNumber ?? ""
                        self.editedphonenum = self.userdetails?.Data?.formattedPhoneNumber ?? ""
                        self.editedlocation = self.userdetails?.Data?.city ?? ""
                        self.editedname = self.userdetails?.Data?.fullName ?? ""
                        
                        print(self.normalphonenum)
                        self.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.userdetails?.Data?.profilePic ?? "")),placeholder: UIImage(named: "profileicon"))
                        
                      
                             
                    }
                    
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
            
            
        }
    }
    
    
//MARK: - changeOTP API Call
    func changeotpapi(){
    
    let params: [String : Any] = [
        "countryCode": "+1",
        "phoneNumber": editedphonenum]
                                  
    print(params)
        
      let url = kBaseUrl+"sendChangePhonenumberOTP"
        //url = "http://13.234.177.61/api7/sendChangePhonenumberOTP"
        
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
         switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                  print("responsedata :",responsedata)
                    let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                        if responsedata["message"] as? String ?? "" == "Verification is sent"{
                            
                            self.otpMainView.isHidden = false
                            self.otpPopupview.isHidden = false
                            otpviewSetup()
                            self.normalphonenum = self.editedphonenum
                            otpviewSetup()
                            
                        }else{
                            let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                        }
                        }
                                                  ))
                            self.present(alert, animated: true)
                }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
            }
        }
    }
    
    
  //MARK: - SAVE API Call
    func saveUserDetailsapi(){
        showActivityIndicator()
    let name = nameTF.text ?? ""
        let countrycode = countryLbl.text ?? ""
        let phnnum = mobileTF.text ?? ""
        let location = locationTF.text ?? ""
        
        
    let params: [String : Any] = [
              "userid": userid,
              "name": name,
              "countrycode": "+1",
              "phonenumber": phnnum,
              "state": "",
              "city": location,
              "latitude": lat,
              "longitude": long]
    //let url = "http://13.234.177.61/api7/updateUserDetails"
        let url = kBaseUrl+"updateUserDetails"
    print("params\(params)")
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
        
        switch (response.result) {
        case .success( let JSON):
            if let responsedata =  JSON as? [String:Any]  {
              print("responsedata :",responsedata)
                hideActivityIndicator()
                self.navigationController?.popViewController(animated: true)
                
            }
            
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
        }
     }
   }
}


extension ConsumerProfile{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }

    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
}



//MARK: - OTP VIEW API CALLS

extension ConsumerProfile{
//MARK: - API Call
func callVerifyOTPApi(){
    showActivityIndicator()
//    let country : String = UserDefaults.standard.value(forKey: "Kcountrycode") as! String ?? ""
    let otp1 = numberTxt1.text ?? ""
    let otp2 = numberTxt2.text ?? ""
    let otp3 = numberTxt3.text ?? ""
    let otp4 = numberTxt4.text ?? ""
        OTP = otp1+otp2+otp3+otp4

    let params: [String : Any] = [
        "phoneNumber": mobileTF.text ?? "",
              "countryCode": "+1",
              "code": OTP]
  
    let url = kBaseUrl+"validateOTP"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: nil).validate(statusCode: 200..<510) .responseJSON { [self] response in

        switch (response.result) {
        case .success( let JSON):
            if let responsedata =  JSON as? [String:Any]  {
              print("responsedata :",responsedata)
                hideActivityIndicator()

                self.otpMainView.isHidden = true
                self.otpPopupview.isHidden = true
                clearTextFields()
            }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
        }
 }
}
    func callResendOTP(){
        showActivityIndicator()
       // let country : String = UserDefaults.standard.value(forKey: "Kcountrycode") as! String
        let params: [String : Any] = [
            "phoneNumber": mobileTF.text ?? "",
                  "countryCode": "+1"]
        let url = kBaseUrl+"varifyPhoneNumber"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: nil).validate(statusCode: 200..<510) .responseJSON { [self] response in
            print(response)
            hideActivityIndicator()
        }
    }
}

extension ConsumerProfile{
    func initializeHideKeyboard(){
     //Declare a Tap Gesture Recognizer which will trigger our dismissMyKeyboard() function
     let tap: UITapGestureRecognizer = UITapGestureRecognizer(
     target: self,
     action: #selector(dismissMyKeyboard))
     //Add this tap gesture recognizer to the parent view
     view.addGestureRecognizer(tap)
     }
     @objc func dismissMyKeyboard(){
     //endEditing causes the view (or one of its embedded text fields) to resign the first responder status.
     //In short- Dismiss the active keyboard.
     view.endEditing(true)
     }

}


//MARK: - LocationCell
class EditLocationcell: UITableViewCell{
    @IBOutlet weak var locationLbl: UILabel!
}

//MARK: - Countrypicker delegate functions
//extension ConsumerProfile: CountryPickerViewDelegate,CountryPickerViewDataSource{
//    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
//        code = country.phoneCode
//       countryLbl.text = code
//       if code  == "+1"{
//           mobileTF.placeholder = "(123) 456-7890"
//       }else if code == "+91"{
//           mobileTF.placeholder = "1234567890"
//       }
//
//    }
//
//    static func checkEnglishPhoneNumberFormat(string: String?, str: String?,phoneNumber : UITextField) -> Bool
//    {
//
//        if string == ""{ //BackSpace
//
//            return true
//
//        }else if str!.count < 3{
//
//            if str!.count == 1{
//
//                phoneNumber.text = "("
//            }
//
//        }else if str!.count == 5{
//
//            phoneNumber.text = phoneNumber.text! + ") "
//
//        }else if str!.count == 10{
//
//            phoneNumber.text = phoneNumber.text! + "-"
//
//        }else if str!.count > 14{
//
//            return false
//        }
//
//        return true
//    }
//    //
//    static func checkindianPhoneNumberFormat(string: String?, str: String?,phoneNumber : UITextField) -> Bool
//    {
//
//        if string == ""{ //BackSpace
//
//            return true
//
//        }else if str!.count > 10{
//
//            return false
//        }
//
//        return true
//     }
//}
