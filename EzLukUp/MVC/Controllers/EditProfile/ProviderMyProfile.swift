//
//  ProviderMyProfile.swift
//  EzLukUp
//
//  Created by REMYA V P on 05/04/23.
//

import UIKit
import SwiftUI
import Kingfisher
import Alamofire
import IPImage
import Contacts


class ProviderMyProfile: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var editBTN: BaseButton!
    @IBOutlet weak var editIMG: UIImageView!
    @IBOutlet weak var profileIMG: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var catview1: BaseView!
    @IBOutlet weak var catview2: BaseView!
    @IBOutlet weak var catview3: BaseView!
    
    @IBOutlet weak var companyname: UILabel!
    @IBOutlet weak var stackviewCat: UIStackView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    
    @IBOutlet weak var stackview: UIStackView!
    @IBOutlet weak var serviceBTNView: BaseView!
    @IBOutlet weak var infoBTNView: BaseView!
    @IBOutlet weak var feedbackBTNView: BaseView!
    @IBOutlet weak var servicesLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var feedbackLbl: UILabel!
    
    @IBOutlet weak var containerView1: UIView!
    @IBOutlet weak var containerView2: UIView!
    @IBOutlet weak var containerView3: UIView!
    @IBOutlet weak var stackviewHeight: NSLayoutConstraint!
    
    var userdetails : getUserDetailsResponse?
    var selectedBtn  = 0 // if 0 selected is servicesbtn , 1- infobtn selected , 2- feedbackbtn selected
    var selectedProfileImage : UIImage!
    var imageData = Data()
    var imageStr = ""
    var fileName = ""
    var mimetype = ""
    var activityView: UIActivityIndicatorView?
    var token : String = UserDefaults.standard.value(forKey: "Ktoken") as? String ?? ""
    var userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
    
    var checkfeedback = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getProviderProfileDetails()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.goToHomeTab), name: Notification.Name("hometab"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getProviderProfileDetails()
    }
    
    
    //MARK: - SETUP_FUNCTIONS
    func setupUI(){
        selectedBtn = 0
        stackview.layer.cornerRadius = stackview.frame.height / 2
        infoBTNView.backgroundColor = UIColor.clear
        feedbackBTNView.backgroundColor = UIColor.clear
        servicesLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        feedbackLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        self.containerView1.isHidden = false
        self.containerView2.isHidden = true
        self.containerView3.isHidden = true
        checkfeedreportdata()
        
    }
    
    @objc func goToHomeTab(){
            navigationController?.popToRootViewController(animated: true)
        }
    
    
    func checkfeedreportdata(){
        print("chakka",checkfeedback)
        if checkfeedback == "fromdata"{
            selectedBtn = 2
            serviceBTNView.backgroundColor = UIColor.clear
            infoBTNView.backgroundColor = UIColor.clear
            feedbackBTNView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
            servicesLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
            infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
            feedbackLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
            self.containerView1.isHidden = true
            self.containerView2.isHidden = true
            self.containerView3.isHidden = false
        }
    }
    
    @IBAction func backBTNAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
//        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
//        let vc = storyboard.instantiateViewController(identifier: "ProviderEditMyProfileVC") as! DashboardVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func editBTNTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProviderEditMyProfileVC") as! ProviderEditMyProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func serviceBTNTapped(_ sender: UIButton) {
        selectedBtn = 0
        infoBTNView.backgroundColor = UIColor.clear
        feedbackBTNView.backgroundColor = UIColor.clear
        serviceBTNView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        servicesLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        feedbackLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        self.containerView1.isHidden = false
        self.containerView2.isHidden = true
        self.containerView3.isHidden = true
       
    }
    
    @IBAction func infoBTNTapped(_ sender: UIButton) {
        selectedBtn = 1
        serviceBTNView.backgroundColor = UIColor.clear
        feedbackBTNView.backgroundColor = UIColor.clear
        infoBTNView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        servicesLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
        feedbackLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
        infoLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        self.containerView1.isHidden = true
        self.containerView2.isHidden = false
        self.containerView3.isHidden = true
        
    }
    
    
    @IBAction func feedbackBTNTapped(_ sender: UIButton) {
        selectedBtn = 2
        serviceBTNView.backgroundColor = UIColor.clear
        infoBTNView.backgroundColor = UIColor.clear
        feedbackBTNView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        servicesLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
        infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
        feedbackLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        self.containerView1.isHidden = true
        self.containerView2.isHidden = true
        self.containerView3.isHidden = false
        
    }
    
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }

    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
    
    //mime types
      func mimeType(for data: Data) -> String {
          
          var b: UInt8 = 0
          data.copyBytes(to: &b, count: 1)

          switch b {
          case 0xFF:
              return "i.jpeg"
          case 0x89:
              return ".png"
          case 0x47:
              return ".gif"
          case 0x4D, 0x49:
              return ".tiff"
          case 0x25:
              return "application/pdf"
          case 0xD0:
              return "application/vnd"
          case 0x46:
              return "text/plain"
          default:
              return "application/octet-stream"
          }
      }
    
    
    

}

extension ProviderMyProfile:UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    @IBAction func profilePhotoTapped(_ sender:UIButton){
//        let picker = UIImagePickerController()
//        picker.sourceType = .photoLibrary
//        picker.delegate = self
//        picker.allowsEditing = true
//        present(picker, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        self.profileIMG.image = image
        selectedProfileImage = image
        uploadImage()
        dismiss(animated: true)
    }
}

extension ProviderMyProfile{
    //MARK: - User details API Call
        func getProviderProfileDetails(){
              
            let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
            let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
            
            let params = ["userId": userid] as [String : Any]
            
           // let url = "http://13.234.177.61/api7/getUserDetails"
            let url = kBaseUrl+"getUserDetails"
            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
                    switch (response.result) {
                    case .success( let JSON):
                        
                        if let responsedata =  JSON as? [String:Any]  {
                            //print("responsedata",responsedata)
                            self.userdetails = getUserDetailsResponse(from:responsedata)
                            
                            self.nameLbl.text = self.userdetails?.Data?.fullName ?? ""
                            
                            //Setup Companyname
                                              // print(self.userdetails?.Data?.companyname)
                            if self.userdetails?.Data?.companyName == ""{
                                                   self.companyname.text = ""
                                               }else{
                                                   self.companyname.text = "@ \(self.userdetails?.Data?.companyName ?? "")"
                                               }
                            
                            self.label1.text = self.userdetails?.Data?.categoryIds[0].name ?? ""
//                            self.label2.text = self.userdetails?.Data?.categoryIds[1].name ?? ""
//                            self.label3.text = self.userdetails?.Data?.categoryIds[2].name ?? ""
                            self.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.userdetails?.Data?.profilePic ?? "")),placeholder: UIImage(named: "profileicon"))
                        }
                        
                    case .failure(let error):
                        print("Request error: \(error.localizedDescription)")
                    }
                
                
            }
        }
    
    
    //MARK: - PROFILE UPLOAD API CALL
         func uploadImage(){
             
             if selectedProfileImage != nil{
                 showActivityIndicator()
                 let uiImage : UIImage = self.selectedProfileImage
                 imageData = uiImage.jpegData(compressionQuality: 0.2)!
                 
                 imageStr = imageData.base64EncodedString()
                 fileName = "image"
                 mimetype = mimeType(for: imageData)
                 print("mime type = \(mimetype)")
             }else{
                 imageData = Data()
                 fileName = ""
             }
           
             ServiceManager.sharedInstance.uploadSingleDataa("saveProfileImage", headerf: token, parameters: ["userId":userid], image: imageData, filename: fileName, mimetype: mimetype, withHud: false) { (success, response, error) in
                 print(response as Any)
                 if success {
                     if response!["status"] as! Bool == true {
                         self.hideActivityIndicator()
                         print("image upload success")
                         
                     }else {
                         showDefaultAlert(viewController: self, title: "", msg: "image upload unsuccessfull")
                     }
                 }else{
                     print(error?.localizedDescription as Any)
                     showDefaultAlert(viewController: self, title: "", msg: error?.localizedDescription ?? "API Failed...!")
                 }
             }
         }
}
