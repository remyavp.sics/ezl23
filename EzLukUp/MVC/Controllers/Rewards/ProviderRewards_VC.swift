//
//  ProviderRewards_VC.swift
//  EzLukUp
//
//  Created by Srishti Innovative on 11/04/23.
//

import UIKit
import Alamofire

    func dropShadowForView(view:UIView){
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 6
        view.layer.cornerRadius = 4.0
}


class ProviderRewards_VC: UIViewController {

    @IBOutlet weak var viewRewards:UIView!
    @IBOutlet weak var stackViewOptions:UIStackView!
    @IBOutlet weak var opportunitiesBtnView:BaseView!
    @IBOutlet weak var trasactionsBtnView:BaseView!
    @IBOutlet weak var lblOpportunities:UILabel!
    @IBOutlet weak var lblTransactions:UILabel!
    @IBOutlet weak var containerView1: UIView!
    @IBOutlet weak var containerView2: UIView!
    @IBOutlet weak var lblTotalInviteRewardPoint : UILabel!
    @IBOutlet weak var lblTotalRecommendRewardPoint : UILabel!
    @IBOutlet weak var lblTotalRewardPoint : UILabel!
    
    var selectedBtn  = 0
    var rewardData : getRewardDescResponse?

    override func viewDidLoad() {
        super.viewDidLoad()
        getRewardData()
        dropShadowForView(view: viewRewards)
        stackViewOptions.layer.cornerRadius = 16.0
        trasactionsBtnView.backgroundColor = UIColor.clear
        opportunitiesBtnView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        lblOpportunities.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        lblTransactions.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        self.containerView1.isHidden = false
        self.containerView2.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.goToHomeTab), name: Notification.Name("hometab"), object: nil)
    }
    
    @objc func goToHomeTab(){
            navigationController?.popToRootViewController(animated: true)
        }
    
    @IBAction func rewardsHelpBtnTapped(_ sender:UIButton){
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProviderSpendRewards_VC") as! ProviderSpendRewards_VC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func opportunitiesBtnTapped(_ sender: UIButton) {
        selectedBtn = 0
        trasactionsBtnView.backgroundColor = UIColor.clear
        opportunitiesBtnView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        lblOpportunities.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        lblTransactions.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        self.containerView1.isHidden = false
        self.containerView2.isHidden = true
       
    }
    
    @IBAction func transationsBtnTapped(_ sender: UIButton) {
        selectedBtn = 1
        opportunitiesBtnView.backgroundColor = UIColor.clear
        trasactionsBtnView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        lblOpportunities.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
        lblTransactions.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        self.containerView1.isHidden = true
        self.containerView2.isHidden = false
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProviderRewards_VC{
    //MARK: - getRewardDesc API  Call
    func getRewardData(){
          
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
        
        let params = ["userid": userid] as [String : Any]
        
        let url = kBaseUrl+"getRewardDesc"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
                switch (response.result) {
                case .success( let JSON):
                    
                    if let responsedata =  JSON as? [String:Any]  {
                        self.rewardData = getRewardDescResponse(from:responsedata)
                        let inviteRewardPoint = self.rewardData?.totalInvitePoints ?? 0
                        let recommendationRewardPoint = self.rewardData?.totalTagPoints ?? 0
                        let totalRewardPoint = inviteRewardPoint + Int(recommendationRewardPoint)
                       // let roundValueTotalRewards = round(totalRewardPoint * 100) / 100
                        print(inviteRewardPoint)
                        print(recommendationRewardPoint)
                        print(totalRewardPoint)
                        
                        self.lblTotalInviteRewardPoint.text = "Inviting - \(inviteRewardPoint) ezM"
                        self.lblTotalRecommendRewardPoint.text = "Recommending - \(recommendationRewardPoint) ezM"
                        self.lblTotalRewardPoint.text = "Rewards - \(String(totalRewardPoint)) ezM"
                        
                    }
                    
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
          }
     }
}
