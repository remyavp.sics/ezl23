//
//  ProviderSpendRewards_VC.swift
//  EzLukUp
//
//  Created by Srishti Innovative on 11/04/23.
//

import UIKit

class ProviderSpendRewards_VC: UIViewController {
    
    @IBOutlet weak var viewComingsoon:UIView!
    @IBOutlet weak var viewPayingServiceProvider:UIView!
    @IBOutlet weak var viewGiftToOtherUsers:UIView!
    @IBOutlet weak var viewPayingForServices:UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setshadowforViews()
        dropShadowForView(view: viewComingsoon)
        
        

        // Do any additional setup after loading the view.
    }
    
    func setshadowforViews(){
        viewPayingServiceProvider.layer.shadowColor = UIColor.gray.cgColor
        viewGiftToOtherUsers.layer.shadowColor = UIColor.gray.cgColor
        viewPayingForServices.layer.shadowColor = UIColor.gray.cgColor
        //view.layer.shadowColor = UIColor.gray.cgColor

        viewPayingServiceProvider.layer.shadowOpacity = 0.3
        viewGiftToOtherUsers.layer.shadowOpacity = 0.3
        viewPayingForServices.layer.shadowOpacity = 0.3
        //view.layer.shadowOpacity = 0.3

        viewPayingServiceProvider.layer.shadowOffset = CGSize.zero
        viewGiftToOtherUsers.layer.shadowOffset = CGSize.zero
        viewPayingForServices.layer.shadowOffset = CGSize.zero
        //view.layer.shadowOffset = CGSize.zero

        viewPayingServiceProvider.layer.shadowRadius = 6
        viewGiftToOtherUsers.layer.shadowRadius = 6
        viewPayingForServices.layer.shadowRadius = 6
        //view.layer.shadowRadius = 6

        viewPayingServiceProvider.layer.cornerRadius = 4.0
        viewGiftToOtherUsers.layer.cornerRadius = 4.0
        viewPayingForServices.layer.cornerRadius = 4.0
        //view.layer.cornerRadius = 16.0

    }
    
    @IBAction func closeBtnTapped(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
