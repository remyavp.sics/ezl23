//
//  ProviderRewardsTransactions_VC.swift
//  EzLukUp
//
//  Created by Srishti Innovative on 11/04/23.
//

import UIKit
import Alamofire

class ProviderRewardsTransactions_VC: UIViewController {

    @IBOutlet weak var ProviderRewardsTransactionTableview:UITableView!
    @IBOutlet weak var dropDownViewHeight:NSLayoutConstraint!
    @IBOutlet weak var lblrewardType:UILabel!
    @IBOutlet weak var dropdownIMG: UIImageView!
    @IBOutlet weak var viewRewardFilter:UIView!
    var dropdownClicked = false
    var rewardTransactionsData : getUserRewardDetailsResponse?
    override func viewDidLoad() {
        super.viewDidLoad()
        getRewardTransactions(filter: "all")
        lblrewardType.text = "All activity"
        if dropdownClicked == false{
            dropDownViewHeight.constant = 0.0
        }
        //viewRewardFilter.roundcornerselected(corners: [.bottomLeft,.bottomRight], radius: 9)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dropDownBtnTapped(_ sender:UIButton){
        dropdownClicked = !dropdownClicked
        if dropdownClicked{
            dropDownViewHeight.constant = 156.0
            UIView.animate(withDuration: 0, animations: {
                self.dropdownIMG.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
            })
            
        }else{
            dropDownViewHeight.constant = 0.0
            UIView.animate(withDuration: 0, animations: {
                 self.dropdownIMG.transform = CGAffineTransform.identity
            })
        }
    }
    
    @IBAction func getAllTransactions(_ sender:UIButton){
        lblrewardType.text = "All Activity"
        getRewardTransactions(filter: "all")
        dropDownViewHeight.constant = 0.0
        dropdownClicked = false
    }
    
    @IBAction func getInvitedTransactions(_ sender:UIButton){
        lblrewardType.text = "Inviting General Contacts"
        getRewardTransactions(filter: "invite")
        dropDownViewHeight.constant = 0.0
        dropdownClicked = false
    }
    
    @IBAction func getRecommendedTransactions(_ sender:UIButton){
        lblrewardType.text = "Tagging Service Providers"
        getRewardTransactions(filter: "tag")
        dropDownViewHeight.constant = 0.0
        dropdownClicked = false
    }
    
}

extension ProviderRewardsTransactions_VC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rewardTransactionsData?.Data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProviderRewardsTransactions_TVCell", for: indexPath) as! ProviderRewardsTransactions_TVCell
        let data = self.rewardTransactionsData?.Data[indexPath.row]
        cell.lblname.text = data?.invitedUserId?.fullName ?? "name not fetched"
        if data?.rewardTypeId?.rewardType == "invite_business_contact"{
            cell.lblrewardType.text = "Recommended"
        }else{
            cell.lblrewardType.text = "Invited"
        }
        cell.lblRewardAmt.text = String(data?.amount ?? 0)
        let date = data?.createdAt ?? ""
        let convertedDate = formatDate(date)
        cell.lblrewardUpdatedDate.text = "Completed: \(convertedDate)"
        
        return cell
    }
    
    
}


extension ProviderRewardsTransactions_VC{
    //MARK: - API Call
    func getRewardTransactions(filter:String){
          
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
        
        let params = ["userid": userid,"filter":filter] as [String : Any]
        
        let url = kBaseUrl+"getReward"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
                switch (response.result) {
                case .success( let JSON):
                    
                    if let responsedata =  JSON as? [String:Any]  {
                        self.rewardTransactionsData = getUserRewardDetailsResponse(from:responsedata)
                    }
                    DispatchQueue.main.async {
                        self.ProviderRewardsTransactionTableview.reloadData()
                    }
                    
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
            
            
        }
    }
}


func formatDate(_ serverString:String)->String{
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone = TimeZone(identifier: "UTC")
    var displayString = ""
    let stringWithoutZ = serverString.replacingOccurrences(of: "Z", with: "")
    
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
   
    if let date = dateFormatter.date(from: stringWithoutZ){
        dateFormatter.timeZone = .current
    dateFormatter.dateFormat = "MM/dd/yyyy"
     displayString = dateFormatter.string(from: date)
    }
    return displayString
}
