//
//  ProviderRewardsOpportunities_VC.swift
//  EzLukUp
//
//  Created by Srishti Innovative on 11/04/23.
//

import UIKit
import Alamofire

class ProviderRewardsOpportunities_VC: UIViewController {
    
    //@IBOutlet weak var viewInviteGeneralContacts:UIView!
    //@IBOutlet weak var viewrecommendProviders:UIView!
    @IBOutlet weak var opportunities_TV:UITableView!


    
    var opportunitiesData : getRewardDescResponse?

    override func viewDidLoad() {
        super.viewDidLoad()
        getRewardOpportunities()
        //dropShadowForView(view: viewInviteGeneralContacts)
        //dropShadowForView(view: viewrecommendProviders)

        // Do any additional setup after loading the view.
    }


}

extension ProviderRewardsOpportunities_VC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return opportunitiesData?.Data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProviderRewardsOpportunities_TVCell", for: indexPath) as! ProviderRewardsOpportunities_TVCell
        cell.lblTitle.text = self.opportunitiesData?.Data[indexPath.row].title ?? ""
        cell.lblDescription.text = self.opportunitiesData?.Data[indexPath.row].description ?? ""
        cell.lblAmount.text = "MAX  \(self.opportunitiesData?.Data[indexPath.row].amount ?? 0)"
        
        
       // String(self.opportunitiesData?.Data[indexPath.row].amount ?? 0)
        return cell
    }
}

extension ProviderRewardsOpportunities_VC{
    //MARK: - API Call
    func getRewardOpportunities(){
          
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
        
        let params = ["userid": userid] as [String : Any]
        
      //  let url = "http://13.234.177.61/api7/getRewardDesc"
        let url = kBaseUrl+"getRewardDesc"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
                switch (response.result) {
                case .success( let JSON):
                    
                    if let responsedata =  JSON as? [String:Any]  {
                        self.opportunitiesData = getRewardDescResponse(from:responsedata)
                    }
                    DispatchQueue.main.async {
                        self.opportunities_TV.reloadData()
                    }
                    
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
            
            
        }
    }
}
