//
//  Splash.swift
//  EzLukUp
//
//  Created by Srishti on 14/09/22.
//

import UIKit

class Splash: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let logstat = UserDefaults.standard.value(forKey: "loggedin") ?? "false"
        if logstat as! String == "true" {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            if #available(iOS 14.0, *) {
                let vc = storyboard.instantiateViewController(identifier: "CustomTabbarVC") as! CustomTabbarVC
                self.navigationController?.pushViewController(vc, animated: false)
            } else {
                // Fallback on earlier versions
            }
        }
        else{
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "WelcomeVC") as! WelcomeVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        let logstat = UserDefaults.standard.value(forKey: "loggedin") ?? "false"
        if logstat as! String == "true" {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            if #available(iOS 14.0, *) {
                let vc = storyboard.instantiateViewController(identifier: "CustomTabbarVC") as! CustomTabbarVC
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                // Fallback on earlier versions
            }
        }
        else{
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "WelcomeVC") as! WelcomeVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
