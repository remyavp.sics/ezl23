//
//  ConSliderSecond.swift
//  EzLukUp
//
//  Created by REMYA V P on 23/10/22.
//

import UIKit

class ConSliderSecond: UIViewController {
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    //@IBOutlet weak var label3: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var continueBtn: BaseButton!
    var usertype = UserDefaults.standard.value(forKey: "Ktype") ?? ""
   override func viewDidLoad() {
        super.viewDidLoad()
       label1.font = UIFont(name: "Jost-Regular", size: 20)
       label2.font = UIFont(name: "Jost-Regular", size: 20)
       //label3.font = UIFont(name: "Jost-Regular", size: 17)
       continueBtn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
       setupUI()
   }
   
    func setupUI(){
        if usertype as! String == "Business"{
            //show provider welcome image
            imgView.image = UIImage(named: "Provider-2")
            label1.isHidden = false //provider
            label2.isHidden = true //consumer
            label1.text = "Invite your customers and get recommendations"
        }else{
            //show Consumer welcome image
            imgView.image = UIImage(named: "Consumer-2")
            label2.isHidden = true
//            label1.isHidden = true
            label1.text = "Recommend service providers who \n are Reliable and Trusted"
        }
    }
    
    @IBAction func btnContinueAction(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name("nextslide"), object: nil)
    }
    
 
}
