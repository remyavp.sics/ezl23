//
//  ConSliderFirst.swift
//  EzLukUp
//
//  Created by REMYA V P on 23/10/22.
//

import UIKit

class ConSliderFirst: UIViewController {

    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnContinue: BaseButton!
    var usertype = UserDefaults.standard.value(forKey: "Ktype") ?? ""
    // var delegate: SwipeViewdelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        btnContinue.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
        label1.font = UIFont(name: "Jost-Regular", size: 20)
        
    }
    
    func setupUI(){
        if usertype as! String == "Business"{
            //show provider welcome image
            imgView.image = UIImage(named: "Provider-1")
            label1.isHidden = false
            label2.isHidden = true
        }else{
            //show Consumer welcome image
            imgView.image = UIImage(named: "Consumer-1")
            label1.isHidden = true
            label2.isHidden = false
        }
    }
   
    
    
    @IBAction func btnContinueAction(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name("nextslide"), object: nil)
    //delegate?.onContinueClicked()
        
        
      //  self.onContinueClicked()
        
        
//        let storyboard = UIStoryboard(name: "Login", bundle: nil)
//                                       let vc = storyboard.instantiateViewController(identifier: "SetupVC") as! SetupVC
//                           self.navigationController?.pushViewController(vc, animated: true)
                              
        
    }
   
    
 
}
