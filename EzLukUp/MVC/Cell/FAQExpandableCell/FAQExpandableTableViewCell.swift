//
//  FAQExpandableTableViewCell.swift
//  EzLukUp
//
//  Created by Srishti Innovative on 15/05/23.
//

import UIKit



class FAQExpandableTableViewCell: UITableViewCell, Registerable {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var tapableStackView: UIStackView! {
        didSet {
            containerView.layer.cornerRadius = 4.0
            dropShadowForView(view: contentView)
            tapableStackView.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapOnExpandableCell))
            tapableStackView.addGestureRecognizer(tapGesture)
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel! {
        didSet {
            descriptionLabel.isHidden = true
        }
    }
    
    @IBOutlet weak var topBorderView: UIView!
    var didTapOnExpandableCell: (() -> Void) = {}
    /* var faqData:FAQ_Data? {
     didSet {
     titleLabel.text = faqData?.question
     descriptionLabel.text = faqData?.answer
     }
     }*/
}

extension FAQExpandableTableViewCell {
}

// MARK: - Action
extension FAQExpandableTableViewCell {
    @objc func tapOnExpandableCell() {
        descriptionLabel.isHidden.toggle()
        if (descriptionLabel.isHidden == true){
            
            self.btnArrow.setImage(UIImage(named: "dropup_icon"), for: .normal)
        }else {
            self.btnArrow.setImage(UIImage(named: "dropdown_icon"), for: .normal)
        }
        didTapOnExpandableCell()
    }
    
    
    @IBAction func dropDownActionBtnTap(_ sender: Any) {
        tapOnExpandableCell()
    }
}

