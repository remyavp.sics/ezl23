//
//  HomeListCell.swift
//  EzLukUp
//
//  Created by REMYA V P on 20/10/22.
//

import UIKit
import Kingfisher

class HomeListCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

//Dashboard fisrt cell
class Listcell1:UITableViewCell{
    @IBOutlet weak var rewardcountLBL:UILabel!
    @IBOutlet weak var generalcontactscountLBL:UILabel!
    @IBOutlet weak var recommendingCntLBL:UILabel!
    @IBOutlet weak var recommendationsCntLBL:UILabel!
    var Dashboardmodel : DashboardDataModel?
}
//Dashboard second cell

class Listcell2:UITableViewCell{
    var Dashboardmodel : DashboardDataModel?
    @IBOutlet weak var recomendationCV1:UICollectionView!
    
    @IBOutlet weak var emptyview: BaseView!
    @IBOutlet weak var viewAllBTN: UIButton!
    var firstisoformatmodel = ""
}
//Dashboard third cell

class Listcell3:UITableViewCell{
    var Dashboardmodel : DashboardDataModel?
    @IBOutlet weak var recomendationCV2:UICollectionView!
    @IBOutlet weak var emptyview: BaseView!
    @IBOutlet weak var viewAllBTN: UIButton!
    var secondisoformatmodel = ""
    var type = ""
}
//Dashboard fourth cell

class Listcell4:UITableViewCell{
    var Dashboardmodel : DashboardDataModel?
    @IBOutlet weak var emptyview: BaseView!
    @IBOutlet weak var recomendationCV3:UICollectionView!
}

//MARK: - 2nd Collectionview delegates
extension Listcell2:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if Dashboardmodel?.Data?.recentRecommendProviders.count ?? 0 <= 7{
            return Dashboardmodel?.Data?.recentRecommendProviders.count ?? 0
        }else{
            return 7
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listcollectioncell", for: indexPath) as! listcollectioncell
        cell.nameLBL.text = Dashboardmodel?.Data?.recentRecommendProviders[indexPath.row].userId?.fullName ?? ""
        cell.profileImage.kf.setImage(with: URL(string: kImageUrl + (self.Dashboardmodel?.Data?.recentRecommendProviders[indexPath.row].userId?.profilePic ?? "")),placeholder: UIImage(named: "profileicon"))
        cell.mainviewBTN.tag = indexPath.row
        
    //days Ago
        self.firstisoformatmodel = self.Dashboardmodel?.Data?.recentRecommendProviders[indexPath.row].recommendedAt ?? ""
        let daysagoString = convertisoformat(dateString: firstisoformatmodel)
          print(daysagoString)
          cell.daysagoLBL.text = daysagoString
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = (collectionView.frame.size.width - 8) / 2.3
               // let h:CGFloat = (collectionView.frame.size.height - 10) / 1
        return CGSize(width: collectionView.frame.width / 2.5, height: collectionView.frame.height)
    }
    
//days calculation
    func convertisoformat(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: firstisoformatmodel)!
        let timeInterval = date.timeIntervalSinceNow
        
        let secondsAgo = Int(abs(timeInterval))
        let minutesAgo = secondsAgo / 60
        let hoursAgo = minutesAgo / 60
        let daysAgo = hoursAgo / 24
        let weeksAgo = daysAgo / 7
        let monthsAgo = weeksAgo / 4
        let yearsAgo = monthsAgo / 12
        
        if secondsAgo < 60 {
            return "\(secondsAgo) seconds ago"
        } else if minutesAgo < 60 {
            return "\(minutesAgo) minutes ago"
        } else if hoursAgo < 24 {
            return "\(hoursAgo) hours ago"
        } else if daysAgo < 7 {
            return "\(daysAgo) days ago"
        } else if weeksAgo < 4 {
            return "\(weeksAgo) weeks ago"
        } else if monthsAgo < 12 {
            return "\(monthsAgo) months ago"
        } else {
            return "\(yearsAgo) years ago"
        }
    }
}


//MARK: - 2nd Collectionview delegates
extension Listcell3:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("chakkarachii")
        print(Dashboardmodel?.Data?.recentFeedback.count)
        if Dashboardmodel?.Data?.recentFeedback.count ?? 0 <= 7{
            return Dashboardmodel?.Data?.recentFeedback.count ?? 0
        }else{
            return 7
        }
//        return Dashboardmodel?.Data?.recentFeedback.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listcollectioncell2", for: indexPath) as! listcollectioncell2
        cell.feedbackLBL.text = Dashboardmodel?.Data?.recentFeedback[indexPath.item].feedback ?? ""
       // cell.nameLBL.text = Dashboardmodel?.Data?.recentFeedback[indexPath.row].userId?.fullName ?? ""
        cell.profileImage.kf.setImage(with: URL(string: kImageUrl + (self.Dashboardmodel?.Data?.recentFeedback[indexPath.row].userId?.profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
        
        
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
        var name : String = Dashboardmodel?.Data?.recentFeedback[indexPath.item].userId?.fullName ?? ""
        

        //MARK:- Set Level of connections
        if Dashboardmodel!.Data!.recentFeedback[indexPath.row].userId != nil && Dashboardmodel!.Data!.recentFeedback[indexPath.row].userId!.firstLevelConnections!.contains(userid){
            cell.nameLabel.text = name + " .1st"
               // cell.connectNameLbl.text = "1st"
        }else if Dashboardmodel!.Data!.recentFeedback[indexPath.row].userId != nil && Dashboardmodel!.Data!.recentFeedback[indexPath.row].userId!.secondLevelConnections!.contains(userid){
            cell.nameLabel.text = name + " .2nd"
                  //  cell.connectNameLbl.text = "2nd"
        }
        else if Dashboardmodel!.Data!.recentFeedback[indexPath.row].userId != nil && Dashboardmodel!.Data!.recentFeedback[indexPath.row].userId!.thirdLevelConnections!.contains(userid){
            cell.nameLabel.text = name + " .3rd"
                   // cell.connectNameLbl.text = "3rd"
                }
        else{
             //   cell.nameLabel.text = Dashboardmodel?.Data?.recentFeedback[indexPath.item].userId?.fullName ?? "" + "3rd"
            cell.nameLabel.text = "3rd"
            }

        
//MARK:- Feedback/Report border colouring
        type = Dashboardmodel?.Data?.recentFeedback[indexPath.item].type ?? ""
            if type == "feedback"{
                cell.mainview.borderColor = #colorLiteral(red: 0.2, green: 0.7960784314, blue: 0.5960784314, alpha: 1)
            }else{
                cell.mainview.borderColor = #colorLiteral(red: 0.9176470588, green: 0.262745098, blue: 0.2078431373, alpha: 1)
            }
        
//MARK:- daysAgo
            self.secondisoformatmodel = self.Dashboardmodel?.Data?.recentFeedback[indexPath.row].createdAt ?? ""
            let daysagoString = convertisoformat(dateString: secondisoformatmodel)
              print(daysagoString)
              cell.daysagoLBL.text = daysagoString
        
        cell.mainviewBTN.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = (collectionView.frame.size.width - 8) / 1.5
              
        return CGSize(width: collectionView.frame.width / 1.7, height: collectionView.frame.height)
    }
    
    
    //days calculation
        func convertisoformat(dateString: String) -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let date = dateFormatter.date(from: secondisoformatmodel)!
            let timeInterval = date.timeIntervalSinceNow
            
            let secondsAgo = Int(abs(timeInterval))
            let minutesAgo = secondsAgo / 60
            let hoursAgo = minutesAgo / 60
            let daysAgo = hoursAgo / 24
            let weeksAgo = daysAgo / 7
            let monthsAgo = weeksAgo / 4
            let yearsAgo = monthsAgo / 12
            
            if secondsAgo < 60 {
                return "\(secondsAgo) seconds ago"
            } else if minutesAgo < 60 {
                return "\(minutesAgo) minutes ago"
            } else if hoursAgo < 24 {
                return "\(hoursAgo) hours ago"
            } else if daysAgo < 7 {
                return "\(daysAgo) days ago"
            } else if weeksAgo < 4 {
                return "\(weeksAgo) weeks ago"
            } else if monthsAgo < 12 {
                return "\(monthsAgo) months ago"
            } else {
                return "\(yearsAgo) years ago"
            }
        }
}


//MARK: - 3rd Collectionview delegates
extension Listcell4:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("chakkarachi")
        print(self.Dashboardmodel?.Data?.profileVisitors.count)
        if Dashboardmodel?.Data?.profileVisitors.count ?? 0 <= 7{
            return Dashboardmodel?.Data?.profileVisitors.count ?? 0
        }else{
            return 7
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listcollectioncell3", for: indexPath) as! listcollectioncell3
        cell.nameLBL.text = Dashboardmodel?.Data?.profileVisitors[indexPath.row].viewerId.first?.fullName ?? ""
        cell.profileImage.kf.setImage(with: URL(string: kImageUrl + (self.Dashboardmodel?.Data?.profileVisitors[indexPath.row].viewerId.first?.profilePic ?? "")),placeholder: UIImage(named: "profileicon"))
         
        let city = Dashboardmodel?.Data?.profileVisitors[indexPath.row].viewerId.first?.areaOfService.first?.city ?? ""
        let stateshortcode = Dashboardmodel?.Data?.profileVisitors[indexPath.row].viewerId.first?.areaOfService.first?.stateShortCode ?? ""

        
        if Dashboardmodel?.Data?.profileVisitors[indexPath.row].viewerId.first?.type == "Business"{
            if city != "" && stateshortcode != ""{
                cell.locationLBL.text = "\(city) , \(stateshortcode)"
            }else{
                if city == ""{
                    cell.locationLBL.text = stateshortcode
                }else{
                    cell.locationLBL.text = city
                }
            }
        }else{
            cell.locationLBL.text = Dashboardmodel?.Data?.profileVisitors[indexPath.row].viewerId.first?.city ?? ""
        }
        
        
        cell.mainviewBTN.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = (collectionView.frame.size.width - 8) / 2.3
               // let h:CGFloat = (collectionView.frame.size.height - 10) / 1
        return CGSize(width: collectionView.frame.width / 2.5, height: collectionView.frame.height)
    }
}

//MARK: - First CollectionviewCell
class listcollectioncell:UICollectionViewCell{
    @IBOutlet weak var profileImage:UIImageView!
    @IBOutlet weak var nameLBL:UILabel!
    @IBOutlet weak var daysagoLBL:UILabel!
    @IBOutlet weak var mainView: BaseView!
    
    @IBOutlet weak var mainviewBTN: UIButton!
}

//MARK: - second CollectionviewCell
class listcollectioncell2:UICollectionViewCell{
    @IBOutlet weak var profileImage:UIImageView!
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var feedbackLBL:UILabel!
    @IBOutlet weak var daysagoLBL:UILabel!
    @IBOutlet weak var mainview: BaseView!
    @IBOutlet weak var mainviewBTN: UIButton!
}

//MARK: - third CollectionviewCell
class listcollectioncell3:UICollectionViewCell{
    @IBOutlet weak var profileImage:UIImageView!
    @IBOutlet weak var nameLBL:UILabel!
    @IBOutlet weak var locationLBL:UILabel!
    @IBOutlet weak var mainview: BaseView!
    @IBOutlet weak var mainviewBTN: UIButton!
}
