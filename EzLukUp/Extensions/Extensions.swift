//
//  Extensions.swift
//  EzLukUp
//
//  Created by REMYA V P on 07/10/22.
//

import Foundation
import UIKit


extension UIView{
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
           let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
           let mask = CAShapeLayer()
           mask.path = path.cgPath
           layer.mask = mask
       }
   
    
    func addCornerForView(cornerRadius:CGFloat? = nil){
        self.layer.cornerRadius = cornerRadius ?? (self.bounds.size.height)/2
        self.clipsToBounds = true
    }
}

extension UIViewController{
    func showToast(message : String, font: UIFont) {
        let xVal = ((self.view.frame.size.width/2)/2)
        let toastLabel = UILabel(frame: CGRect(x: xVal, y: self.view.frame.size.height-65, width: self.view.frame.size.width/2 + 60, height: 50))
        print("Toast posiiton: \(self.view.frame.size.width/2 - 65)")
        toastLabel.backgroundColor = UIColor.white
        toastLabel.textColor = UIColor.black
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
    //    toastLabel.center = self.view.center
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
//extension Sequence where Element: Hashable {
//    func uniqued() -> [Element] {
//        var set = Set<Element>()
//        return filter { set.insert($0).inserted }
//    }
//}
////newly added
//extension Array where Element: Equatable {
//    var unique: [Element] {
//        var uniqueValues: [Element] = []
//        forEach { item in
//            guard !uniqueValues.contains(item) else { return }
//            uniqueValues.append(item)
//        }
//        return uniqueValues
//    }
//}
// end of newly added
extension Sequence {
    public func splitSorted(by condition: (Element, Element)->(Bool)) -> [[Element]] {
        var it = makeIterator()
        guard var currentElem = it.next() else {
            return [] // Input sequence is empty
        }
        var result = [[Element]]()
        var currentSegment = [currentElem]
        while let nextElem = it.next() {
            if condition(currentElem, nextElem) {
                // Append to current segment:
                currentSegment.append(nextElem)
            } else {
                // Start new segment:
                result.append(currentSegment)
                currentSegment = [nextElem]
            }
            currentElem = nextElem
        }
        result.append(currentSegment)
        return result
    }
}
extension Array where Element: Hashable {
    func uniqued() -> Array {
        var buffer = Array()
        var added = Set<Element>()
        for elem in self {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
}
