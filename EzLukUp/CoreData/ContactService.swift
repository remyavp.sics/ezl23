//
//  ContactService.swift
//  SampleAPI
//
//  Created by mac on 22/06/23.
//

import Foundation

class ContactService {
    func getSingleContactList(limit:String,userid:String ,callback: @escaping(SingleContactListResponse?, Error?)->()) {
        let params = [
            "userId": userid,
            "filter":"all",
            "offset":0,
            "limit":0
        ] as [String:Any]
        let manager = ContactManager()
        manager.getSingleContactList(params: params) { response, error in
            if error == nil {
                callback(response, nil)
            } else {
                callback(nil, error)
            }
        }
    }
    func getProviderContactList(userid:String , callback: @escaping(ProviderSingleContactListResponse?, Error?)->()){
        let params = [
            "userId": userid,
            "filter":"all",
            
        ] as [String:Any]
        let manager = ContactManager()
        manager.getProviderContactList(params: params)
       { response, error in
            if error == nil {
                callback(response, nil)
            } else {
                callback(nil, error)
            }
        }
    }
}
class ContactManager: WSManager {
    func getSingleContactList(params: [String:Any], callback: @escaping(SingleContactListResponse?, Error?)->()) {
        self.makeRequest(url: Url.contactsSingleApi, decodeItem: SingleContactListResponse.self, method: .post, params: params, callback: callback)
    }
    func getProviderContactList(params: [String:Any], callback: @escaping(ProviderSingleContactListResponse?, Error?)->()) {
        self.makeRequest(url: Url.providercontactsSingleApi, decodeItem: ProviderSingleContactListResponse.self, method: .post, params: params, callback: callback)
    }
}
