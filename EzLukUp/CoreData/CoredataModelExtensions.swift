//
//  CoredataModelExtensions.swift
//  EzLukUp
//
//  Created by Srishti Innovative on 08/07/23.
//

import Foundation
extension SingleContactListResponse{
    func gettotalcount() -> Int{
        return (self.normalListCount ?? 0) + (self.possibleProvidersCount ?? 0) + (self.taggedContactCount ?? 0)
    }
    func getnormallistChunked(list:[SingleContactListData] = [] , isFiltered:Bool) -> [getcontactDataListModelcodable]{
        let filteredlist = !isFiltered &&  list.isEmpty ? self.normalList : list
        var normalList :[getcontactDataListModelcodable] = []
        let groupedModels = Dictionary(grouping: filteredlist ?? []) { model in
            return model.name?.uppercased().prefix(1)
        }
        let mapedmodel = groupedModels.map({ (String($0.0 ?? ""),Array($0.1)) })
        let contactx = mapedmodel.sorted(by: {$0.0.compare($1.0, options: .caseInsensitive) == .orderedAscending })
        for contact in contactx {
            normalList.append(getcontactDataListModelcodable(headername: contact.0, contacts: contact.1))
                    }
        return normalList
    }
//filter & search function for normal list (ABCD ....)
    func getfilterednormallist(_ type: filtertype = .all, _ searchKey: String = "") -> [getcontactDataListModelcodable]{
        let searchResult = searchKey.isEmpty ? self.normalList ?? [] : self.normalList?.filter({ $0.name?.range(of: searchKey, options: .caseInsensitive) != nil }) ?? []
        switch type{
        case .all:
            return self.getnormallistChunked(list: searchResult, isFiltered: !searchKey.isEmpty)
        case .invite:
            let filteredarray = searchResult.filter({!($0.isUser!) && !$0.isReferred! && !$0.notInUs!})
            return getnormallistChunked(list: filteredarray, isFiltered: !searchKey.isEmpty)
        case .invited:
            let filteredarray = searchResult.filter({$0.isReferred! && !$0.notInUs! && !$0.isUser!})
            return getnormallistChunked(list: filteredarray, isFiltered: !searchKey.isEmpty)
        case .inez:
            let filteredarray = searchResult.filter({($0.isUser!) && !$0.notInUs!})
            return getnormallistChunked(list: filteredarray, isFiltered: !searchKey.isEmpty )
        }
    }
    //filter & search function for tagged contact list (ABCD ....)
    func getFilteredTaggedContactlist(_ type: filtertype = .all, _ searchKey: String = "") -> [SingleContactListData]{
        let searchResult = searchKey.isEmpty ? self.taggedContact ?? [] : self.taggedContact?.filter({ $0.name?.range(of: searchKey, options: .caseInsensitive) != nil }) ?? []
        switch type{
        case .all:
            return searchResult
        case .invite:
            let filteredarray = searchResult.filter({!($0.isUser!) && !$0.isReferred! && !$0.notInUs!})
            return []
        case .invited:
            let filteredarray = searchResult.filter({$0.isReferred! && !$0.notInUs!})
            return []
        case .inez:
            let filteredarray = searchResult.filter({($0.isUser!) && !$0.notInUs!})
            return filteredarray
        }
    }
    //filter & search for possible list (ABCD ....)
    func getFilteredPossibleProviderlist(_ type: filtertype = .all, _ searchKey: String = "") -> [SingleContactListData]{
        let searchResult = searchKey.isEmpty ? self.possibleProviders ?? [] : self.possibleProviders?.filter({ $0.name?.range(of: searchKey, options: .caseInsensitive) != nil }) ?? []
        switch type{
        case .all:
            return searchResult
        case .invite:
            let filteredarray = searchResult.filter({!($0.isUser!) && !$0.isReferred! && !$0.notInUs!})
            return []
        case .invited:
            let filteredarray = searchResult.filter({$0.isReferred! && !$0.notInUs!})
            return []
        case .inez:
            let filteredarray = searchResult.filter({($0.isUser!) && !$0.notInUs!})
            return filteredarray
        }
    }
}

enum filtertype : Int{
    case all = 0 , invite ,invited , inez
}

enum Providerfiltertype : Int{

    case all = 0 , inezRecomendedbyMe , inezRecomendedbyOthers , inezjustConnected , inezNeedsreview

}
extension ProviderSingleContactListResponse{
    
    func getFilteredProviderlist(_ type: Providerfiltertype = .all, _ searchKey: String = "") -> [ProviderCategoryListModel]{
      
        let searchResult = searchKey.isEmpty ? self.data : self.data.compactMap({ProviderCategoryListModel(_id: $0._id ,name: $0.name, contacts: $0.contacts.filter({ $0.name?.localizedCaseInsensitiveContains(searchKey) ?? false }) )}).filter({ !$0.contacts.isEmpty })
        switch type{
        case .all:
            return searchResult
        case .inezRecomendedbyMe:
            let filteredarray = searchResult.compactMap({ProviderCategoryListModel(_id: $0._id ,name: $0.name , contacts: $0.contacts.filter({($0.isRecommended ?? false) && $0.recommendedCount == 1}) )})
                return filteredarray
        case .inezRecomendedbyOthers:
            let filteredarray = searchResult.compactMap({ProviderCategoryListModel(_id: $0._id ,name: $0.name , contacts: $0.contacts.filter({$0.recommendedCount >= 1}) )})
            return filteredarray
        case .inezjustConnected:
            let filteredarray = searchResult.compactMap({ProviderCategoryListModel(_id: $0._id ,name: $0.name , contacts: $0.contacts.filter({!($0.isRecommended ?? false) && ($0.isReferred ?? false)}) )})
            return filteredarray
        case .inezNeedsreview :
            let filteredarray = searchResult.compactMap({ProviderCategoryListModel(_id: $0._id ,name: $0.name , contacts: $0.contacts.filter({$0.needCatagoryReview ?? false}) )})
            return filteredarray
        }
    }
}
//in ez  Recomended by Me   = isRecommended   = true
//in ez  Recomended by Others = isRecommended   false, recommendedCount >= 1
//in ez  just Connected =  isRecommended   false , isrefered   true
//in ez  Needs review = needCatagoryReview  true
